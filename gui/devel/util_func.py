
from pykat import finesse
import numpy as np

##################################################################################
# get_model()
# |- Add xaxis
# |- Add PDs
# |
# |- return model
##################################################################################

def get_model(sim_conf, inteferometer_config_list):
    

    input_finesse = """

    # ======== Constants ========================
    const fsb1 16.881M
    const fsb2 45.0159M
    const mfsb1 -16.881M
    const mfsb2 -45.0159M
    const a 0.686
    const phi_PRM 90
    const pi 3.1415
    const phi_SRM 90

    # ======== Input optics =====================
    l i1 1 0 n0
    s s_eo0 0 n0 n_eo1
    mod eom1 $fsb1 0.3 1 pm n_eo1 n_eo2
    s s_eo1 0 n_eo2 n_eo3
    mod eom2 $fsb2 0.3 1 pm n_eo3 n_eo4
    #s s_eo2 0 n_eo4 n_eo5
    s s_eo2 0 n_eo4 REFL

    ## ======= PRC each mirror loss 45ppm =======
    # PRC
    #m1 PRM 0.1 45e-6 $phi_PRM REFL_gouy_tuner_out npr1
    m1 PRM 0.1 45e-6 $phi_PRM REFL npr1
    s sLpr1 14.7615 npr1 npr2
    bs1 PR2 500e-6 45e-6 0 $a npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
    s sLpr2 11.0661 npr3 npr4
    bs1 PR3 50e-6 45e-6 0 $a dump dump npr4 npr5
    s sLpr3 15.7638 npr5 npr6

    # Michelson
    bs MIBS 0.5 0.5 0 45 npr6 n2 n3 n4
    s lx 26.4018 n3 nitx1 %26.6649-thickness*1.754
    s ly 23.072 n2 nity1  %23.3351-thickness*1.754


    # ======== Thick ITMs ======================
    m IXAR 0     1     0 nitx1 nitx2
    s thick_IX 0.15 1.754 nitx2 nx1

    m IYAR 0     1     0 nity1 nity2
    s thick_IY 0.15 1.754 nity2 ny1

    # X arm
    m ITMX 0.996 0.004 0 nx1 nx2
    s sx1 3000 nx2 nx3
    m ETMX 0.999995 5e-06 0 nx3 netx1

    # Y arm
    m ITMY 0.996 0.004 90 ny1 ny2
    s sy1 3000 ny2 ny3
    m ETMY 0.999995 5e-06 90 ny3 nety1

    # ======== Thick ETMs ======================
    s thick_EX 0.15 1.754 netx1 netx2
    m EXAR 0     1     0  netx2 TMSX_gouy_tuner_in
    s TMSX_gouy_tuner 0 TMSX_gouy_tuner_in TMSX
    attr TMSX_gouy_tuner g 0.0

    s thick_EY 0.15 1.754 nety1 nety2
    m EYAR 0     1     0  nety2 TMSY_gouy_tuner_in
    s TMSY_gouy_tuner 0 TMSY_gouy_tuner_in TMSY
    attr TMSY_gouy_tuner g 0.0

    # ========= SRC each mirror loss 45ppm =======
    s sLsr3 15.7386 n4 nsr5
    bs1 SR3 50e-6 45e-6 0 $a nsr5 nsr4 dump dump
    s sLsr2 11.1115 nsr4 nsr3
    bs1 SR2 500e-6 45e-6 0 $a nsr2 nsr3 POS_gouy_tuner_in dump
    s sLsr1 14.7412 nsr2 nsr1
    #s sLsr1 15.2412 nsr2 nsr1
    m1 SRM 0.3 0e-6 $phi_SRM nsr1 AS_gouy_tuner_in

    # 追加したもの
    #bs REFL_dummy_mirror 0.001 0.009 0 0 n_eo5 dump REFL_dummy_mirror_out REFL #このREFLのBSを入れると変になる
    #s REFL_gouy_tuner 0 REFL_dummy_mirror_out REFL_gouy_tuner_out

    s POP_gouy_tuner 0 POP_gouy_tuner_in POP
    s POP2_gouy_tuner 0 POP2_gouy_tuner_in POP2


    # おそらく間違っていると思われるもの
    s POS_gouy_tuner 0 POS_gouy_tuner_in POS
    s AS_gouy_tuner 0 AS_gouy_tuner_in AS
    #attr REFL_gouy_tuner g 0.0
    #attr POP_gouy_tuner g 0.0
    #attr POP2_gouy_tuner g 0.0
    #attr POS_gouy_tuner g 0.0
    #attr AS_gouy_tuner g 0.0
    """
    
    # ASC用
    if sim_conf["kifo_cl_ali"]:
        input_finesse   = (
        """
        # ======== Constants ========================
        const fsb1 16.881M
        const fsb2 45.0159M
        const mfsb1 -16.881M
        const mfsb2 -45.0159M
        const a 0.686
        const pi 3.1415
        
        # おそらく間違っていると思われるもの
        attr REFL_gouy_tuner g 0.0
        attr POP_gouy_tuner g 0.0
        attr POP2_gouy_tuner g 0.0


        # ======== Input optics =====================
        l i1 1 0 n0
        s s_eo0 0 n0 n_eo1
        mod eom1 $fsb1 0.3 1 pm n_eo1 n_eo2
        s s_eo1 0 n_eo2 n_eo3
        mod eom2 $fsb2 0.3 1 pm n_eo3 n_eo4
        s s_eo2 0 n_eo4 n_eo5
        bs REFL_dummy_mirror 0.001 0.009 0 0 n_eo5 dump REFL_dummy_mirror_out REFL
        s REFL_gouy_tuner 0 REFL_dummy_mirror_out REFL_gouy_tuner_out

        ## ======= PRC each mirror loss 45ppm =======
        # PRC

        m1 PRM 0.1 45e-6 0 REFL_gouy_tuner_out npr1
        s sLpr1 14.7615 npr1 npr2
        bs1 PR2 500e-6 45e-6 0 $a npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
        s sLpr2 11.0661 npr3 npr4
        bs1 PR3 50e-6 45e-6 0 $a dump dump npr4 npr5
        s sLpr3 15.7638 npr5 npr6
        s POP_gouy_tuner 0 POP_gouy_tuner_in POP
        s POP2_gouy_tuner 0 POP2_gouy_tuner_in POP2

        # Michelson
        bs MIBS 0.5 0.5 0 45 npr6 n2 n3 n4
        s lx 26.4018 n3 nitx1 %26.6649-thickness*1.754
        s ly 23.072 n2 nity1  %23.3351-thickness*1.754

        # ======== Thick ITMs ======================
        m IXAR 0     1     0 nitx1 nitx2
        s thick_IX 0.15 1.754 nitx2 nx1

        m IYAR 0     1     0 nity1 nity2
        s thick_IY 0.15 1.754 nity2 ny1

        # X arm
        m ITMX 0.996 0.004 0 nx1 nx2
        s sx1 3000 nx2 nx3
        m ETMX 0.999995 5e-06 0 nx3 netx1

        # Y arm
        m ITMY 0.996 0.004 90 ny1 ny2
        s sy1 3000 ny2 ny3
        m ETMY 0.999995 5e-06 90 ny3 nety1

        # ======== Thick ETMs ======================
        s thick_EX 0.15 1.754 netx1 netx2
        m EXAR 0     1     0  netx2 TMSX_gouy_tuner_in
        s TMSX_gouy_tuner 0 TMSX_gouy_tuner_in TMSX
        attr TMSX_gouy_tuner g 0.0

        s thick_EY 0.15 1.754 nety1 nety2
        m EYAR 0     1     0  nety2 TMSY_gouy_tuner_in
        s TMSY_gouy_tuner 0 TMSY_gouy_tuner_in TMSY
        attr TMSY_gouy_tuner g 0.0

        # ========= SRC each mirror loss 45ppm =======
        s sLsr3 15.7386 n4 nsr5
        bs1 SR3 50e-6 45e-6 0 $a nsr5 nsr4 dump dump
        s sLsr2 11.1115 nsr4 nsr3
        bs1 SR2 500e-6 45e-6 0 $a nsr2 nsr3 POS_gouy_tuner_in dump
        s POS_gouy_tuner 0 POS_gouy_tuner_in POS
        attr POS_gouy_tuner g 0.0
        s sLsr1 14.7412 nsr2 nsr1
        m1 SRM 0.3 0e-6 0 nsr1 AS_gouy_tuner_in
        s AS_gouy_tuner 0 AS_gouy_tuner_in AS
        attr AS_gouy_tuner g 0.0

        """
                )

    '''
    input_finesse = """

    #修正済み
    # ======== Constants ========================
    const fsb1 16.881M
    const fsb2 45.0159M
    const mfsb1 -16.881M
    const mfsb2 -45.0159M
    const a 0.686
    const phi_PRM 90
    const pi 3.1415
    const phi_SRM 90

    # ======== Input optics =====================
    l i1 1 0 n0
    s s_eo0 0 n0 n_eo1
    mod eom1 $fsb1 0.3 1 pm n_eo1 n_eo2
    s s_eo1 0 n_eo2 n_eo3
    mod eom2 $fsb2 0.3 1 pm n_eo3 n_eo4
    #s s_eo2 0 n_eo4 n_eo5
    s s_eo2 0 n_eo4 REFL

    ## ======= PRC each mirror loss 45ppm =======
    # PRC
    m1 PRM 0.1 45e-6 $phi_PRM REFL npr1
    s sLpr1 14.7615 npr1 npr2
    bs1 PR2 500e-6 45e-6 0 $a npr3 npr2 POP POP2
    s sLpr2 11.0661 npr3 npr4
    bs1 PR3 50e-6 45e-6 0 $a dump dump npr4 npr5
    s sLpr3 15.7638 npr5 npr6

    # Michelson
    bs MIBS 0.5 0.5 0 45 npr6 n2 n3 n4
    s lx 26.4018 n3 nitx1 %26.6649-thickness*1.754
    s ly 23.072 n2 nity1  %23.3351-thickness*1.754


    # ======== Thick ITMs ======================
    m IXAR 0     1     0 nitx1 nitx2
    s thick_IX 0.15 1.754 nitx2 nx1

    m IYAR 0     1     0 nity1 nity2
    s thick_IY 0.15 1.754 nity2 ny1

    # X arm
    m ITMX 0.996 0.004 0 nx1 nx2
    s sx1 3000 nx2 nx3
    m ETMX 0.999995 5e-06 0 nx3 netx1

    # Y arm
    m ITMY 0.996 0.004 90 ny1 ny2
    s sy1 3000 ny2 ny3
    m ETMY 0.999995 5e-06 90 ny3 nety1

    # ======== Thick ETMs ======================
    s thick_EX 0.15 1.754 netx1 netx2
    m EXAR 0     1     0  netx2 TMSX

    s thick_EY 0.15 1.754 nety1 nety2
    m EYAR 0     1     0  nety2 TMSY

    # ========= SRC each mirror loss 45ppm =======
    s sLsr3 15.7386 n4 nsr5
    bs1 SR3 50e-6 45e-6 0 $a nsr5 nsr4 dump dump
    s sLsr2 11.1115 nsr4 nsr3
    bs1 SR2 500e-6 45e-6 0 $a nsr2 nsr3 POS dump
    s sLsr1 14.7412 nsr2 nsr1
    #s sLsr1 15.2412 nsr2 nsr1
    m1 SRM 0.3 0e-6 $phi_SRM nsr1 AS
    """
    '''

    interferometer = inteferometer_config_list[0]
    type_of_pd_signal = inteferometer_config_list[1]

    # base
    drfpmi_model = finesse.kat()
    drfpmi_model.parse(input_finesse)
    print("drfpmi = \n")
    print(drfpmi_model)
    # model
    model = create_base_by_setting_param(sim_conf, drfpmi_model, interferometer)

    # use HOM
    if sim_conf["k_inf_c_laser_use_hom"]:
        model = hom_extention(sim_conf, model)

    # add xaxis command
    # add PDs
    if sim_conf["kifo_cl_ali"]:
        model.parse("""
        #s POS_gouy_tuner 0 POS_gouy_tuner_in POS
        #s AS_gouy_tuner 0 AS_gouy_tuner_in AS
        attr REFL_gouy_tuner g 0.0
        attr POP_gouy_tuner g 0.0
        attr POP2_gouy_tuner g 0.0
        attr POS_gouy_tuner g 0.0
        attr AS_gouy_tuner g 0.0
        """)

        if sim_conf["kifo_isswan"]:
            # sweep
            if sim_conf["kifo_an_misal_use_dof_sc_1"]:#  mirot_templates
                model = add_dofcommand_mirot_templates_to_model(sim_conf, model)
                model = add_pds_to_model(model, sim_conf, type_of_pd_signal, add_dofcommand_to_model)
            elif sim_conf["kifo_an_misal_use_dof_sc_2"]:#  mirot_optional
                model = add_dofcommand_mirot_optional_to_model(sim_conf, model)
                model = add_pds_to_model(model, sim_conf, type_of_pd_signal, add_dofcommand_to_model)
            elif sim_conf["kifo_swan_gouy_use_dof_sc_1"]:# gouyp_templates
                model = add_dofcommand_gouyp_templates_to_model(sim_conf, model)
                model = add_pds_to_model(model, sim_conf, type_of_pd_signal, add_dofcommand_to_model)
            elif sim_conf["kifo_swan_gouy_use_dof_sc_2"]:# gouyp_optional
                model = add_dofcommand_gouyp_optional_to_model(sim_conf, model)
                model = add_pds_to_model(model, sim_conf, type_of_pd_signal, add_dofcommand_to_model)
            elif sim_conf["swan_dof_dmodp_mirot_radiobox_key_1"]:# dmodp_mirot_templates
                model = add_dofcommand_dmodp_mirot_templates_pd_to_model(sim_conf, model)
            elif sim_conf["swan_dof_dmodp_mirot_radiobox_key_2"]:# dmodp_mirot_optional
                model = add_dofcommand_dmodp_mirot_optional_pd_to_model(sim_conf, model)
            elif sim_conf["swan_dof_dmodp_mirot_fsig_radiobox_key_1"]:# dmodp_mfsig_templates
                model = add_dofcommand_dmodp_mfsig_templates_pd_to_model(sim_conf, model)
            elif sim_conf["swan_dof_dmodp_mirot_fsig_radiobox_key_2"]:# dmodp_mfsig_optional
                model = add_dofcommand_dmodp_mfsig_optional_pd_to_model(sim_conf, model)
        elif sim_conf["kifo_istfan"]:
            # transfer function
            if sim_conf["kifo_istfan_mirot"]:
                if sim_conf["tfan_dof_dmod2_fsig_mirot_radiobox_key_1"]:#dof template
                    model = add_dofcommand_tfan_mirot_templates_pd_to_model(sim_conf, model)
                elif sim_conf["tfan_dof_dmod2_fsig_mirot_radiobox_key_2"]:#dof optional
                    model = add_dofcommand_tfan_mirot_optional_pd_to_model(sim_conf, model)
    elif sim_conf["kifo_cl_len"]:
        model = add_dofcommand_to_model(model, sim_conf, type_of_pd_signal)
        model = add_pds_to_model(model, sim_conf, type_of_pd_signal, add_dofcommand_to_model)

    return model

##################################################################################
# Add xaxis
##################################################################################

# Main function
def add_dofcommand_to_model(base, sim_conf, type_of_pd_signal):
    # lengthのdofを追加
    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])
    plotscale       = "lin"
    if sim_conf["k_inf_c_xaxis_log"]:
        plotscale = "log"
    if sim_conf["kifo_dof_selection"]:
        dof = sim_conf["kifo_dof"]
        if dof=="BS":
            if(type_of_pd_signal=="sw_power" or type_of_pd_signal=="sw_dmod1" or type_of_pd_signal=="sw_amptd"):
                model.parse(
                    """
                    xaxis ITMX phi lin -180 180 1000
                    put* ITMY phi $mx1
                    put* ETMX phi $x1
                    put* ETMY phi $mx1
                    """)
            elif(type_of_pd_signal=="sw_dmodp"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ITMX 10 0
                    fsig sig1 ETMY 10 180
                    fsig sig1 ITMY 10 180
                    variable demod_phase 3
                    xaxis demod_phase abs lin -180 180 1000
                    yaxis lin abs:deg
                    """)
            elif(type_of_pd_signal=="tf_power" or type_of_pd_signal=="tf_dmod2" or type_of_pd_signal=="tf_amptd"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ITMX 10 0
                    fsig sig1 ETMY 10 180
                    fsig sig1 ITMY 10 180
                    xaxis sig1 f log 10 100 1000
                    yaxis lin abs:deg
                    """)
            elif(type_of_pd_signal=="st_type1" or type_of_pd_signal=="st_type2"):
                model.parse("""
                    fsig sig1 lx 1 0
                    fsig sig1 ly 1 180
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis log abs:deg
                    """)
        elif dof=="DARM":
            if(type_of_pd_signal=="sw_power" or type_of_pd_signal=="sw_dmod1" or type_of_pd_signal=="sw_amptd"):
                model.parse("""
                    xaxis ETMX phi lin -180 180 1000
                    put* ETMY phi $mx1
                    """)
            elif(type_of_pd_signal=="sw_dmodp"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ETMY 10 180
                    variable demod_phase 3
                    xaxis demod_phase abs lin -180 180 1000
                    yaxis lin abs:deg
                    """)
            elif(type_of_pd_signal=="tf_power" or type_of_pd_signal=="tf_dmod2" or type_of_pd_signal=="tf_amptd"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ETMY 10 180
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis lin abs:deg
                    """)
            elif(type_of_pd_signal=="st_type1" or type_of_pd_signal=="st_type2"):
                model.parse("""
                    fsig sig1 sx1 1 0
                    fsig sig1 sy1 1 180
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis log abs:deg
                    """)
        elif dof=="CARM":
            if(type_of_pd_signal=="sw_power" or type_of_pd_signal=="sw_dmod1" or type_of_pd_signal=="sw_amptd"):
                model.parse(
                    """
                    xaxis ETMX phi lin -180 180 1000
                    put* ETMY phi $x1
                    """)
            elif(type_of_pd_signal=="sw_dmodp"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ETMY 10 0
                    variable demod_phase 3
                    xaxis demod_phase abs lin -180 180 1000
                    yaxis lin abs:deg
                    """)
            if(type_of_pd_signal=="tf_power" or type_of_pd_signal=="tf_dmod2" or type_of_pd_signal=="tf_amptd" or type_of_pd_signal=="st_type1"):
                model.parse("""
                    fsig sig1 ETMX 10 0
                    fsig sig1 ETMY 10 0
                    xaxis sig1 f log 10 100 1000
                    yaxis lin abs:deg
                                """)
            elif(type_of_pd_signal=="st_type1" or type_of_pd_signal=="st_type2"):
                model.parse("""
                    fsig sig1 sx1 1 0
                    fsig sig1 sy1 1 0
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis log abs:deg
                    """)
        elif dof=="PRCL":
            if(type_of_pd_signal=="sw_power" or type_of_pd_signal=="sw_dmod1" or type_of_pd_signal=="sw_amptd"):
                model.parse(
                    """
                    xaxis PRM phi lin -180 180 1000
                    """)
            elif(type_of_pd_signal=="sw_dmodp"):
                model.parse("""
                    fsig sig1 PRM 1 0
                    variable demod_phase 3
                    xaxis demod_phase abs lin -180 180 1000
                    yaxis lin abs:deg
                    """)
            if(type_of_pd_signal=="tf_power" or type_of_pd_signal=="tf_dmod2" or type_of_pd_signal=="tf_amptd" or type_of_pd_signal=="st_type1"):
                model.parse("""
                    fsig sig1 PRM 1 0
                    xaxis sig1 f log 10 100 1000
                    yaxis lin abs:deg
                                """)
            elif(type_of_pd_signal=="st_type1" or type_of_pd_signal=="st_type2"):
                model.parse("""
                    fsig sig1 sLpr1 1 0
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis log abs:deg
                    """)
        elif dof=="SRCL":
            if(type_of_pd_signal=="sw_power" or type_of_pd_signal=="sw_dmod1" or type_of_pd_signal=="sw_amptd"):
                model.parse(
                    """
                    var tuning 0.0
                    xaxis tuning phi lin -180 180 1000
                    put SRM phi $x1
                    """)
            elif(type_of_pd_signal=="sw_dmodp"):
                model.parse("""
                    fsig sig1 SRM 1 0
                    variable demod_phase 3
                    xaxis demod_phase abs lin -180 180 1000
                    yaxis lin abs:deg
                    """)
            if(type_of_pd_signal=="tf_power" or type_of_pd_signal=="tf_dmod2" or type_of_pd_signal=="tf_amptd" or type_of_pd_signal=="st_type1"):
                model.parse("""
                    fsig sig1 SRM 1 0
                    xaxis sig1 f log 10 100 1000
                    yaxis lin abs:deg
                                """)
            elif(type_of_pd_signal=="st_type1" or type_of_pd_signal=="st_type2"):
                model.parse("""
                    fsig sig1 sLsr1 1 0
                    xaxis sig1 f log 0.01 1000 1000
                    yaxis log abs:deg
                    """)
    elif sim_conf["kifo_misal_mirror_selection"]:
        if sim_conf["kifo_an_misal_use_dof_sc_1"]:#  mirot_templates
            model = add_dofcommand_mirot_templates_to_model(sim_conf, model)
            #model = add_an_misal_xaxisconfig_to_model(model, sim_conf)
        elif sim_conf["kifo_an_misal_use_dof_sc_2"]:#  mirot_optional
            model = add_dofcommand_mirot_optional_to_model(sim_conf, model)
        elif sim_conf["kifo_swan_gouy_use_dof_sc_1"]:# gouyp_templates
            model = add_dofcommand_gouyp_templates_to_model(sim_conf, model)
        elif sim_conf["kifo_swan_gouy_use_dof_sc_2"]:# gouyp_optional
            model = add_dofcommand_gouyp_optional_to_model(sim_conf, model)

    model.xaxis.scale  = plotscale
    model.xaxis.limits = [xaxis_range_beg, xaxis_range_end]
    model.xaxis.steps  = samplingnum
    return model

# sub functions
# these 4 functions are contained in "add_dofcommand_to_model()" function. 
def add_dofcommand_mirot_templates_to_model(sim_conf, base):# kifo_an_misal_use_dof_sc_1  misalign_dof_box_1
    model = base.deepcopy()
    x_arm_cavity = sim_conf["kifo_an_misal_resoaxis_xarm"]# "tilt+", "shift+",...
    y_arm_cavity = sim_conf["kifo_an_misal_resoaxis_yarm"]# "tilt+", "shift+",...
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])
    an_axis_x    = sim_conf["kifo_an_misal_xarm_rotate_direction"]
    an_axis_y    = sim_conf["kifo_an_misal_yarm_rotate_direction"]
    # x cavity
    if an_axis_x == "yaw":
        #an_axis="x-split"
        xarm_beta   ="xbeta"
    elif an_axis_x == "pitch":
        #an_axis="y-split"
        xarm_beta   ="ybeta"
    else:
        xarm_beta="xbeta"
    # y cavity
    if an_axis_y == "yaw":
        #an_axis="x-split"
        yarm_beta   ="xbeta"
    elif an_axis_y == "pitch":
        #an_axis="y-split"
        yarm_beta   ="ybeta"
    else:
        yarm_beta="xbeta"

    model.parse(
    """
    variable deltax 1 
    xaxis deltax abs lin %s %s %s 
    """%(xaxis_range_beg, xaxis_range_end, samplingnum)
            )
    # Xarm
    if x_arm_cavity =="aligned":
        pass
    elif x_arm_cavity =="shift +":
        model.parse(
        """
        put* ITMX %s $x1
        put* ETMX %s $mx1
        """%(xarm_beta, xarm_beta)
        )
        pass
    elif x_arm_cavity =="shift -":
        model.parse(
        """
        put* ITMX %s $mx1
        put* ETMX %s $x1
        """%(xarm_beta, xarm_beta)
        )
        pass
    elif x_arm_cavity =="tilt +":
        model.parse(
        """
        put* ITMX %s $x1
        put* ETMX %s $x1
        """%(xarm_beta, xarm_beta)
        )
    elif x_arm_cavity =="tilt -":
        model.parse(
        """
        put* ITMX %s $mx1
        put* ETMX %s $mx1
        """%(xarm_beta, xarm_beta)
        )
    else:
        print("dofのxarmが選択できていません")
    # Yarm
    if y_arm_cavity =="aligned":
        pass
    elif y_arm_cavity =="shift +":
        model.parse(
        """
        put* ITMY %s $x1
        put* ETMY %s $mx1
        """%(yarm_beta, yarm_beta)
        )
    elif y_arm_cavity =="shift -":
        model.parse(
        """
        put* ITMY %s $mx1
        put* ETMY %s $x1
        """%(yarm_beta, yarm_beta)
        )
    elif y_arm_cavity =="tilt +":
        model.parse(
        """
        put* ITMY %s $x1
        put* ETMY %s $x1
        """%(yarm_beta, yarm_beta)
        )
    elif y_arm_cavity =="tilt -":
        model.parse(
        """
        put* ITMY %s $mx1
        put* ETMY %s $mx1
        """%(yarm_beta, yarm_beta)
        )
    else:
        print("dofのyarmが選択できていません")
    print(model)
    return model

def add_dofcommand_mirot_optional_to_model(sim_conf, base):#  kifo_an_misal_use_dof_sc_2  misalign_dof_box_2
    model       = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    model.parse(
    """
    variable deltax 1 
    xaxis deltax abs lin %s %s %s 
    """%(xaxis_range_beg, xaxis_range_end, samplingnum)
            )

    for i in range(sim_conf["kgui_an_misal_plotnum"]):# kgui_an_misal_plotnum = an_misal_plotnum
        num = i+1
        print("now num = " + str(num))
        #an_mirror = sim_conf["kifo_misalign_mirror"]
        an_mirror  = sim_conf["kifo_an_misal_mirr_combo%s"%str(num)]
        an_plmi    = sim_conf["kifo_an_misal_plmi_combo%s"%str(num)]
        #an_axis   = sim_conf["kifo_misalign_axes"]
        an_axis    = sim_conf["kifo_an_misal_drct_combo%s"%str(num)]
        beta="xbeta"
        if an_axis == "yaw":
            #an_axis="x-split"
            beta   ="xbeta"
        if an_axis == "pitch":
            #an_axis="y-split"
            beta   ="ybeta"
        # plus pr minus
        if an_plmi=="+":
            model.parse(
            """
            put* %s %s $x1
            """%(an_mirror, beta)
            )
        elif an_plmi=="-":
            model.parse(
            """
            put* %s %s $mx1
            """%(an_mirror, beta)
            )
    print("チェック")
    print(model)
    return model

def add_dofcommand_gouyp_templates_to_model(sim_conf, base):# kifo_swan_gouy_use_dof_sc_1 misalign_dof_box_1_gouyp
    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    xarm_angle      = sim_conf["kifo_an_misal_gouyp_rotetean_xarm"]
    print(sim_conf["kifo_an_misal_gouyp_rotetean_xarm"])
    xarm_axis       = sim_conf["kifo_an_misal_gouyp_xarm_rotate_direction"]
    xarm_beta       = "xbeta"

    yarm_angle      = sim_conf["kifo_an_misal_gouyp_rotetean_yarm"]
    print(sim_conf["kifo_an_misal_gouyp_rotetean_yarm"])
    yarm_axis       = sim_conf["kifo_an_misal_gouyp_yarm_rotate_direction"]
    yarm_beta       = "xbeta"

    if xarm_axis == "yaw":
        #an_axis="x-split"
        xarm_beta   ="xbeta"
    if xarm_axis == "pitch":
        #an_axis="y-split"
        xarm_beta   ="ybeta"
    if yarm_axis == "yaw":
        #an_axis="x-split"
        yarm_beta   ="xbeta"
    if yarm_axis == "pitch":
        #an_axis="y-split"
        yarm_beta   ="ybeta"

    model.parse(
    """
    variable deltax 1 
    xaxis deltax abs lin %s %s %s 
    """%(xaxis_range_beg, xaxis_range_end, samplingnum)
            )

    # Xarm
    if sim_conf["kifo_an_misal_gouyp_resoaxis_xarm"]=="shift +": #shift+:
        model.parse(
        """
        attr ITMX %s %s
        attr ETMX %s -%s
        """%(xarm_beta, xarm_angle
            ,xarm_beta, xarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_xarm"]=="shift -": #shift-:
        model.parse(
        """
        attr ITMX %s -%s
        attr ETMX %s %s
        """%(xarm_beta, xarm_angle
            ,xarm_beta, xarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_xarm"]=="tilt +": #tilt+:
        model.parse(
        """
        attr ITMX %s %s
        attr ETMX %s %s
        """%(xarm_beta, xarm_angle
            ,xarm_beta, xarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_xarm"]=="tilt -": #tilt-:
        model.parse(
        """
        attr ITMX %s -%s
        attr ETMX %s -%s
        """%(xarm_beta, xarm_angle
            ,xarm_beta, xarm_angle)
        )
    # Yarm
    if sim_conf["kifo_an_misal_gouyp_resoaxis_yarm"]=="shift +":#shift+:
        model.parse(
        """
        attr ITMY %s %s
        attr ETMY %s -%s
        """%(yarm_beta, yarm_angle
            ,yarm_beta, yarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_yarm"]=="shift -":#shift-:
        model.parse(
        """
        attr ITMY %s -%s
        attr ETMY %s %s
        """%(yarm_beta, yarm_angle
            ,yarm_beta, yarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_yarm"]=="tilt +":#tilt+:
        model.parse(
        """
        attr ITMY %s %s
        attr ETMY %s %s
        """%(yarm_beta, yarm_angle
            ,yarm_beta, yarm_angle)
        )
    if sim_conf["kifo_an_misal_gouyp_resoaxis_yarm"]=="tilt +":#tilt-:
        model.parse(
        """
        attr ITMY %s -%s
        attr ETMY %s -%s
        """%(yarm_beta, yarm_angle
            ,yarm_beta, yarm_angle)
        )
    tmp_texts = [""]
    for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):
        num = i+1
        port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
        tmp_texts.append(
        """
        put* %s_gouy_tuner gx $x1
        put* %s_gouy_tuner gy $x1
        """%(port, port)
        )
    tmp_texts = set(tmp_texts)
    s = '\n'.join(tmp_texts)
    model.parse(s)
    return model

def add_dofcommand_gouyp_optional_to_model(sim_conf, base):#  kifo_swan_gouy_use_dof_sc_2 misalign_dof_box_2_gouypmisalign_dof_box_2
    model       = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    model.parse(
    """
    variable deltax 1 
    xaxis deltax abs lin %s %s %s 
    """%(xaxis_range_beg, xaxis_range_end, samplingnum)
            )
    for i in range(sim_conf["kgui_swan_gouyp_plotnum"]):# kgui_an_misal_plotnum = an_misal_plotnum
        num = i+1
        print("now num = " + str(num))
        #an_mirror = sim_conf["kifo_misalign_mirror"]
        an_mirror  = sim_conf["kifo_swan_gouyp_a_mirr_combo%s"%str(num)]
        an_plmi    = sim_conf["kifo_swan_gouyp_a_plmi_combo%s"%str(num)]
        #an_axis   = sim_conf["kifo_misalign_axes"]
        an_axis    = sim_conf["kifo_swan_gouyp_a_drct_combo%s"%str(num)]
        angle      = sim_conf["kifo_swan_gouyp_a_rtan_combo%s"%str(num)]
        beta       = "xbeta"
        if an_axis == "yaw":
            #an_axis="x-split"
            beta   ="xbeta"
        if an_axis == "pitch":
            #an_axis="y-split"
            beta   ="ybeta"
        # plus pr minus
        if an_plmi=="+":
            model.parse(
            """
            attr %s %s %s
            """%(an_mirror, beta, angle)
            )
        elif an_plmi=="-":
            model.parse(
            """
            attr %s %s -%s
            """%(an_mirror, beta, angle)
            )

    tmp_texts = [""]
    for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):
        num = i+1
        port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
        tmp_texts.append(
        """
        put* %s_gouy_tuner gx $x1
        put* %s_gouy_tuner gy $x1
        """%(port, port)
        )
    tmp_texts = set(tmp_texts)
    s = '\n'.join(tmp_texts)
    model.parse(s)
    return model

def add_dofcommand_dmodp_mirot_templates_pd_to_model(sim_conf, base):
    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    #Xarn
    template_dof_x = sim_conf["swan_dof_dmodp_mirot_dc_template_x_cavity_combo_key"]
    xarm_angle     = sim_conf["swan_dof_dmodp_mirot_dc_template_x_angle_combo_key"]
    xarm_axis      = sim_conf["swan_dof_dmodp_mirot_dc_template_x_direction_combo_key"]
    if xarm_axis=="yaw":
        xarm_axis="xbeta"
    elif xarm_axis=="pitch":
        xarm_axis="ybeta"
    if template_dof_x =="shift+":
        model.parse(f"attr ITMX {xarm_axis} {xarm_angle}")
        model.parse(f"attr ETMX {xarm_axis} -{xarm_angle}")
    elif template_dof_x =="shift-":
        model.parse(f"attr ITMX {xarm_axis} -{xarm_angle}")
        model.parse(f"attr ETMX {xarm_axis} {xarm_angle}")
    elif template_dof_x =="tilt+":
        model.parse(f"attr ITMX {xarm_axis} {xarm_angle}")
        model.parse(f"attr ETMX {xarm_axis} {xarm_angle}")
    elif template_dof_x =="tilt-":
        model.parse(f"attr ITMX {xarm_axis} -{xarm_angle}")
        model.parse(f"attr ETMX {xarm_axis} -{xarm_angle}")
    #Yarn
    template_dof_y = sim_conf["swan_dof_dmodp_mirot_dc_template_y_cavity_combo_key"]
    yarm_angle     = sim_conf["swan_dof_dmodp_mirot_dc_template_y_angle_combo_key"]
    yarm_axis      = sim_conf["swan_dof_dmodp_mirot_dc_template_y_direction_combo_key"]
    if yarm_axis=="yaw":
        yarm_axis="xbeta"
    elif yarm_axis=="pitch":
        yarm_axis="ybeta"
    if template_dof_y =="shift+":
        model.parse(f"attr ITMY {yarm_axis} {yarm_angle}")
        model.parse(f"attr ETMY {yarm_axis} -{yarm_angle}")
    elif template_dof_y =="shift-":
        model.parse(f"attr ITMY {yarm_axis} -{yarm_angle}")
        model.parse(f"attr ETMY {yarm_axis} {yarm_angle}")
    elif template_dof_y =="tilt+":
        model.parse(f"attr ITMY {yarm_axis} {yarm_angle}")
        model.parse(f"attr ETMY {yarm_axis} {yarm_angle}")
    elif template_dof_y =="tilt-":
        model.parse(f"attr ITMY {yarm_axis} -{yarm_angle}")
        model.parse(f"attr ETMY {yarm_axis} -{yarm_angle}")
    
    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd1 {pdname} {dmodf} 0 {port}")
            model.parse(f"pdtype pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###
    model.parse(f"variable deltax 1\n")
    model.parse(f"xaxis deltax abs lin {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n")
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put* sig1 f $x1\n")

    return model

def add_dofcommand_dmodp_mirot_optional_pd_to_model(sim_conf, base):
    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    # rotation
    for num in range(10):
        if sim_conf[f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_checkbox_{num}"]:
            mirror    = sim_conf[f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Mirror_combo_{num}"]
            angle     = sim_conf[f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_angle_combo_{num}"]
            direction = sim_conf[f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_direction_combo_{num}"]
            if direction=="yaw":
                direction="xbeta"
            if direction=="pitch":
                direction="ybeta"
            model.parse(f"attr {mirror} {direction} {angle}")
    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd1 {pdname} {dmodf} 0 {port}")
            model.parse(f"pdtype pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###
    model.parse(f"variable deltax 1\n")
    model.parse(f"xaxis deltax abs lin {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n")
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put* {pdname} phase1 $x1\n")
    return model

def add_dofcommand_dmodp_mfsig_templates_pd_to_model(sim_conf, base):

    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    #Xarn
    template_dof_x = sim_conf["swan_dof_dmodp_mfsig_fsig_template_x_cavity_combo_key"]
    xarm_freq      = sim_conf["swan_dof_dmodp_mfsig_fsig_template_x_frequency_combo_key"]
    xarm_freq      = xarm_freq.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
    xarm_freq      = xarm_freq.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
    xarm_axis      = sim_conf["swan_dof_dmodp_mfsig_fsig_template_x_direction_combo_key"]
    if xarm_axis=="yaw":
        xarm_axis="xbeta"
    elif xarm_axis=="pitch":
        xarm_axis="ybeta"
    if template_dof_x =="shift+":
        model.parse(f"fsig sig1 ITMX {xarm_axis} {xarm_freq} 0")
        model.parse(f"fsig sig1 ETMX {xarm_axis} {xarm_freq} 180")
    elif template_dof_x =="shift-":
        model.parse(f"fsig sig1 ITMX {xarm_axis} {xarm_freq} 180")
        model.parse(f"fsig sig1 ETMX {xarm_axis} {xarm_freq} 0")
    elif template_dof_x =="tilt+":
        model.parse(f"fsig sig1 ITMX {xarm_axis} {xarm_freq} 0")
        model.parse(f"fsig sig1 ETMX {xarm_axis} {xarm_freq} 0")
    elif template_dof_x =="tilt-":
        model.parse(f"fsig sig1 ITMX {xarm_axis} {xarm_freq} 180")
        model.parse(f"fsig sig1 ETMX {xarm_axis} {xarm_freq} 180")
    #Yarn
    template_dof_y = sim_conf["swan_dof_dmodp_mfsig_fsig_template_y_cavity_combo_key"]
    yarm_freq      = sim_conf["swan_dof_dmodp_mfsig_fsig_template_y_frequency_combo_key"]
    yarm_freq      = yarm_freq.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
    yarm_freq      = yarm_freq.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
    yarm_axis      = sim_conf["swan_dof_dmodp_mfsig_fsig_template_y_direction_combo_key"]
    if yarm_axis=="yaw":
        yarm_axis="xbeta"
    elif yarm_axis=="pitch":
        yarm_axis="ybeta"
    if template_dof_y =="shift+":
        model.parse(f"fsig sig1 ITMY {yarm_axis} {yarm_freq} 0")
        model.parse(f"fsig sig1 ETMY {yarm_axis} {yarm_freq} 180")
    elif template_dof_y =="shift-":
        model.parse(f"fsig sig1 ITMY {yarm_axis} {yarm_freq} 180")
        model.parse(f"fsig sig1 ETMY {yarm_axis} {yarm_freq} 0")
    elif template_dof_y =="tilt+":
        model.parse(f"fsig sig1 ITMY {yarm_axis} {yarm_freq} 0")
        model.parse(f"fsig sig1 ETMY {yarm_axis} {yarm_freq} 0")
    elif template_dof_y =="tilt-":
        model.parse(f"fsig sig1 ITMY {yarm_axis} {yarm_freq} 180")
        model.parse(f"fsig sig1 ETMY {yarm_axis} {yarm_freq} 180")
    
    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf      = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf      = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd1 {pdname} {dmodf} 0 {port}")
            model.parse(f"pdtype pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###
    model.parse(f"variable deltax 1\n")
    model.parse(f"xaxis deltax abs lin {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n")
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put* {pdname} phase1 $x1\n")


    return model

def add_dofcommand_dmodp_mfsig_optional_pd_to_model(sim_conf, base):
    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])

    # rotation
    for num in range(10):
        if sim_conf[f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_checkbox_{num}"]:
            mirror    = sim_conf[f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Mirror_combo_{num}"]
            fsig_freq = sim_conf[f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_angle_combo_{num}"]
            fsig_freq = fsig_freq.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            fsig_freq = fsig_freq.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            direction = sim_conf[f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_direction_combo_{num}"]
            polality  = sim_conf[f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Fsig_polarity_combo_{num}"]
            if direction=="yaw":
                direction="xbeta"
            if direction=="pitch":
                direction="ybeta"
            model.parse(f"fsig sig1 {mirror} {direction} {fsig_freq} {polality}")
    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd1 {pdname} {dmodf} 0 {port}")
            model.parse(f"pdtype pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###
    model.parse(f"variable deltax 1\n")
    model.parse(f"xaxis deltax abs lin {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n")
    for num in range(10):
        if sim_conf[f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}"]:
            port       = sim_conf[f"kifo_swan_dmodp_detector_select_box_port_combo_{num}"]
            split_axis = sim_conf[f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}"]
            dmodf      = sim_conf[f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            gouy       = sim_conf[f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd1_{dmodf}_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put* {pdname} phase1 $x1\n")
    return model

def add_dofcommand_tfan_mirot_templates_pd_to_model(sim_conf, base):

    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])
    #Xarn
    template_dof_x = sim_conf["tfan_dof_dmod2_mfsig_template_x_cavity_combo_key"]
    xarm_axis      = sim_conf["tfan_dof_dmod2_mfsig_template_x_direction_combo_key"]
    if xarm_axis=="yaw":
        xarm_axis="xbeta"
    elif xarm_axis=="pitch":
        xarm_axis="ybeta"
    if template_dof_x =="shift+":
        model.parse(f"fsig sig1 ITMX {xarm_axis} 10 0")
        model.parse(f"fsig sig1 ETMX {xarm_axis} 10 180")
    elif template_dof_x =="shift-":
        model.parse(f"fsig sig1 ITMX {xarm_axis} 10 180")
        model.parse(f"fsig sig1 ETMX {xarm_axis} 10 0")
    elif template_dof_x =="tilt+":
        model.parse(f"fsig sig1 ITMX {xarm_axis} 10 0")
        model.parse(f"fsig sig1 ETMX {xarm_axis} 10 0")
    elif template_dof_x =="tilt-":
        model.parse(f"fsig sig1 ITMX {xarm_axis} 10 180")
        model.parse(f"fsig sig1 ETMX {xarm_axis} 10 180")
    #Yarn
    template_dof_y = sim_conf["tfan_dof_dmod2_mfsig_template_y_cavity_combo_key"]
    yarm_axis      = sim_conf["tfan_dof_dmod2_mfsig_template_y_direction_combo_key"]
    if yarm_axis=="yaw":
        yarm_axis="xbeta"
    elif yarm_axis=="pitch":
        yarm_axis="ybeta"
    if template_dof_y =="shift+":
        model.parse(f"fsig sig1 ITMY {yarm_axis} 10 0")
        model.parse(f"fsig sig1 ETMY {yarm_axis} 10 180")
    elif template_dof_y =="shift-":
        model.parse(f"fsig sig1 ITMY {yarm_axis} 10 180")
        model.parse(f"fsig sig1 ETMY {yarm_axis} 10 0")
    elif template_dof_y =="tilt+":
        model.parse(f"fsig sig1 ITMY {yarm_axis} 10 0")
        model.parse(f"fsig sig1 ETMY {yarm_axis} 10 0")
    elif template_dof_y =="tilt-":
        model.parse(f"fsig sig1 ITMY {yarm_axis} 10 180")
        model.parse(f"fsig sig1 ETMY {yarm_axis} 10 180")

    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}"]:
            port       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_port_combo_{num}"]
            dmodf      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            dmodp      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_combo_{num}"]
            gouy       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_gouy_phase_combo_{num}"]
            split_axis = sim_conf[f"kifo_tfan_dmod2_pd_select_box_split_axis_combo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd2_{dmodf}_dmod_phase({dmodp}deg)_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd2 {pdname} {dmodf} {dmodp} 10 0 {port}")
            model.parse(f"pdtype {pdname} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###

    for num in range(10):
        if sim_conf[f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}"]:
            print(sim_conf[f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}"])
            port       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_port_combo_{num}"]
            dmodf      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            dmodp      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_combo_{num}"]
            gouy       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_gouy_phase_combo_{num}"]
            split_axis = sim_conf[f"kifo_tfan_dmod2_pd_select_box_split_axis_combo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd2_{dmodf}_dmod_phase({dmodp}deg)_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put {pdname} f2 $x1\n")
    code  = f"xaxis sig1 f log {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n"
    model.parse(code)


    return model

def add_dofcommand_tfan_mirot_optional_pd_to_model(sim_conf, base):

    model           = base.deepcopy()
    xaxis_range_beg = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_beg'])
    xaxis_range_end = change_nums_unit_str_to_float(sim_conf['k_inf_c_xaxis_range_end'])
    samplingnum     = change_nums_unit_str_to_float(sim_conf['k_inf_c_samplingnum'])


    # rotation
    for num in range(10):
        if sim_conf[f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_checkbox_{num}"]:
            mirror    = sim_conf[f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Mirror_combo_{num}"]
            direction = sim_conf[f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Rotation_direction_combo_{num}"]
            polality  = sim_conf[f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Fsig_polarity_combo_{num}"]
            if direction=="yaw":
                direction="xbeta"
            if direction=="pitch":
                direction="ybeta"
            model.parse(f"fsig sig1 {mirror} {direction} 10 {polality}")

    ###
    # PDs
    ###
    for num in range(10):
        if sim_conf[f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}"]:
            port       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_port_combo_{num}"]
            dmodf      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            dmodp      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_combo_{num}"]
            gouy       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_gouy_phase_combo_{num}"]
            split_axis = sim_conf[f"kifo_tfan_dmod2_pd_select_box_split_axis_combo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd2_{dmodf}_dmod_phase({dmodp}deg)_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"pd2 {pdname} {dmodf} {dmodp} 10 0 {port}")
            model.parse(f"pdtype {pdname} {split_axis}")
            model.parse(f"attr {port}_gouy_tuner g {gouy}")
    
    ###
    # xaxis
    ###
    for num in range(10):
        if sim_conf[f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}"]:
            port       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_port_combo_{num}"]
            dmodf      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_combo_{num}"]
            dmodf = dmodf.replace("fsb1",sim_conf["k_inf_c_f1_mod_frequency"])
            dmodf = dmodf.replace("fsb2",sim_conf["k_inf_c_f2_mod_frequency"])
            dmodp      = sim_conf[f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_combo_{num}"]
            gouy       = sim_conf[f"kifo_tfan_dmod2_pd_select_box_gouy_phase_combo_{num}"]
            split_axis = sim_conf[f"kifo_tfan_dmod2_pd_select_box_split_axis_combo_{num}"]
            if split_axis=="yaw":
                split_axis="x-split"
            if split_axis=="pitch":
                split_axis="y-split"
            pdname = f"pd2_{dmodf}_dmod_phase({dmodp}deg)_{split_axis}_gouy({gouy}rad)_{port}"
            model.parse(f"put {pdname} f2 $x1\n")
    code  = f"xaxis sig1 f log {xaxis_range_beg} {xaxis_range_end} {samplingnum}\n"
    model.parse(code)

    return model

##################################################################################
# Add PD
##################################################################################

def parse_an_misal_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq):
    tmp_model = model.deepcopy()
    if an_pd_axis == "yaw":
        an_pd_axis="x-split"
        beta   ="xbeta"
    if an_pd_axis == "pitch":
        an_pd_axis="y-split"
        beta   ="ybeta"
    tmp_model.parse("""
    ad amptd_an_misal_%s_%s_%s_%s_%s %s %s %s %s
    pdtype amptd_an_misal_%s_%s_%s_%s_%s %s
    """%(port, n_mode, m_mode, freq, an_pd_axis, n_mode, m_mode, freq, port
        ,port, n_mode, m_mode, freq,an_pd_axis, an_pd_axis))
    return tmp_model

def parse_swan_gouyp_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq):
    tmp_model = model.deepcopy()
    if an_pd_axis == "yaw":
        an_pd_axis="x-split"
        #beta   ="xbeta"
    if an_pd_axis == "pitch":
        an_pd_axis="y-split"
        #beta   ="ybeta"
    tmp_model.parse("""
    ad amptd_swan_gouyp_%s_%s_%s_%s_%s %s %s %s %s
    pdtype amptd_swan_gouyp_%s_%s_%s_%s_%s %s
    """%(port, n_mode, m_mode, freq, an_pd_axis, n_mode, m_mode, freq, port
        ,port, n_mode, m_mode, freq, an_pd_axis, an_pd_axis))
    return tmp_model

def parse_an_misal_dmod1_kat(model, sim_conf, an_pd_axis, freq, phase, port):
    tmp_model = model.deepcopy()
    optimal_phase = get_first_mixer_optimal_phase(tmp_model, sim_conf, port, freq)

    if an_pd_axis == "yaw":
        an_pd_axis="x-split"
        beta   ="xbeta"
    if an_pd_axis == "pitch":
        an_pd_axis="y-split"
        beta   ="ybeta"

    if phase=="optimal+I":
        freq_value = replace_chr(freq, sim_conf)
        tmp_model.parse("""
        pd1 pd_an_misal_dmod1_%s_%s_%s_%s %s %s %s
        pdtype pd_an_misal_dmod1_%s_%s_%s_%s %s
        """%(port,freq,"optimalI",an_pd_axis, freq_value,optimal_phase, port
            ,port,freq,"optimalI",an_pd_axis, an_pd_axis))
    elif phase=="optimal+Q":
        freq_value = replace_chr(freq, sim_conf)
        phase = eval("%s + 90"%optimal_phase)
        tmp_model.parse("""
        pd1 pd_an_misal_dmod1_%s_%s_%s_%s %s %s %s
        pdtype pd_an_misal_dmod1_%s_%s_%s_%s %s
        """%(port,freq,"optimalQ",an_pd_axis, freq_value,phase, port
            ,port,freq,"optimalQ",an_pd_axis, an_pd_axis))
    else:
        freq_value = replace_chr(freq, sim_conf)
        tmp_model.parse("""
        pd1 pd_an_misal_dmod1_%s_%s_%s_%s %s %s %s
        pdtype pd_an_misal_dmod1_%s_%s_%s_%s %s
        """%(port,freq,phase,an_pd_axis, freq_value,phase, port
            ,port,freq,phase,an_pd_axis, an_pd_axis))
    return tmp_model

def parse_an_misal_attr_kat(model, an_mirror, an_axis):
    tmp_model = model.deepcopy()
    if an_axis == "yaw":
        an_axis="x-split"
        beta   ="xbeta"
    if an_axis == "pitch":
        an_axis="y-split"
        beta   ="ybeta"
    tmp_model.parse("""
    attr %s %s 0
    """%(an_mirror,beta))
    return tmp_model

def parse_an_misal_pd_kat(model, an_pd_axis, port):
    tmp_model = model.deepcopy()
    if an_pd_axis == "yaw":
        an_pd_axis="x-split"
    if an_pd_axis == "pitch":
        an_pd_axis="y-split"
    tmp_model.parse("""
    pd pd_an_misal_%s_%s %s
    pdtype pd_an_misal_%s_%s %s
    """%(port,an_pd_axis,port
        ,port,an_pd_axis,an_pd_axis))
    return tmp_model

def parse_an_misal_gp_kat(model, port, deg, direction="both"):
    tmp_model = model.deepcopy()
    if direction=="both":
        tmp_model.parse("""
        attr %s_gouy_tuner g %s
        """%(port,deg))
    return tmp_model
# sub functions

def parse_sw_amptd_kat(model, sim_conf, hom_flag, n_mode, m_mode, port):
    """
        sw_amptd:sweep amplitude

        put PDs for length-sweep-amplitude_detector simulation

        Attributes
        ----------
        名前 : 型
            説明

        Returns
        -------
        model : 型
            model
    """
    # default value
    fsb1 = sim_conf["k_inf_c_f1_mod_frequency"]
    mfsb1 = "-"+sim_conf["k_inf_c_f1_mod_frequency"]
    fsb2 = sim_conf["k_inf_c_f2_mod_frequency"]
    mfsb2 = "-"+sim_conf["k_inf_c_f2_mod_frequency"]
    #n_mode = sim_conf["kifo_sw_amptd_ad_hom_n_order"]
    #m_mode = sim_conf["kifo_sw_amptd_ad_hom_m_order"]
    #model.parse("ad car_ad_REFL 0 REFL")
    if hom_flag:
        if sim_conf["kifo_put_car_sw_amptd_flag"]:# or kifo_put_car_tf_amptd_flag:
            model.parse("ad car_ad_%s_%s_%s %s %s 0 %s" % (n_mode, m_mode, port, n_mode, m_mode, port))
        if sim_conf["kifo_put_f1u_sw_amptd_flag"]:# or kifo_put_f1u_tf_amptd_flag:
            model.parse("ad fsb1_upper_ad_%s_%s_%s %s %s %s %s" % (n_mode, m_mode, port, n_mode, m_mode, fsb1, port))
        if sim_conf["kifo_put_f1l_sw_amptd_flag"]:# or kifo_put_f1l_tf_amptd_flag:
            model.parse("ad fsb1_lower_ad_%s_%s_%s %s %s %s %s" % (n_mode, m_mode, port, n_mode, m_mode, mfsb1, port))
        if sim_conf["kifo_put_f2u_sw_amptd_flag"]:# or kifo_put_f2u_tf_amptd_flag:
            model.parse("ad fsb2_upper_ad_%s_%s_%s %s %s %s %s" % (n_mode, m_mode, port, n_mode, m_mode, fsb2, port))
        if sim_conf["kifo_put_f2l_sw_amptd_flag"]:# or kifo_put_f2l_tf_amptd_flag:
            model.parse("ad fsb2_lower_ad_%s_%s_%s %s %s %s %s" % (n_mode, m_mode, port, n_mode, m_mode, mfsb2, port))
    else:
        if sim_conf["kifo_put_car_sw_amptd_flag"]:# or kifo_put_car_tf_amptd_flag:
            model.parse("ad car_ad_%s 0 %s" % (port, port))
        if sim_conf["kifo_put_f1u_sw_amptd_flag"]:# or kifo_put_f1u_tf_amptd_flag:
            model.parse("ad fsb1_upper_ad_%s %s %s" % (port, fsb1, port))
        if sim_conf["kifo_put_f1l_sw_amptd_flag"]:# or kifo_put_f1l_tf_amptd_flag:
            model.parse("ad fsb1_lower_ad_%s %s %s" % (port, mfsb1, port))
        if sim_conf["kifo_put_f2u_sw_amptd_flag"]:# or kifo_put_f2u_tf_amptd_flag:
            model.parse("ad fsb2_upper_ad_%s %s %s" % (port, fsb2, port))
        if sim_conf["kifo_put_f2l_sw_amptd_flag"]:# or kifo_put_f2l_tf_amptd_flag:
            model.parse("ad fsb2_lower_ad_%s %s %s" % (port, mfsb2, port))
    return model

def get_first_mixer_optimal_phase(model, sim_conf, port, freq):
    """
        this function get optimal demodulation phase for selected port.
        it'll put two PD1s(demod phase=(1)0 and (2)90) on selected port,
        and calculate the optimal phase using (1) and (2).

        Attributes
        ----------
        port : string
            selected port
        freq : string
            selected demodulation frequency: DC,fsb1,fsb2, 168888,...

        Returns
        -------
        optimal_phase : 型
            optimal demod phase

        See Also
        --------
        calc_optimal_dphase_IQ(​out, I_pdname, Q_pdname, N​) : pd1(demod phase=0 and 90)からoptimal phaseを計算して返す
    """
    # calculate first mixer optimal phase
    tmp_model = model.deepcopy()
    tmp1         = "pd1 pd1_insert_freqs_later_0_%s %s 0 %s" % (port, freq, port)
    tmp1         = replace_chr(tmp1, sim_conf)
    tmp2         = "pd1 pd1_insert_freqs_later_90_%s %s 90 %s" % (port, freq, port)
    tmp2         = replace_chr(tmp2, sim_conf)
    I_pdname     = "pd1_insert_freqs_later_0_%s"%(port)
    I_pdname     = replace_chr(I_pdname, sim_conf)
    Q_pdname     = "pd1_insert_freqs_later_90_%s"%(port)
    Q_pdname     = replace_chr(Q_pdname, sim_conf)
    tmp_model.parse(tmp1)
    tmp_model.parse(tmp2)
    tmp_model.xaxis.remove()
    tmp_model = add_dofcommand_to_model(tmp_model, sim_conf, "tf_dmod2")# sw_dmod1の項目だけど、ここで渡すのは"tf_dmod2"で合ってる
    tmp_model.xaxis.scale = "log"
    tmp_model.xaxis.limits = (1e-30, 1000)
    #print("now calculating optimalphase")
    #print(tmp_model)
    out = tmp_model.run()
    optimal_phase = calc_optimal_dphase_IQ(out, I_pdname, Q_pdname, 0)[0]
    print("calculated optimal phase = %s"%optimal_phase)
    return optimal_phase

def parse_sw_dmod1_kat(sim_conf, model, port, freq, phase):
    """
        sw_dmod1: sweep demodulation pd1

        put PDs for length-sweep-demodulated signal(length error signal) simulation

        Attributes
        ----------
        port : string
            ポート
        freq : string
            selected demodulation frequency: DC,fsb1,fsb2, 168888,...
        phase : string
            selected demodulation phase: Iphase, Qphase, optimal, 45,99,...

        Returns
        -------
        model : 型
            model

        See Also
        --------
        get_first_mixer_optimal_phase(​model, sim_conf, port, freq​) : pd1 の最適な復調位相を取得する
    """
    if freq=="DC":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
        return model
    else:
        if phase=="optimal":
            optimal_phase = get_first_mixer_optimal_phase(model, sim_conf, port, freq)
            phase = optimal_phase
            tmp = "pd1 pd1_insert_freqs_later_optimal%s_%s %s %s %s" % (optimal_phase, port, freq, optimal_phase, port)
            """
            tmp = replace_chr(tmp, sim_conf)
            tmp = tmp.replace('insert_freqs_later', freq)
            tmp = tmp.replace('Iphase', '0' )
            tmp = tmp.replace('Qphase', '90')
            model.parse(tmp)
            """
        else:
            tmp = "pd1 pd1_insert_freqs_later_%s_%s %s %s %s" % (phase, port, freq, phase, port)
            print(tmp)
        tmp = replace_chr(tmp, sim_conf)
        tmp = tmp.replace('insert_freqs_later', freq)
        tmp = tmp.replace('Iphase', '0' )
        tmp = tmp.replace('Qphase', '90')
        model.parse(tmp)
        return model
        ## def end

def parse_sw_dmodp_kat(sim_conf, model, port, freq):

    if freq=="DC":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
        return model
    else:
        tmp = """
        pd1 pd1_insert_freqs_later_%s %s 0 %s
        put* pd1_insert_freqs_later_%s phase1 $x1
        """ % (port, freq, port
              ,port)
        print(tmp)
        tmp = replace_chr(tmp, sim_conf)
        tmp = tmp.replace('insert_freqs_later', freq)
        model.parse(tmp)
        return model

def parse_sw_dmod2_kat_not_optimal(sim_conf, model, port, freq, phase):
    """
        sw_dmod2_kat_not_optimal: sweep demodulation pd2, demodulation phase is not optimized

        put PD2 on selected port.
        add this line
        -> "pd2 pd2_name freq phase 10 port"
        pd2 parameter "f2" is 10.(it doesn't matter.)

        Attributes
        ----------
        port : string
            selected port
        freq : string
            selected demodulation frequency: DC,fsb1,fsb2, 168888,...
        phase : string
            selected demodulation phase: Iphase, Qphase, optimal, 45,99,...

        Returns
        -------
        model : 型
            model
    """
    tmp = "pd2 pd2_insert_freqs_later_%s_%s %s %s 10 %s"%(phase, port, freq, phase, port)
    tmp = tmp.replace('fsb1',sim_conf["k_inf_c_f1_mod_frequency"])
    tmp = tmp.replace('fsb2',sim_conf["k_inf_c_f2_mod_frequency"])
    tmp = tmp.replace('insert_freqs_later', freq)
    tmp = tmp.replace('Iphase', '0' )
    tmp = tmp.replace('Qphase', '90')
    model.parse(tmp)
    return model

def parse_sw_dmod2_kat_optimal(sim_conf, model, port, freq):
    """
        sw_dmod2_kat_optimal: sweep demodulation pd2, demodulation phase is optimized

        選択したポートに選択した周波数と最適な復調位相を設定したpd2を置く

        Attributes
        ----------
        port : string
            selected port
        freq : string
            selected demodulation frequency: DC,fsb1,fsb2, 168888,...
            this parameter is used for "f1" in FINESSE pd2 component.

        Returns
        -------
        model : 型
            model

        See Also
        --------
        get_first_mixer_optimal_phase(model, sim_conf, port, freq) : 選択したポートと周波数における最適な復調位相を返す
    """
    # calculate dmod1 optimal
    optimal_phase1 = get_first_mixer_optimal_phase(model, sim_conf, port, freq)

    # calculate dmod2 optimal
    tmp_model = model.deepcopy()
    tmp3         = "pd2 pd2_insert_freqs_later_0_%s %s %s 10 0 %s"%(port, freq, optimal_phase1, port)
    tmp3         = replace_chr(tmp3, sim_conf)
    tmp4         = "pd2 pd2_insert_freqs_later_90_%s %s %s 10 90 %s"%(port, freq, optimal_phase1, port)
    tmp4         = replace_chr(tmp4, sim_conf)
    I_pdname     = "pd2_insert_freqs_later_0_%s"%(port)
    I_pdname     = replace_chr(I_pdname, sim_conf)
    Q_pdname     = "pd2_insert_freqs_later_90_%s"%(port)
    Q_pdname     = replace_chr(Q_pdname, sim_conf)
    tmp_model.parse(tmp3)
    tmp_model.parse(tmp4)
    tmp_model.xaxis.remove()
    tmp_model = add_dofcommand_to_model(tmp_model, sim_conf, "tf_dmod2")# sw_dmod1の項目だけど、ここで渡すのは"tf_dmod2"で合ってる
    print("now calculating optimalphase")
    print(tmp_model)
    out = tmp_model.run()
    optimal_phase2 = calc_optimal_dphase_IQ(out, I_pdname, Q_pdname, 0)[0]
    print("optimal phase2 = ")
    print(optimal_phase2)

    # put PD
    tmp5 = "pd2 pd2_insert_freqs_later_optimal%s_%s_%s %s %s 10 %s %s"%(optimal_phase1, optimal_phase2, port, freq, optimal_phase1, optimal_phase2, port)
    tmp5 = replace_chr(tmp5, sim_conf)
    tmp5 = tmp5.replace('insert_freqs_later', freq)
    tmp5 = tmp5.replace('Iphase', '0' )
    tmp5 = tmp5.replace('Qphase', '90')
    model.parse(tmp5)
    #model.parse(tmp4)
    print("now added optimal pd2")
    print(model)
    return model

def parse_st_type1_kat(sim_conf, model, port, freq, phase):

    if freq=="DC":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
        return model
    else:

        tmp = """
        qnoisedS pq_insert_freqs_later_%s_%s 2 %s %s $fs %s
        """ % (phase, port, freq, phase, port)

        print(tmp)
        tmp = replace_chr(tmp, sim_conf)
        tmp = tmp.replace('insert_freqs_later', freq)
        tmp = tmp.replace('Iphase', '0' )
        tmp = tmp.replace('Qphase', '90')
        model.parse(tmp)

        return model
def parse_st_type2_kat(sim_conf, model, port, freq, phase):

    if freq=="DC":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
        return model
    else:

        tmp = """
        qnoisedS pq_insert_freqs_later_%s_%s 2 %s %s $fs %s
        """ % (phase, port, freq, phase, port)

        print(tmp)
        tmp = replace_chr(tmp, sim_conf)
        tmp = tmp.replace('insert_freqs_later', freq)
        tmp = tmp.replace('Iphase', '0' )
        tmp = tmp.replace('Qphase', '90')
        model.parse(tmp)

        return model
# Main function
def add_pds_to_model(base, sim_conf, type_of_pd_signal, add_dofcommand_to_model):
    model = base.deepcopy()
    #model = make_kat_pd(model, type_of_pd_signal, sim_conf)
    if type_of_pd_signal  =="sw_power":
        for port in get_all_port_list():
            if sim_conf["kifo_sw_power_%s"%port]:
                model.parse("pd0 pd0_DC_%s %s" % (port, port))
    elif type_of_pd_signal=="sw_amptd":
        for num in range(10):
            key   =  f"kifo_sw_amptd_field_select_box_field_checkbox_{num}"
            print(key)
            if key in sim_conf:
                if sim_conf[key]:
                    field  = sim_conf[f"kifo_sw_amptd_field_select_box_field_combo_{num}"]
                    n_mode = sim_conf[f"kifo_sw_amptd_field_select_box_TEM_n_combo_{num}"]
                    m_mode = sim_conf[f"kifo_sw_amptd_field_select_box_TEM_m_combo_{num}"]
                    port   = sim_conf[f"kifo_sw_amptd_field_select_box_port_combo_{num}"]
                    pdname = f"ad {field}_ad_{n_mode}_{m_mode}_{port} {n_mode} {m_mode} 0 {port}"
                    model.parse(pdname)
                    #error_check = [a+"empty" for a in [field,n_mode,m_mode,port] if a==""]
                    #if len(error_check)==0:
                    #    pdname = f"ad {field}_ad_{n_mode}_{m_mode}_{port} {n_mode} {m_mode} 0 {port}"
                    #    model.parse(pdname)
    elif type_of_pd_signal=="sw_dmod1":
        for i in range(sim_conf["kgui_sw_dmod1_plotnum"]):
            num = i+1
            freq  = sim_conf["kifo_sw_dmod1_freq_combo%s"%(str(num))]
            phase = sim_conf["kifo_sw_dmod1_phase_combo%s"%(str(num))]
            port  = sim_conf["kifo_sw_dmod1_port_combo%s"%(str(num))]
            model = parse_sw_dmod1_kat(sim_conf, model, port, freq, phase)
    elif type_of_pd_signal=="sw_dmodp":
        max = int(sim_conf["kifo_sw_dmodp_box_combo"])
        for i in range(max):
            num = i + 1
            freq  = sim_conf["kifo_sw_dmodp_freq_combo%s"%(str(num))]
            port  = sim_conf["kifo_sw_dmodp_port_combo%s"%(str(num))]
            model = parse_sw_dmodp_kat(sim_conf, model, port, freq)
    elif type_of_pd_signal=="tf_power":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
    elif type_of_pd_signal=="tf_amptd":
        model.parse("pd0 pd0_DC_%s %s" % (port, port))
    elif type_of_pd_signal=="tf_dmod2":
        if sim_conf["kifo_tf_dmod2_matrix"]:
            # for making DC gain matrix
            pass
        # else:
        for i in range(sim_conf["kgui_tf_dmod2_plotnum"]):
            i = i+1
            freq  = sim_conf["kifo_tf_dmod2_freq_combo%s"%(str(i))]
            phase = sim_conf["kifo_tf_dmod2_phase_combo%s"%(str(i))]
            port  = sim_conf["kifo_tf_dmod2_port_combo%s"%(str(i))]
            if phase =="optimal":
                model = parse_sw_dmod2_kat_optimal(sim_conf, model, port, freq)
            else:
                model = parse_sw_dmod2_kat_not_optimal(sim_conf, model, port, freq, phase)
        #####
        # pd2を置く時にputのコマンドも一緒にparseすると重複した時にエラーが出る
        # pdは重複したものを勝手に消してくれるからいいが、
        # putの行をどうやってpykatのコマンドを使ってどう消すのかわからないので後から付け加えることにした
        print("now adding put")
        print(model.detectors.keys())
        for pdname in model.detectors.keys():
            model.parse("put %s f2 $x1"%pdname)

    elif type_of_pd_signal=="st_type1": # shot noise only
        max = int(sim_conf["kifo_st_type1_box_combo"])
        for i in range(max):
            num = i + 1
            freq  = sim_conf["kifo_st_type1_freq_combo%s"%(str(num))]
            phase = sim_conf["kifo_st_type1_phase_combo%s"%(str(num))]
            port  = sim_conf["kifo_st_type1_port_combo%s"%(str(num))]
            model = parse_st_type1_kat(sim_conf, model, port, freq, phase)
        #freq = "0"
        #freq = sim_conf["k_inf_c_f1_mod_frequency"]
        #for port in get_all_port_list():
        #    if sim_conf["kifo_st_type1_%s"%port]:
        #        tmp = "qnoisedS qnoisedS_insert_freqs_later_%s 2 %s max $fs %s"%(port, freq, port)
        #        tmp = tmp.replace('fsb1',sim_conf["k_inf_c_f1_mod_frequency"])
        #        tmp = tmp.replace('fsb2',sim_conf["k_inf_c_f2_mod_frequency"])
        #        tmp = tmp.replace('insert_freqs_later', freq)
        #        model.parse(tmp)
        # mirror parameter
        mibs_mass = sim_conf["k_inf_c_mibs_mirror_mass"]

        itmx_mass = sim_conf["k_inf_c_itmx_mirror_mass"]
        itmy_mass = sim_conf["k_inf_c_itmy_mirror_mass"]
        etmx_mass = sim_conf["k_inf_c_etmx_mirror_mass"]
        etmy_mass = sim_conf["k_inf_c_etmy_mirror_mass"]

        prm_mass = sim_conf["k_inf_c_prm_mirror_mass"]
        pr2_mass = sim_conf["k_inf_c_pr2_mirror_mass"]
        pr3_mass = sim_conf["k_inf_c_pr3_mirror_mass"]

        srm_mass = sim_conf["k_inf_c_srm_mirror_mass"]
        sr2_mass = sim_conf["k_inf_c_sr2_mirror_mass"]
        sr3_mass = sim_conf["k_inf_c_sr3_mirror_mass"]
        
        # mass
        model.parse("""

        sq sq1 0 0 90 AS

        attr MIBS mass %s
        attr ITMX mass %s
        attr ITMY mass %s
        attr ETMX mass %s
        attr ETMY mass %s

        attr PRM mass %s
        attr PR2 mass %s
        attr PR3 mass %s

        attr SRM mass %s
        attr SR2 mass %s
        attr SR3 mass %s

        """%(mibs_mass,  itmx_mass, itmy_mass, etmx_mass, etmy_mass,  prm_mass, pr2_mass, pr3_mass,  srm_mass, sr2_mass, sr3_mass))

        # tf
        # mibs
        if sim_conf["mibs_using_tf_f_to_m"]:
            mibs_resonance_num = sim_conf["k_inf_c_mibs_mirror_tf_rn"]
            mibs_tf_ftm_head = "tf mibs_ftm_sus1"
            gain             = "1"
            phase            = "0"
            mibs_tf_ftm = mibs_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(mibs_resonance_num)):
                num = i+1
                mibs_tf_ftm_zp     = sim_conf["k_inf_c_mibs_mirror_tf_zp_%s"%num]
                mibs_tf_ftm_rf     = sim_conf["k_inf_c_mibs_mirror_tf_rf_%s"%num]
                mibs_tf_ftm_rq     = sim_conf["k_inf_c_mibs_mirror_tf_rq_%s"%num]
                mibs_tf_ftm       += " %s %s %s"%(mibs_tf_ftm_zp, mibs_tf_ftm_rf, mibs_tf_ftm_rq)
                model.parse(mibs_tf_ftm)
            model.parse("""
            attr MIBS zmech mibs_ftm_sus1
            """)


        # ITMX
        if sim_conf["itmx_using_tf_f_to_m"]:
            itmx_resonance_num = sim_conf["k_inf_c_itmx_mirror_tf_rn"]
            itmx_tf_ftm_head = "tf itmx_ftm_sus1"
            gain             = "1"
            phase            = "0"
            itmx_tf_ftm = itmx_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(itmx_resonance_num)):
                num = i+1
                itmx_tf_ftm_zp     = sim_conf["k_inf_c_itmx_mirror_tf_zp_%s"%num]
                itmx_tf_ftm_rf     = sim_conf["k_inf_c_itmx_mirror_tf_rf_%s"%num]
                itmx_tf_ftm_rq     = sim_conf["k_inf_c_itmx_mirror_tf_rq_%s"%num]
                itmx_tf_ftm       += " %s %s %s"%(itmx_tf_ftm_zp, itmx_tf_ftm_rf, itmx_tf_ftm_rq)
                model.parse(itmx_tf_ftm)
            model.parse("""
            attr ITMX zmech itmx_ftm_sus1
            """)
        # itmy
        if sim_conf["itmy_using_tf_f_to_m"]:
            itmy_resonance_num = sim_conf["k_inf_c_itmy_mirror_tf_rn"]
            itmy_tf_ftm_head = "tf itmy_ftm_sus1"
            gain             = "1"
            phase            = "0"
            itmy_tf_ftm = itmy_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(itmy_resonance_num)):
                num = i+1
                itmy_tf_ftm_zp     = sim_conf["k_inf_c_itmy_mirror_tf_zp_%s"%num]
                itmy_tf_ftm_rf     = sim_conf["k_inf_c_itmy_mirror_tf_rf_%s"%num]
                itmy_tf_ftm_rq     = sim_conf["k_inf_c_itmy_mirror_tf_rq_%s"%num]
                itmy_tf_ftm       += " %s %s %s"%(itmy_tf_ftm_zp, itmy_tf_ftm_rf, itmy_tf_ftm_rq)
                model.parse(itmy_tf_ftm)
            model.parse("""
            attr ITMY zmech itmy_ftm_sus1
            """)
        # etmx
        if sim_conf["etmx_using_tf_f_to_m"]:
            etmx_resonance_num = sim_conf["k_inf_c_etmx_mirror_tf_rn"]
            etmx_tf_ftm_head = "tf etmx_ftm_sus1"
            gain             = "1"
            phase            = "0"
            etmx_tf_ftm = etmx_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(etmx_resonance_num)):
                num = i+1
                etmx_tf_ftm_zp     = sim_conf["k_inf_c_etmx_mirror_tf_zp_%s"%num]
                etmx_tf_ftm_rf     = sim_conf["k_inf_c_etmx_mirror_tf_rf_%s"%num]
                etmx_tf_ftm_rq     = sim_conf["k_inf_c_etmx_mirror_tf_rq_%s"%num]
                etmx_tf_ftm       += " %s %s %s"%(etmx_tf_ftm_zp, etmx_tf_ftm_rf, etmx_tf_ftm_rq)
                model.parse(etmx_tf_ftm)
            model.parse("""
            attr ETMX zmech etmx_ftm_sus1
            """)
        # etmy
        if sim_conf["etmy_using_tf_f_to_m"]:
            etmy_resonance_num = sim_conf["k_inf_c_etmy_mirror_tf_rn"]
            etmy_tf_ftm_head = "tf etmy_ftm_sus1"
            gain             = "1"
            phase            = "0"
            etmy_tf_ftm = etmy_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(etmy_resonance_num)):
                num = i+1
                etmy_tf_ftm_zp     = sim_conf["k_inf_c_etmy_mirror_tf_zp_%s"%num]
                etmy_tf_ftm_rf     = sim_conf["k_inf_c_etmy_mirror_tf_rf_%s"%num]
                etmy_tf_ftm_rq     = sim_conf["k_inf_c_etmy_mirror_tf_rq_%s"%num]
                etmy_tf_ftm       += " %s %s %s"%(etmy_tf_ftm_zp, etmy_tf_ftm_rf, etmy_tf_ftm_rq)
                model.parse(etmy_tf_ftm)
            model.parse("""
            attr ETMY zmech etmy_ftm_sus1
            """)
        
        # prm
        if sim_conf["prm_using_tf_f_to_m"]:
            prm_resonance_num = sim_conf["k_inf_c_prm_mirror_tf_rn"]
            prm_tf_ftm_head = "tf prm_ftm_sus1"
            gain             = "1"
            phase            = "0"
            prm_tf_ftm = prm_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(prm_resonance_num)):
                num = i+1
                prm_tf_ftm_zp     = sim_conf["k_inf_c_prm_mirror_tf_zp_%s"%num]
                prm_tf_ftm_rf     = sim_conf["k_inf_c_prm_mirror_tf_rf_%s"%num]
                prm_tf_ftm_rq     = sim_conf["k_inf_c_prm_mirror_tf_rq_%s"%num]
                prm_tf_ftm       += " %s %s %s"%(prm_tf_ftm_zp, prm_tf_ftm_rf, prm_tf_ftm_rq)
                model.parse(prm_tf_ftm)
            model.parse("""
            attr PRM zmech prm_ftm_sus1
            """)
        # pr2
        if sim_conf["pr2_using_tf_f_to_m"]:
            pr2_resonance_num = sim_conf["k_inf_c_pr2_mirror_tf_rn"]
            pr2_tf_ftm_head = "tf pr2_ftm_sus1"
            gain             = "1"
            phase            = "0"
            pr2_tf_ftm = pr2_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(pr2_resonance_num)):
                num = i+1
                pr2_tf_ftm_zp     = sim_conf["k_inf_c_pr2_mirror_tf_zp_%s"%num]
                pr2_tf_ftm_rf     = sim_conf["k_inf_c_pr2_mirror_tf_rf_%s"%num]
                pr2_tf_ftm_rq     = sim_conf["k_inf_c_pr2_mirror_tf_rq_%s"%num]
                pr2_tf_ftm       += " %s %s %s"%(pr2_tf_ftm_zp, pr2_tf_ftm_rf, pr2_tf_ftm_rq)
                model.parse(pr2_tf_ftm)
            model.parse("""
            attr PR2 zmech pr2_ftm_sus1
            """)
        # pr3
        if sim_conf["pr3_using_tf_f_to_m"]:
            pr3_resonance_num = sim_conf["k_inf_c_pr3_mirror_tf_rn"]
            pr3_tf_ftm_head = "tf pr3_ftm_sus1"
            gain             = "1"
            phase            = "0"
            pr3_tf_ftm = pr3_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(pr3_resonance_num)):
                num = i+1
                pr3_tf_ftm_zp     = sim_conf["k_inf_c_pr3_mirror_tf_zp_%s"%num]
                pr3_tf_ftm_rf     = sim_conf["k_inf_c_pr3_mirror_tf_rf_%s"%num]
                pr3_tf_ftm_rq     = sim_conf["k_inf_c_pr3_mirror_tf_rq_%s"%num]
                pr3_tf_ftm       += " %s %s %s"%(pr3_tf_ftm_zp, pr3_tf_ftm_rf, pr3_tf_ftm_rq)
                model.parse(pr3_tf_ftm)
            model.parse("""
            attr PR3 zmech pr3_ftm_sus1
            """)
        # srm
        if sim_conf["srm_using_tf_f_to_m"]:
            srm_resonance_num = sim_conf["k_inf_c_srm_mirror_tf_rn"]
            srm_tf_ftm_head = "tf srm_ftm_sus1"
            gain             = "1"
            phase            = "0"
            srm_tf_ftm = srm_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(srm_resonance_num)):
                num = i+1
                srm_tf_ftm_zp     = sim_conf["k_inf_c_srm_mirror_tf_zp_%s"%num]
                srm_tf_ftm_rf     = sim_conf["k_inf_c_srm_mirror_tf_rf_%s"%num]
                srm_tf_ftm_rq     = sim_conf["k_inf_c_srm_mirror_tf_rq_%s"%num]
                srm_tf_ftm       += " %s %s %s"%(srm_tf_ftm_zp, srm_tf_ftm_rf, srm_tf_ftm_rq)
                model.parse(srm_tf_ftm)
            model.parse("""
            attr SRM zmech srm_ftm_sus1
            """)
        # sr2
        if sim_conf["sr2_using_tf_f_to_m"]:
            sr2_resonance_num = sim_conf["k_inf_c_sr2_mirror_tf_rn"]
            sr2_tf_ftm_head = "tf sr2_ftm_sus1"
            gain             = "1"
            phase            = "0"
            sr2_tf_ftm = sr2_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(sr2_resonance_num)):
                num = i+1
                sr2_tf_ftm_zp     = sim_conf["k_inf_c_sr2_mirror_tf_zp_%s"%num]
                sr2_tf_ftm_rf     = sim_conf["k_inf_c_sr2_mirror_tf_rf_%s"%num]
                sr2_tf_ftm_rq     = sim_conf["k_inf_c_sr2_mirror_tf_rq_%s"%num]
                sr2_tf_ftm       += " %s %s %s"%(sr2_tf_ftm_zp, sr2_tf_ftm_rf, sr2_tf_ftm_rq)
                model.parse(sr2_tf_ftm)
            model.parse("""
            attr SR2 zmech sr2_ftm_sus1
            """)
        # sr3
        if sim_conf["sr3_using_tf_f_to_m"]:
            sr3_resonance_num = sim_conf["k_inf_c_sr3_mirror_tf_rn"]
            sr3_tf_ftm_head = "tf sr3_ftm_sus1"
            gain             = "1"
            phase            = "0"
            sr3_tf_ftm = sr3_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(sr3_resonance_num)):
                num = i+1
                sr3_tf_ftm_zp     = sim_conf["k_inf_c_sr3_mirror_tf_zp_%s"%num]
                sr3_tf_ftm_rf     = sim_conf["k_inf_c_sr3_mirror_tf_rf_%s"%num]
                sr3_tf_ftm_rq     = sim_conf["k_inf_c_sr3_mirror_tf_rq_%s"%num]
                sr3_tf_ftm       += " %s %s %s"%(sr3_tf_ftm_zp, sr3_tf_ftm_rf, sr3_tf_ftm_rq)
                model.parse(sr3_tf_ftm)
            model.parse("""
            attr SR3 zmech sr3_ftm_sus1
            """)
            model.parse("""
            sq sq1 0 0 90 AS
            """)

    elif type_of_pd_signal=="st_type2": # shot noise only
        #freq = "0"
        max = int(sim_conf["kifo_st_type2_box_combo"])
        for i in range(max):
            num = i + 1
            freq  = sim_conf["kifo_st_type2_freq_combo%s"%(str(num))]
            phase = sim_conf["kifo_st_type2_phase_combo%s"%(str(num))]
            port  = sim_conf["kifo_st_type2_port_combo%s"%(str(num))]
            model = parse_st_type2_kat(sim_conf, model, port, freq, phase)

        # mirror parameter
        mibs_mass = sim_conf["k_inf_c_mibs_mirror_mass"]

        itmx_mass = sim_conf["k_inf_c_itmx_mirror_mass"]
        itmy_mass = sim_conf["k_inf_c_itmy_mirror_mass"]
        etmx_mass = sim_conf["k_inf_c_etmx_mirror_mass"]
        etmy_mass = sim_conf["k_inf_c_etmy_mirror_mass"]

        prm_mass = sim_conf["k_inf_c_prm_mirror_mass"]
        pr2_mass = sim_conf["k_inf_c_pr2_mirror_mass"]
        pr3_mass = sim_conf["k_inf_c_pr3_mirror_mass"]

        srm_mass = sim_conf["k_inf_c_srm_mirror_mass"]
        sr2_mass = sim_conf["k_inf_c_sr2_mirror_mass"]
        sr3_mass = sim_conf["k_inf_c_sr3_mirror_mass"]
        
        # mass
        model.parse("""

        sq sq1 0 0 90 AS

        attr MIBS mass %s
        attr ITMX mass %s
        attr ITMY mass %s
        attr ETMX mass %s
        attr ETMY mass %s

        attr PRM mass %s
        attr PR2 mass %s
        attr PR3 mass %s

        attr SRM mass %s
        attr SR2 mass %s
        attr SR3 mass %s

        """%(mibs_mass,  itmx_mass, itmy_mass, etmx_mass, etmy_mass,  prm_mass, pr2_mass, pr3_mass,  srm_mass, sr2_mass, sr3_mass))

        # tf
        # mibs
        if sim_conf["mibs_using_tf_f_to_m"]:
            mibs_resonance_num = sim_conf["k_inf_c_mibs_mirror_tf_rn"]
            mibs_tf_ftm_head = "tf mibs_ftm_sus1"
            gain             = "1"
            phase            = "0"
            mibs_tf_ftm = mibs_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(mibs_resonance_num)):
                num = i+1
                mibs_tf_ftm_zp     = sim_conf["k_inf_c_mibs_mirror_tf_zp_%s"%num]
                mibs_tf_ftm_rf     = sim_conf["k_inf_c_mibs_mirror_tf_rf_%s"%num]
                mibs_tf_ftm_rq     = sim_conf["k_inf_c_mibs_mirror_tf_rq_%s"%num]
                mibs_tf_ftm       += " %s %s %s"%(mibs_tf_ftm_zp, mibs_tf_ftm_rf, mibs_tf_ftm_rq)
                model.parse(mibs_tf_ftm)
            model.parse("""
            attr MIBS zmech mibs_ftm_sus1
            """)
        # ITMX
        if sim_conf["itmx_using_tf_f_to_m"]:
            itmx_resonance_num = sim_conf["k_inf_c_itmx_mirror_tf_rn"]
            itmx_tf_ftm_head = "tf itmx_ftm_sus1"
            gain             = "1"
            phase            = "0"
            itmx_tf_ftm = itmx_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(itmx_resonance_num)):
                num = i+1
                itmx_tf_ftm_zp     = sim_conf["k_inf_c_itmx_mirror_tf_zp_%s"%num]
                itmx_tf_ftm_rf     = sim_conf["k_inf_c_itmx_mirror_tf_rf_%s"%num]
                itmx_tf_ftm_rq     = sim_conf["k_inf_c_itmx_mirror_tf_rq_%s"%num]
                itmx_tf_ftm       += " %s %s %s"%(itmx_tf_ftm_zp, itmx_tf_ftm_rf, itmx_tf_ftm_rq)
                model.parse(itmx_tf_ftm)
            model.parse("""
            attr ITMX zmech itmx_ftm_sus1
            """)
        # itmy
        if sim_conf["itmy_using_tf_f_to_m"]:
            itmy_resonance_num = sim_conf["k_inf_c_itmy_mirror_tf_rn"]
            itmy_tf_ftm_head = "tf itmy_ftm_sus1"
            gain             = "1"
            phase            = "0"
            itmy_tf_ftm = itmy_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(itmy_resonance_num)):
                num = i+1
                itmy_tf_ftm_zp     = sim_conf["k_inf_c_itmy_mirror_tf_zp_%s"%num]
                itmy_tf_ftm_rf     = sim_conf["k_inf_c_itmy_mirror_tf_rf_%s"%num]
                itmy_tf_ftm_rq     = sim_conf["k_inf_c_itmy_mirror_tf_rq_%s"%num]
                itmy_tf_ftm       += " %s %s %s"%(itmy_tf_ftm_zp, itmy_tf_ftm_rf, itmy_tf_ftm_rq)
                model.parse(itmy_tf_ftm)
            model.parse("""
            attr ITMY zmech itmy_ftm_sus1
            """)
        # etmx
        if sim_conf["etmx_using_tf_f_to_m"]:
            etmx_resonance_num = sim_conf["k_inf_c_etmx_mirror_tf_rn"]
            etmx_tf_ftm_head = "tf etmx_ftm_sus1"
            gain             = "1"
            phase            = "0"
            etmx_tf_ftm = etmx_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(etmx_resonance_num)):
                num = i+1
                etmx_tf_ftm_zp     = sim_conf["k_inf_c_etmx_mirror_tf_zp_%s"%num]
                etmx_tf_ftm_rf     = sim_conf["k_inf_c_etmx_mirror_tf_rf_%s"%num]
                etmx_tf_ftm_rq     = sim_conf["k_inf_c_etmx_mirror_tf_rq_%s"%num]
                etmx_tf_ftm       += " %s %s %s"%(etmx_tf_ftm_zp, etmx_tf_ftm_rf, etmx_tf_ftm_rq)
                model.parse(etmx_tf_ftm)
            model.parse("""
            attr ETMX zmech etmx_ftm_sus1
            """)
        # etmy
        if sim_conf["etmy_using_tf_f_to_m"]:
            etmy_resonance_num = sim_conf["k_inf_c_etmy_mirror_tf_rn"]
            etmy_tf_ftm_head = "tf etmy_ftm_sus1"
            gain             = "1"
            phase            = "0"
            etmy_tf_ftm = etmy_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(etmy_resonance_num)):
                num = i+1
                etmy_tf_ftm_zp     = sim_conf["k_inf_c_etmy_mirror_tf_zp_%s"%num]
                etmy_tf_ftm_rf     = sim_conf["k_inf_c_etmy_mirror_tf_rf_%s"%num]
                etmy_tf_ftm_rq     = sim_conf["k_inf_c_etmy_mirror_tf_rq_%s"%num]
                etmy_tf_ftm       += " %s %s %s"%(etmy_tf_ftm_zp, etmy_tf_ftm_rf, etmy_tf_ftm_rq)
                model.parse(etmy_tf_ftm)
            model.parse("""
            attr ETMY zmech etmy_ftm_sus1
            """)
        # prm
        if sim_conf["prm_using_tf_f_to_m"]:
            prm_resonance_num = sim_conf["k_inf_c_prm_mirror_tf_rn"]
            prm_tf_ftm_head = "tf prm_ftm_sus1"
            gain             = "1"
            phase            = "0"
            prm_tf_ftm = prm_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(prm_resonance_num)):
                num = i+1
                prm_tf_ftm_zp     = sim_conf["k_inf_c_prm_mirror_tf_zp_%s"%num]
                prm_tf_ftm_rf     = sim_conf["k_inf_c_prm_mirror_tf_rf_%s"%num]
                prm_tf_ftm_rq     = sim_conf["k_inf_c_prm_mirror_tf_rq_%s"%num]
                prm_tf_ftm       += " %s %s %s"%(prm_tf_ftm_zp, prm_tf_ftm_rf, prm_tf_ftm_rq)
                model.parse(prm_tf_ftm)
            model.parse("""
            attr PRM zmech prm_ftm_sus1
            """)
        # pr2
        if sim_conf["pr2_using_tf_f_to_m"]:
            pr2_resonance_num = sim_conf["k_inf_c_pr2_mirror_tf_rn"]
            pr2_tf_ftm_head = "tf pr2_ftm_sus1"
            gain             = "1"
            phase            = "0"
            pr2_tf_ftm = pr2_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(pr2_resonance_num)):
                num = i+1
                pr2_tf_ftm_zp     = sim_conf["k_inf_c_pr2_mirror_tf_zp_%s"%num]
                pr2_tf_ftm_rf     = sim_conf["k_inf_c_pr2_mirror_tf_rf_%s"%num]
                pr2_tf_ftm_rq     = sim_conf["k_inf_c_pr2_mirror_tf_rq_%s"%num]
                pr2_tf_ftm       += " %s %s %s"%(pr2_tf_ftm_zp, pr2_tf_ftm_rf, pr2_tf_ftm_rq)
                model.parse(pr2_tf_ftm)
            model.parse("""
            attr PR2 zmech pr2_ftm_sus1
            """)
        # pr3
        if sim_conf["pr3_using_tf_f_to_m"]:
            pr3_resonance_num = sim_conf["k_inf_c_pr3_mirror_tf_rn"]
            pr3_tf_ftm_head = "tf pr3_ftm_sus1"
            gain             = "1"
            phase            = "0"
            pr3_tf_ftm = pr3_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(pr3_resonance_num)):
                num = i+1
                pr3_tf_ftm_zp     = sim_conf["k_inf_c_pr3_mirror_tf_zp_%s"%num]
                pr3_tf_ftm_rf     = sim_conf["k_inf_c_pr3_mirror_tf_rf_%s"%num]
                pr3_tf_ftm_rq     = sim_conf["k_inf_c_pr3_mirror_tf_rq_%s"%num]
                pr3_tf_ftm       += " %s %s %s"%(pr3_tf_ftm_zp, pr3_tf_ftm_rf, pr3_tf_ftm_rq)
                model.parse(pr3_tf_ftm)
            model.parse("""
            attr PR3 zmech pr3_ftm_sus1
            """)
        # srm
        if sim_conf["srm_using_tf_f_to_m"]:
            srm_resonance_num = sim_conf["k_inf_c_srm_mirror_tf_rn"]
            srm_tf_ftm_head = "tf srm_ftm_sus1"
            gain             = "1"
            phase            = "0"
            srm_tf_ftm = srm_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(srm_resonance_num)):
                num = i+1
                srm_tf_ftm_zp     = sim_conf["k_inf_c_srm_mirror_tf_zp_%s"%num]
                srm_tf_ftm_rf     = sim_conf["k_inf_c_srm_mirror_tf_rf_%s"%num]
                srm_tf_ftm_rq     = sim_conf["k_inf_c_srm_mirror_tf_rq_%s"%num]
                srm_tf_ftm       += " %s %s %s"%(srm_tf_ftm_zp, srm_tf_ftm_rf, srm_tf_ftm_rq)
                model.parse(srm_tf_ftm)
            model.parse("""
            attr SRM zmech srm_ftm_sus1
            """)
        # sr2
        if sim_conf["sr2_using_tf_f_to_m"]:
            sr2_resonance_num = sim_conf["k_inf_c_sr2_mirror_tf_rn"]
            sr2_tf_ftm_head = "tf sr2_ftm_sus1"
            gain             = "1"
            phase            = "0"
            sr2_tf_ftm = sr2_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(sr2_resonance_num)):
                num = i+1
                sr2_tf_ftm_zp     = sim_conf["k_inf_c_sr2_mirror_tf_zp_%s"%num]
                sr2_tf_ftm_rf     = sim_conf["k_inf_c_sr2_mirror_tf_rf_%s"%num]
                sr2_tf_ftm_rq     = sim_conf["k_inf_c_sr2_mirror_tf_rq_%s"%num]
                sr2_tf_ftm       += " %s %s %s"%(sr2_tf_ftm_zp, sr2_tf_ftm_rf, sr2_tf_ftm_rq)
                model.parse(sr2_tf_ftm)
            model.parse("""
            attr SR2 zmech sr2_ftm_sus1
            """)
        # sr3
        if sim_conf["sr3_using_tf_f_to_m"]:
            sr3_resonance_num = sim_conf["k_inf_c_sr3_mirror_tf_rn"]
            sr3_tf_ftm_head = "tf sr3_ftm_sus1"
            gain             = "1"
            phase            = "0"
            sr3_tf_ftm = sr3_tf_ftm_head +" "+ gain +" "+ phase

            for i in range(int(sr3_resonance_num)):
                num = i+1
                sr3_tf_ftm_zp     = sim_conf["k_inf_c_sr3_mirror_tf_zp_%s"%num]
                sr3_tf_ftm_rf     = sim_conf["k_inf_c_sr3_mirror_tf_rf_%s"%num]
                sr3_tf_ftm_rq     = sim_conf["k_inf_c_sr3_mirror_tf_rq_%s"%num]
                sr3_tf_ftm       += " %s %s %s"%(sr3_tf_ftm_zp, sr3_tf_ftm_rf, sr3_tf_ftm_rq)
                model.parse(sr3_tf_ftm)
            model.parse("""
            attr SR3 zmech sr3_ftm_sus1
            """)
            model.parse("""
            sq sq1 0 0 90 AS
            """)

    elif type_of_pd_signal=="swan_mirot_power":
        if sim_conf["kifo_an_misal_use_dof_sc_1"]:
            an_mirror1 = "ITMX"
            an_axis1 = sim_conf["kifo_an_misal_xarm_rotate_direction"]#pitch/yaw
            parse_an_misal_attr_kat(model, an_mirror1, an_axis1)
            an_mirror2 = "ETMX"
            an_axis2 = sim_conf["kifo_an_misal_xarm_rotate_direction"]#pitch/yaw
            parse_an_misal_attr_kat(model, an_mirror2, an_axis2)
            an_mirror3 = "ITMY"
            an_axis3 = sim_conf["kifo_an_misal_yarm_rotate_direction"]#pitch/yaw
            parse_an_misal_attr_kat(model, an_mirror3, an_axis3)
            an_mirror4 = "ETMY"
            an_axis4 = sim_conf["kifo_an_misal_yarm_rotate_direction"]#pitch/yaw
            parse_an_misal_attr_kat(model, an_mirror4, an_axis4)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                model      = parse_an_misal_pd_kat(model, an_pd_axis, port)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        elif sim_conf["kifo_an_misal_use_dof_sc_2"]:
            for i in range(sim_conf["kgui_an_misal_plotnum"]):#mirror plotnum
                # mirrorにattrで角度を追加
                num = i+1
                an_mirror  = sim_conf["kifo_an_misal_mirr_combo%s"%str(num)]
                an_axis    = sim_conf["kifo_an_misal_drct_combo%s"%str(num)]
                model      = parse_an_misal_attr_kat(model, an_mirror, an_axis)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                model      = parse_an_misal_pd_kat(model, an_pd_axis, port)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        pass
    
    elif type_of_pd_signal=="swan_mirot_amptd":
        if sim_conf["kifo_an_misal_use_dof_sc_1"]:
            an_mirror1 = "ITMX"
            an_axis1 = sim_conf["kifo_an_misal_xarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror1, an_axis1)
            an_mirror2 = "ETMX"
            an_axis2 = sim_conf["kifo_an_misal_xarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror2, an_axis2)
            an_mirror3 = "ITMY"
            an_axis3 = sim_conf["kifo_an_misal_yarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror3, an_axis3)
            an_mirror4 = "ETMY"
            an_axis4 = sim_conf["kifo_an_misal_yarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror4, an_axis4)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                n_mode     = sim_conf["kifo_swan_mirot_gaun_combo%s"%str(num)]
                m_mode     = sim_conf["kifo_swan_mirot_gaum_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_mirot_side_combo%s"%str(num)]
                if freq=="fsb1":
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                elif freq=="fsb2":
                    freq=sim_conf["k_inf_c_f2_mod_frequency"]
                    print(type(freq))
                    print(freq)
                else:
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                model      = parse_an_misal_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        elif sim_conf["kifo_an_misal_use_dof_sc_2"]:
            for i in range(sim_conf["kgui_an_misal_plotnum"]):#mirror plotnum
                # mirrorにattrで角度を追加
                num = i+1
                an_mirror  = sim_conf["kifo_an_misal_mirr_combo%s"%str(num)]
                an_axis    = sim_conf["kifo_an_misal_drct_combo%s"%str(num)]
                model      = parse_an_misal_attr_kat(model, an_mirror, an_axis)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                n_mode     = sim_conf["kifo_swan_mirot_gaun_combo%s"%str(num)]
                m_mode     = sim_conf["kifo_swan_mirot_gaum_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_mirot_side_combo%s"%str(num)]
                if freq=="fsb1":
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                elif freq=="fsb2":
                    freq=sim_conf["k_inf_c_f2_mod_frequency"]
                else:
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                model      = parse_an_misal_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        pass
    
    elif type_of_pd_signal=="swan_mirot_dmod1":
        if sim_conf["kifo_an_misal_use_dof_sc_1"]:
            an_mirror1 = "ITMX"
            an_axis1 = sim_conf["kifo_an_misal_xarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror1, an_axis1)
            an_mirror2 = "ETMX"
            an_axis2 = sim_conf["kifo_an_misal_xarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror2, an_axis2)
            an_mirror3 = "ITMY"
            an_axis3 = sim_conf["kifo_an_misal_yarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror3, an_axis3)
            an_mirror4 = "ETMY"
            an_axis4 = sim_conf["kifo_an_misal_yarm_rotate_direction"]
            parse_an_misal_attr_kat(model, an_mirror4, an_axis4)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_mirot_dmof_combo%s"%str(num)]
                phase      = sim_conf["kifo_swan_mirot_dmop_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                model      = parse_an_misal_dmod1_kat(model, sim_conf, an_pd_axis, freq, phase, port)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        elif sim_conf["kifo_an_misal_use_dof_sc_2"]:
            for i in range(sim_conf["kgui_an_misal_plotnum"]):#mirror plotnum
                # mirrorにattrで角度を追加
                num = i+1
                an_mirror  = sim_conf["kifo_an_misal_mirr_combo%s"%str(num)]
                an_axis    = sim_conf["kifo_an_misal_drct_combo%s"%str(num)]
                model      = parse_an_misal_attr_kat(model, an_mirror, an_axis)
            for i in range(sim_conf["kgui_an_misal_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_mirot_pddr_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_mirot_dmof_combo%s"%str(num)]
                phase      = sim_conf["kifo_swan_mirot_dmop_combo%s"%str(num)]
                port       = sim_conf["kifo_an_misal_port_combo%s"%str(num)]
                model      = parse_an_misal_dmod1_kat(model, sim_conf, an_pd_axis, freq, phase, port)
                deg        = sim_conf["kifo_swan_mirot_pdgy_combo%s"%str(num)]
                model      = parse_an_misal_gp_kat(model, port, deg)
        pass
    
    elif type_of_pd_signal=="swan_gouyp_power":
        print("swan_gouyp_power")
        if sim_conf["kifo_swan_gouy_use_dof_sc_1"]:
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                model      = parse_an_misal_pd_kat(model, an_pd_axis, port)
        elif sim_conf["kifo_swan_gouy_use_dof_sc_2"]:
            print("kifo_swan_gouy_use_dof_sc_2")
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                print(port)
                model      = parse_an_misal_pd_kat(model, an_pd_axis, port)
        pass
    
    elif type_of_pd_signal=="swan_gouyp_amptd":
        print("swan_gouyp_amptd")
        if sim_conf["kifo_swan_gouy_use_dof_sc_1"]:
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                n_mode     = sim_conf["kifo_swan_gouyp_gaun_combo%s"%str(num)]
                m_mode     = sim_conf["kifo_swan_gouyp_gaum_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_gouyp_side_combo%s"%str(num)]
                if freq=="fsb1":
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                elif freq=="fsb2":
                    freq=sim_conf["k_inf_c_f2_mod_frequency"]
                    print(type(freq))
                    print(freq)
                else:
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                model      = parse_swan_gouyp_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq)
        elif sim_conf["kifo_swan_gouy_use_dof_sc_2"]:
            print("kifo_swan_gouy_use_dof_sc_2")
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                n_mode     = sim_conf["kifo_swan_gouyp_gaun_combo%s"%str(num)]
                m_mode     = sim_conf["kifo_swan_gouyp_gaum_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_gouyp_side_combo%s"%str(num)]
                if freq=="fsb1":
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                elif freq=="fsb2":
                    freq=sim_conf["k_inf_c_f2_mod_frequency"]
                    print(type(freq))
                    print(freq)
                else:
                    freq=sim_conf["k_inf_c_f1_mod_frequency"]
                    print(type(freq))
                    print(freq)
                model      = parse_swan_gouyp_amptd_kat(model, an_pd_axis, port, n_mode, m_mode, freq)
        pass
    
    elif type_of_pd_signal=="swan_gouyp_dmod1":
        print("swan_gouyp_dmod1")
        if sim_conf["kifo_swan_gouy_use_dof_sc_1"]:
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_gouyp_dmof_combo%s"%str(num)]
                phase      = sim_conf["kifo_swan_gouyp_dmop_combo%s"%str(num)]
                model      = parse_an_misal_dmod1_kat(model, sim_conf, an_pd_axis, freq, phase, port)
        elif sim_conf["kifo_swan_gouy_use_dof_sc_2"]:
            print("kifo_swan_gouy_use_dof_sc_2")
            for i in range(sim_conf["kgui_swan_gouyp_mirtnum"]):#mirror rotate num
                # QPDを追加
                num = i+1
                an_pd_axis = sim_conf["kifo_swan_gouyp_pddr_combo%s"%str(num)]
                port       = sim_conf["kifo_swan_gouyp_port_combo%s"%str(num)]
                freq       = sim_conf["kifo_swan_gouyp_dmof_combo%s"%str(num)]
                phase      = sim_conf["kifo_swan_gouyp_dmop_combo%s"%str(num)]
                model      = parse_an_misal_dmod1_kat(model, sim_conf, an_pd_axis, freq, phase, port)
        pass
    
    print("now added PDs")
    print(model)
    return model
    
    #make_plot_figure_title(base_name, sim_conf["kifo_dof"], "Power")





##################################################################################
# utility
##################################################################################
def run_func_by_boolean(boolean, func1, func2):
    """
    if boolean==True, run func1
    else, run func2
    """
    if boolean==True:
        func1()
    else:
        func2()

def date_to_num(dt_now):
    """
    dt_nowに含まれる" ", ":", ".", "-"を取り除いて数字のみのstrにする

    Parameters
    ----------
    dt_now    : str
        str型の日付

    Returns
    -------
    dt_now : str
        引数から記号を取り除いたもの
    """
    dt_now = dt_now.replace(' ','')
    dt_now = dt_now.replace('-','')
    dt_now = dt_now.replace(':','')
    dt_now = dt_now.replace(',','')
    dt_now = dt_now.replace('.','')
    dt_now = str(dt_now)
    return dt_now

def replace_chr(tmp, sim_conf):
    list1 = ["2fsb1","2fsb2", "3fsb1","3fsb2", "fsb1","fsb2"]
    f1_2  = change_nums_unit_str_to_float("2*%s"%sim_conf["k_inf_c_f1_mod_frequency"])
    f1_3  = change_nums_unit_str_to_float("3*%s"%sim_conf["k_inf_c_f1_mod_frequency"])
    f2_2  = change_nums_unit_str_to_float("2*%s"%sim_conf["k_inf_c_f2_mod_frequency"])
    f2_3  = change_nums_unit_str_to_float("3*%s"%sim_conf["k_inf_c_f2_mod_frequency"])
    list2 = [str(f1_2), str(f2_2), str(f1_3), str(f2_3), sim_conf["k_inf_c_f1_mod_frequency"], sim_conf["k_inf_c_f2_mod_frequency"]]
    #print(list2)
    for i in range(len(list1)):
        tmp = tmp.replace(list1[i], list2[i])
    return tmp

def change_nums_unit_str_to_float(str_num):
    """
    str_numに含まれる省略した単位を数値に変換して,str_numをfloat型の数字に変換して計算した結果を返す

    Parameters
    ----------
    str_num    : str
        str型の数字

    Returns
    -------
    str_num : float
        計算した結果のfloat型の数字
    """
    str_num = str_num.replace('ppm','*10**(-6)')#pより先に置かないとpが置き換わってしまう
    str_num = str_num.replace('p',  '*10**(-12)')
    str_num = str_num.replace('n',  '*10**(-9)')
    str_num = str_num.replace('k',  '*10**3')
    str_num = str_num.replace('m',  '*10**(-3)')
    str_num = str_num.replace('M',  '*10**6')
    str_num = str_num.replace('G',  '*10**9')
    if str_num=="":
        pass
    else:
        str_num = eval(str_num)
    return str_num

def verify_input_chr_foramt_is_correct(input_str):
    """
    input_strに値が入っているか、入っている値はchange_nums_unit_str_to_floatで計算できるのかを調べる
    GUIのinputboxで入力した値をkatファイルのパラメータに代入できるかを調べるために作った
    """
    if input_str=="":
        return False
    else:
        try:
            a = change_nums_unit_str_to_float(input_str)
            a = a
            return True
        except:
            return False

def check_variable_existence(variable):
    if variable in globals():
        return True
    else:
        return False

def try_exception(code,error_log):
    try:
        exec(code)
        return True
    except:
        print("\n")
        print("Error! error log =")
        print(error_log)
        print("\n")
        return False


def return_list_value(list,index):
    value = list[index].value
    return value

##################################################################################
# PLOT TITLE
##################################################################################
def make_plot_figure_title(base_name, dof, type_of_pd_signal):
    plot_title = base_name+" "+dof+" "+type_of_pd_signal
    return plot_title

def make_plot_axes_title(base_name, interferometer, dof, type_of_pd_signal):

    plot_title = base_name+"_"+interferometer+"_"+dof+"_"+type_of_pd_signal
    return plot_title

def calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum):
    """
    legendのfontsizeをplotする数によって変更する
    """
    fontsize = 18
    if   1<=plotnum and plotnum<=4:
        fontsize = 9
    elif 5<=plotnum and plotnum<=9:
        fontsize = 6
    elif 10<=plotnum and plotnum<=16:
        fontsize = 4
    else:
        # plotnumが大量にある時はfontsize=6くらいだと図が隠れて4だと見辛いが逆にs大体形がわかればいいと思った
        fontsize = 4

    return fontsize

##################################################################################
#sim_conf
##################################################################################
def get_layout_drawing_path(base, interferometer, drawingsize):
    path=""
    if base=="kagra":
        if interferometer=="MI":
            if drawingsize=="normalsize":
                path = "../fig/MI_picture_normal.png"
            if drawingsize=="largesize":
                path = "../fig/MI_picture_large.png"
        if interferometer=="FPMI":
            if drawingsize=="normalsize":
                path = "../fig/FPMI_picture_normal.png"
            if drawingsize=="largesize":
                path = "../fig/FPMI_picture_large.png"
        if interferometer=="PRFPMI":
            if drawingsize=="normalsize":
                path = "../fig/PRFPMI_picture_normal.png"
            if drawingsize=="largesize":
                path = "../fig/PRFPMI_picture_large.png"
        if interferometer=="PRMI":
            """
            if drawingsize=="normalsize":
                path = "../fig/PRMI_picture_normal.png"
            if drawingsize=="largesize":
                path = "../fig/PRMI_picture_large.png"
            """
        if interferometer=="DRFPMI":
            if drawingsize=="normalsize":
                path = "../fig/DRFPMI_picture_normal.png"
            if drawingsize=="largesize":
                path = "../fig/DRFPMI_picture_large.png"
    else:
        pass
    return path

def create_base_by_setting_param(sim_conf, base, interferometer):
    changed_model = base.deepcopy()
    gui_param_to_model_param(sim_conf, changed_model)
    if interferometer=="MI":
        #FP
        changed_model = disable_FP_by_param(changed_model)
        #PRC
        changed_model = disable_PRC_by_param(changed_model)
        #SRC
        changed_model = disable_SRC_by_param(changed_model)
        return changed_model
    elif interferometer=="FPMI":
        #PRC
        changed_model = disable_PRC_by_param(changed_model)
        #SRC
        changed_model = disable_SRC_by_param(changed_model)
        return changed_model
    elif interferometer=="PRFPMI":
        #SRC
        changed_model = disable_SRC_by_param(changed_model)
        return changed_model
    elif interferometer=="PRMI":
        #FP
        changed_model = disable_FP_by_param(changed_model)
        #SRC
        changed_model = disable_SRC_by_param(changed_model)
        return changed_model
    else:
        return changed_model

def disable_FP_by_param(model):
    model.ITMX.setRTL(0,1,0)
    model.ITMY.setRTL(0,1,0)
    model.ETMX.setRTL(0,1,0)
    model.ETMY.setRTL(0,1,0)
    model.sx1.L   = 0
    model.sy1.L   = 0
    #model.ETMX.setRTL(0,0,1)
    #model.ETMY.setRTL(0,0,1)
    return model

def disable_PRC_by_param(model):
    model.PRM.setRTL(0,1,0)
    model.PR2.setRTL(1,0,0)
    model.PR3.setRTL(1,0,0)
    model.sLpr1.L = 0
    model.sLpr2.L = 0
    model.sLpr3.L = 0
    return model

def disable_SRC_by_param(model):
    model.SRM.setRTL(0,1,0)
    model.SR2.setRTL(1,0,0)
    model.SR3.setRTL(1,0,0)
    model.sLsr1.L = 0
    model.sLsr2.L = 0
    model.sLsr3.L = 0
    return model

def hom_extention(sim_conf, base):
    model = base.deepcopy()
    # HOM parse
    hom_extention = """
    # =========  HOM Expansion =======
    attr PRM Rc -458.1285
    attr PR2 Rc -3.0764
    attr PR3 Rc -24.9165
    attr MIBS Rc 0
    attr ITMX Rc -1900.   # measured -1904.6
    attr ETMX Rc 1900.    # measured  1908.24
    attr ITMY Rc -1900    # measured -1904.4
    attr ETMY Rc 1900.    # measured  1905.55
    attr SRM Rc 458.1285
    attr SR2 Rc -2.9872
    attr SR3 Rc 24.9165
    attr IXAR Rc 0
    attr IYAR Rc 0

    cav XARM ITMX nx2 ETMX nx3
    cav YARM ITMY ny2 ETMY ny3
    cav PRX PRM npr1 ITMX nx1
    cav PRY PRM npr1 ITMY ny1
    cav SRX SRM nsr1 ITMX nx1
    cav SRY SRM nsr1 ITMY ny1
    maxtem off
    """
    model.parse(hom_extention)
    model.maxtem = int(sim_conf["k_inf_c_num_of_hom_order"])
    #print(model)
    return model

def gui_param_to_model_param(sim_conf, model):
    #print(model)
    # laser power
    model.i1.P = sim_conf["k_inf_c_laser_power"]
    # transmittance
    model.MIBS.T = sim_conf["k_inf_c_mibs_mirror_transmittance"]
    model.ITMX.T = sim_conf["k_inf_c_itmx_mirror_transmittance"]
    model.ITMY.T = sim_conf["k_inf_c_itmy_mirror_transmittance"]
    model.ETMX.T = sim_conf["k_inf_c_etmx_mirror_transmittance"]
    model.ETMY.T = sim_conf["k_inf_c_etmy_mirror_transmittance"]
    model.PRM.T  = sim_conf["k_inf_c_prm_mirror_transmittance"]
    model.SRM.T  = sim_conf["k_inf_c_srm_mirror_transmittance"]
    model.PR2.T  = sim_conf["k_inf_c_pr2_mirror_transmittance"]
    model.PR3.T  = sim_conf["k_inf_c_pr3_mirror_transmittance"]
    model.SR2.T  = sim_conf["k_inf_c_sr2_mirror_transmittance"]
    model.SR3.T  = sim_conf["k_inf_c_sr3_mirror_transmittance"]
    # loss
    model.MIBS.L = sim_conf["k_inf_c_mibs_mirror_loss"]
    model.ITMX.L = sim_conf["k_inf_c_itmx_mirror_loss"]
    model.ITMY.L = sim_conf["k_inf_c_itmy_mirror_loss"]
    model.ETMX.L = sim_conf["k_inf_c_etmx_mirror_loss"]
    model.ETMY.L = sim_conf["k_inf_c_etmy_mirror_loss"]
    model.PRM.L  = sim_conf["k_inf_c_prm_mirror_loss"]
    model.SRM.L  = sim_conf["k_inf_c_srm_mirror_loss"]
    model.PR2.L  = sim_conf["k_inf_c_pr2_mirror_loss"]
    model.PR3.L  = sim_conf["k_inf_c_pr3_mirror_loss"]
    model.SR2.L  = sim_conf["k_inf_c_sr2_mirror_loss"]
    model.SR3.L  = sim_conf["k_inf_c_sr3_mirror_loss"]
    # reflectance
    model.MIBS.R = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_mibs_mirror_loss"]) - change_nums_unit_str_to_float(sim_conf["k_inf_c_mibs_mirror_transmittance"])
    model.ITMX.R = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_itmx_mirror_loss"]) - change_nums_unit_str_to_float(sim_conf["k_inf_c_itmx_mirror_transmittance"])
    model.ITMY.R = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_itmy_mirror_loss"]) - change_nums_unit_str_to_float(sim_conf["k_inf_c_itmy_mirror_transmittance"])
    model.ETMX.R = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_etmx_mirror_loss"]) - change_nums_unit_str_to_float(sim_conf["k_inf_c_etmx_mirror_transmittance"])
    model.ETMY.R = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_etmy_mirror_loss"]) - change_nums_unit_str_to_float(sim_conf["k_inf_c_etmy_mirror_transmittance"])
    model.PRM.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_prm_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_prm_mirror_transmittance"])
    model.SRM.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_srm_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_srm_mirror_transmittance"])
    model.PR2.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_pr2_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_pr2_mirror_transmittance"])
    model.PR3.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_pr3_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_pr3_mirror_transmittance"])
    model.SR2.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_sr2_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_sr2_mirror_transmittance"])
    model.SR3.R  = 1 - change_nums_unit_str_to_float(sim_conf["k_inf_c_sr3_mirror_loss"])  - change_nums_unit_str_to_float(sim_conf["k_inf_c_sr3_mirror_transmittance"])
    # Length
    model.sLpr1.L = sim_conf["k_inf_c_length_prm_pr2"]
    model.sLpr2.L = sim_conf["k_inf_c_length_pr2_pr3"]
    model.sLpr3.L = sim_conf["k_inf_c_length_pr3_bs"]

    model.lx.L    = sim_conf["k_inf_c_length_bs_itmx"]
    model.ly.L    = sim_conf["k_inf_c_length_bs_itmy"]

    model.sx1.L   = sim_conf["k_inf_c_length_itmx_etmx"]
    model.sy1.L   = sim_conf["k_inf_c_length_itmy_etmy"]

    model.sLsr1.L = sim_conf["k_inf_c_length_srm_sr2"]
    model.sLsr2.L = sim_conf["k_inf_c_length_sr2_sr3"]
    model.sLsr3.L = sim_conf["k_inf_c_length_sr3_bs"]
    # EOM
    model.eom1.f     = sim_conf["k_inf_c_f1_mod_frequency"]
    model.eom1.midx  = sim_conf["k_inf_c_f1_mod_index"]
    model.eom1.order = sim_conf["k_inf_c_num_of_sidebands"]
    model.eom2.f     = sim_conf["k_inf_c_f2_mod_frequency"]
    model.eom2.midx  = sim_conf["k_inf_c_f2_mod_index"]
    model.eom2.order = sim_conf["k_inf_c_num_of_sidebands"]

def get_all_port_list(ifo_name="KAGRA",ifo_conf="DRFPMI"):
    """
    現在の干渉計構成でPDを置くことができる全てのポートを返す。

    Attributes
    ----------
    ifo_name : string
        KAGRA, LIGO, VIRGO
        (only "KAGRA" is available for the simulation)
    ifo_conf : string
        MI, FPMI, PRFPMI, DRFPMI

    Returns
    -------
    all_port_list : string list
        port list where we can put PDs: REFL,AS,POP,POS, ...
    """
    if ifo_name=="KAGRA":
        all_port_list = ["REFL", "AS", "TMSX", "TMSY",
                            "POP",
                            "POS",
                            "npr1","npr2","npr3","npr4","npr5","npr6",
                            "nsr1","nsr2","nsr3","nsr4","nsr5",
                            "n2", "n3", # n1=REFL, n4=AS
                            "nx1", "nx2", "nx3",
                            "ny1", "ny2", "ny3",
                            "n0", "n_eo1", "n_eo2", "n_eo3", "n_eo4"]
        if ifo_conf=="MI":
            all_port_list.remove('POP')
            all_port_list.remove('POS')
        else:
            pass
    return all_port_list

def get_main_port_list(ifo_name="KAGRA",ifo_conf="DRFPMI"):
    """
    現在の干渉計構成でPDを置くことができる主要なポートを返す。

    Attributes
    ----------
    ifo_name : string
        KAGRA, LIGO, VIRGO
        (only "KAGRA" is available for the simulation)
    ifo_conf : string
        MI, FPMI, PRFPMI, DRFPMI

    Returns
    -------
    main_port_list : string list
        port list where we can put PDs: REFL,AS,POP,POS, ...
    """
    if ifo_name=="KAGRA":
        main_port_list = ["REFL", "AS", "TMSX", "TMSY","POP","POS"]
        if ifo_conf=="MI":
            main_port_list.remove('POP')
            main_port_list.remove('POS')
            main_port_list.remove('TMSX')
            main_port_list.remove('TMSY')
        elif ifo_conf=="FPMI":
            main_port_list.remove('POP')
            main_port_list.remove('POS')
        elif ifo_conf=="PRFPMI":
            main_port_list.remove('POS')
        elif ifo_conf=="DRFPMI":
            pass
        else:
            pass
    return main_port_list

def get_all_dof_list(interferometer):
    dof_list =[]
    if interferometer=="all_dof":
        dof_list = ["DARM", "CARM", "BS", "PRCL","SRCL"]
    if interferometer=="DRFPMI":
        dof_list = ["DARM", "CARM", "BS", "PRCL","SRCL"]
    if interferometer=="PRFPMI":
        dof_list = ["DARM", "CARM", "BS", "PRCL"]
    if interferometer=="FPMI":
        dof_list = ["DARM", "CARM", "BS"]
    if interferometer=="MI":
        dof_list = ["BS"]
    return dof_list

def get_all_mirror_list(interferometer,small_letter=False):
    """
        選択したinterferometerの構成での設定されているmirrorの名前をlistで返す

        Attributes
        ----------
        interferometer : str
        DRFPMI,PRFPMI,FPMI,MI...

        Returns
        -------
        dof_list : list[str]
        DoFのlist
    """
    if interferometer=="all_mirror":
        mirror_list = ["PRM","PR2","PR3",
                        "ITMX","ITMY",
                        "ETMX","ETMY",
                        "SRM","SR2","SR3",
                        "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "itmx", "itmy", "etmx", "etmy", "prm", "srm", "pr2", "pr3", "sr2", "sr3"]
    if interferometer=="DRFPMI":
        mirror_list = ["PRM","PR2","PR3",
                        "ITMX","ITMY",
                        "ETMX","ETMY",
                        "SRM","SR2","SR3",
                        "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "itmx", "itmy", "etmx", "etmy", "prm", "srm", "pr2", "pr3", "sr2", "sr3"]
    if interferometer=="PRFPMI":
        mirror_list = [
            "PRM","PR2","PR3",
            "ITMX","ITMY",
            "ETMX","ETMY",
            "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "itmx", "itmy", "etmx", "etmy", "prm", "pr2", "pr3"]
    if interferometer=="FPMI":
        mirror_list = [
            "ITMX","ITMY",
            "ETMX","ETMY",
            "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "itmx", "itmy", "etmx", "etmy"]
    if interferometer=="MI":
        mirror_list = [
            "ETMX","ETMY",
            "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "etmx", "etmy"]
    if interferometer=="PRMI":
        mirror_list = [
            "PRM","PR2","PR3",
            "ETMX","ETMY",
            "MIBS"]
        if small_letter==True:
            mirror_list = ["mibs", "etmx", "etmy", "prm", "pr2", "pr3"]
    return mirror_list

def get_all_parameter_names():
    all_parameter_key_names = []
    # laser
    key = "k_inf_c_laser_power"
    all_parameter_key_names.append(key)
    # mirror
    mirror_names = ["mibs", "itmx", "itmy", "etmx", "etmy", "prm", "srm", "pr2", "pr3", "sr2", "sr3"]
    mirror_paras = ["transmittance", "loss"]
    for mirror in mirror_names:
        for param in mirror_paras:
            key = "k_inf_c_%s_mirror_%s"%(mirror, param)
            all_parameter_key_names.append(key)
    # length
    length_names = ["prm_pr2", "pr2_pr3", "pr3_bs",
                    "srm_sr2", "sr2_sr3", "sr3_bs",
                    "bs_itmx", "bs_itmy", "itmx_etmx", "itmy_etmy"]
    for length in length_names:
        key = "k_inf_c_length_%s"%length
        all_parameter_key_names.append(key)
    # eom
    eom_names = ["f1", "f2"]
    eom_params = ["mod_frequency", "mod_index"]
    for eom in eom_names:
        for param in eom_params:
            key = "k_inf_c_%s_%s"%(eom, param)
            all_parameter_key_names.append(key)
    return all_parameter_key_names

def calc_optimal_dphase_IQ(out, I_pdname, Q_pdname, N):
    #
    # usage: mag, phase = utils_DRFPMI.demod_phase(out, port, SB, N):
    # out: the computed signals from run_fsig
    # N: integer related to the frequency. Should be updated.
    #
    #
    #jupyter notebook script
    
    tot = out[I_pdname] + 1j*out[Q_pdname]
    #print(type(out[I_pdname]))
    #print(type(out[Q_pdname]))
    #mag=1
    phase = np.angle(tot)*180/np.pi
    # kokeyamasan script
    #I = np.sign(np.sin(np.angle(out[I_pdname][N])))* np.abs(out[I_pdname][N])
    #print("out_I = ")
    #print(I) #0.0になる
    #Q = np.sign(np.sin(np.angle(out[Q_pdname][N])))* np.abs(out[Q_pdname][N])
    #print("out_Q = ")
    #print(Q) #0.0になる
    #phase = np.arctan(Q/I)*180/np.pi
    #print("optimalphase = ")
    #print(phase) #nanになる
    #mag = np.sqrt(I**2 + Q**2)
    #print("mag = ")
    #print(mag)
    return phase













