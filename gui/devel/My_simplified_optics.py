
class My_simplified_laser():
    def __init__(self, finesse_laser):
        self._Component__name      = finesse_laser._Component__name
        self._requested_node_names = finesse_laser._requested_node_names
        self._Component__id        = finesse_laser._Component__id
        self.simplified_params                 =  {
        "power"                               :  finesse_laser._laser__power,      
        "f_offset"                            :  finesse_laser._laser__f_offset,      
        "phase"                               :  finesse_laser._laser__phase,      
        "w0"                                  :  finesse_laser._laser__w0,    
        "z"                                   :  finesse_laser._laser__z,    
        "noise"                               :  finesse_laser._laser__noise
        }  
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

class My_simplified_mirror():
    def __init__(self, finesse_component):
        self._Component__name                  =  finesse_component._Component__name
        self._requested_node_names             =  finesse_component._requested_node_names            
        self._Component__id                    =  finesse_component._Component__id             
        self.simplified_params                 =  {
        "R"                                    :  finesse_component._AbstractMirrorComponent__R,      
        "T"                                    :  finesse_component._AbstractMirrorComponent__T,      
        "L"                                    :  finesse_component._AbstractMirrorComponent__L,      
        "phi"                                  :  finesse_component._AbstractMirrorComponent__phi,    
        "Rcx"                                  :  finesse_component._AbstractMirrorComponent__Rcx,    
        "Rcy"                                  :  finesse_component._AbstractMirrorComponent__Rcy,    
        "xbeta"                                :  finesse_component._AbstractMirrorComponent__xbeta,  
        "ybeta"                                :  finesse_component._AbstractMirrorComponent__ybeta,  
        "mass"                                 :  finesse_component._AbstractMirrorComponent__mass, 
        "Ix"                                   :  finesse_component._AbstractMirrorComponent__Ix,     
        "Iy"                                   :  finesse_component._AbstractMirrorComponent__Iy,     
        "r_ap"                                 :  finesse_component._AbstractMirrorComponent__r_ap,   
        "zmech"                                :  finesse_component._AbstractMirrorComponent__zmech,  
        "rxmech"                               :  finesse_component._AbstractMirrorComponent__rxmech, 
        "rymech"                               :  finesse_component._AbstractMirrorComponent__rymech, 
        "z"                                    :  finesse_component._AbstractMirrorComponent__z,      
        "rx"                                   :  finesse_component._AbstractMirrorComponent__rx,     
        "ry"                                   :  finesse_component._AbstractMirrorComponent__ry,     
        "Fz"                                   :  finesse_component._AbstractMirrorComponent__Fz,     
        "Frx"                                  :  finesse_component._AbstractMirrorComponent__Frx,    
        "Fry"                                  :  finesse_component._AbstractMirrorComponent__Fry,    
        "Fs0"                                  :  finesse_component._AbstractMirrorComponent__Fs0,    
        "Fs1"                                  :  finesse_component._AbstractMirrorComponent__Fs1
        }  
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
          
class My_simplified_beamSplitter():
    def __init__(self, finesse_component):
        self._Component__name                  =  finesse_component._Component__name
        self._requested_node_names             =  finesse_component._requested_node_names
        self._Component__id                    =  finesse_component._Component__id
        self.simplified_params                 =  {
        "R"                                 :  finesse_component._params[0],
        "T"                                 :  finesse_component._params[1],
        "L"                                 :  finesse_component._params[2],
        "phi"                               :  finesse_component._params[3],
        "Rcx"                               :  finesse_component._params[4],
        "Rcy"                               :  finesse_component._params[5],
        "xbeta"                             :  finesse_component._params[6],
        "ybeta"                             :  finesse_component._params[7],
        "mass"                              :  finesse_component._params[8],
        "Ix"                                :  finesse_component._params[9],#yawの回転の慣性モーメント
        "Iy"                                :  finesse_component._params[10],#yawの回転の慣性モーメント
        "r_ap"                              :  finesse_component._params[11],#mirrorの大きさ[m]
        "zmech"                             :  finesse_component._params[12],#zmech = transfer function of Longitudinal motion
        "rxmech"                            :  finesse_component._params[13],#rxmech = transfer function of yaw motion
        "rymech"                            :  finesse_component._params[14],#rymech = transfer function of pitch motion
        "z"                                 :  finesse_component._params[15],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: z = longitudinal motion
        "rx"                                :  finesse_component._params[16],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: rx, yaw = x-z plane rotation
        "ry"                                :  finesse_component._params[17],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: ry, pitch = y-z plane rotation
        "Fz"                                :  finesse_component._params[18],# target property of the fsig to Fz, i.e. a force to the z motion.
        "Frx"                               :  finesse_component._params[19],#force in the rotational x-z plane motion.
        "Fry"                               :  finesse_component._params[20],#force in the rotational y-z plane motion.
        "Fs0"                               :  finesse_component._params[21],#よくわからない,おそらくsurface motionのtransfer functionのなにか
        "Fs1"                               :  finesse_component._params[22],#よくわからない,おそらくsurface motionのtransfer functionのなにか
        "s0"                                :  finesse_component._params[23],#よくわからない,おそらくsurface motionのなにか
        "s1"                                :  finesse_component._params[24],#よくわからない,おそらくsurface　motionのなにか
        "alpha"                             :  finesse_component._params[25]
        }
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
        
class My_simplified_space():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.simplified_params              =  {
        "L"                                 :  finesse_component.L,
        "n"                                 :  finesse_component.n,
        "gouyx"                             :  finesse_component.gouyx,
        "gouyy"                             :  finesse_component.gouyy,
        }

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

# modulator
class My_simplified_modulator():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.simplified_params              =  {
        "f"                                 :  finesse_component.f,
        "midx"                              :  finesse_component.midx,
        "phase"                             :  finesse_component.phase,
        "order"                             :  finesse_component.order,
        "type"                              :  finesse_component.type,
        }

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
# lens
class My_simplified_lens():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.simplified_params              =  {
        "f"                                 :  finesse_component.f,
        "p"                                 :  finesse_component.p,
        }

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
