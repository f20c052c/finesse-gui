#!/usr/bin/env python
# coding: utf-8

# In[1]:


import PySimpleGUI as sg
from Layout.Left_layout import Left_layout
from Layout.Right_layout import Right_dc_power_layout
from Layout.Right_layout import Right_amplitude_layout
from Layout.Right_layout import Right_WFS_layout
from Layout.Right_layout import Right_simulation_result_layout
from Utility import utility
from Utility import Asc_base_model
from Utility import My_simplified_optics
from pykat import finesse
import copy
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasAgg
import matplotlib.figure
import matplotlib.pyplot as plt
#get_ipython().run_line_magic('matplotlib', 'tk')
import io
from matplotlib import cm
from mpl_toolkits.mplot3d.axes3d import get_test_data
from matplotlib.ticker import NullFormatter  # useful for `logit` scale

sg.theme('GreenTan') # give our window a spiffy set of colors


# %% [markdown]
# # layout

# %%

layout_left_0 = Left_layout.Return_left_layout_0()
layout_left_1 = Left_layout.Return_left_layout_1()
layout_left_2 = Left_layout.Return_left_layout_2()
#layout_left_3 = Return_left_layout_3()
layout_left_4 = Left_layout.Return_left_layout_4()
layout_left_5 = Left_layout.Return_left_layout_5()

DC_power_tab  = Right_dc_power_layout.Return_layout_right()
Amplitude_tab = Right_amplitude_layout.Return_layout_right()
WFS_tab       = Right_WFS_layout.Return_layout_right()
SImulation_result_tab = Right_simulation_result_layout.Return_simulation_reusult_tab()#Right_simulation_result_layout.Return_right_layout()

layout_left_top = [
    [sg.TabGroup(
        [[sg.Tab("Base model",layout_left_0,key=None)]
        ,[sg.Tab("Extra settings",layout_left_1,key=None)]
        ,[sg.Tab("Beam parameter check",layout_left_2,key=None)]
        #,[sg.Tab("Simulation result",layout_left_3,key=None)]
        ,[sg.Tab("Source script style",layout_left_4,key=None)]
        ,[sg.Tab("Stdout",layout_left_5,key=None)]
        ]
    ,expand_x=True,expand_y=True,size=(640,1200))]
]
layout_right_top = [
    [sg.TabGroup(
        [[sg.Tab("DC Power"            ,DC_power_tab,key=None)]
        ,[sg.Tab("Amplitude"           ,Amplitude_tab,key=None)]
        ,[sg.Tab("Demodulation signal" ,WFS_tab,key=None)]
        ,[sg.Tab("Simulation result"   ,SImulation_result_tab,key=None)]
        ]
    ,expand_x=True,expand_y=True,size=(640,1200))]
]

layout = [
    [sg.Frame("left" ,layout_left_top ,size=(640,720),expand_x = True,expand_y = True)
    ,sg.Frame("right",layout_right_top,size=(640,720),expand_x = True,expand_y = True)]
    ]
    

# %%
class out_figure_axes:
    """
    Parameter
    --------------------
    configuration_dict : dictionary

    key : configuration_dictionary{

    model           : finesse.kat(),
    out             : finesse.run(),
    axes_title      : str,
    xlabel          : str,
    ylabel          : str,
    scale           : str ex) linear-linear,
    plot_style      : str,
    detectors_info  : dictionary
    key : detectors_info{
        pdname      : {line_style : str, line_color : str},
    }
    }
    set_linestyle(configuration_dictionary['line_style'])
    """
    def __init__(self,fig,fig_num,configuration_dictionary,out):
        self.axes = fig.add_subplot(fig_num[0],fig_num[1],fig_num[2]
                                    ,xlabel = configuration_dictionary['xlabel']
                                    ,ylabel = configuration_dictionary['xlabel'])
        self.set_axes_title(configuration_dictionary)
        self.set_xlabel(configuration_dictionary)
        self.set_ylabel(configuration_dictionary)
        self.set_scale(configuration_dictionary)
        self.set_detectors(configuration_dictionary,out)

    def set_axes_title(self,configuration_dictionary):
        self.axes.set_title(configuration_dictionary['axes_title'])
    def set_xlabel(self,configuration_dictionary):
        self.axes.set_xlabel(configuration_dictionary['xlabel'])
    def set_ylabel(self,configuration_dictionary):
        self.axes.set_ylabel(configuration_dictionary['ylabel'])
    def set_scale(self,configuration_dictionary):
        xscale = configuration_dictionary['scale'].split('-')[0]
        yscale = configuration_dictionary['scale'].split('-')[1]
        self.axes.set_xscale(xscale)
        self.axes.set_yscale(yscale)
    def set_detectors(self,configuration_dictionary,out):
        detectors =list(configuration_dictionary["detectors_info"].keys())

        def plot(pdname,x,y,configuration_dictionary):
            line_color = configuration_dictionary["detectors_info"][pdname]["line_color"]
            line_style = configuration_dictionary["detectors_info"][pdname]["line_style"]
            max_index = np.argmax(y)
            min_index = np.argmin(y)
            if line_color!="default" and line_style!="default":
                self.axes.plot(x,y,label = pdname,linestyle=line_style,color=line_color)
            elif line_color!="default" and line_style=="default":
                self.axes.plot(x,y,label = pdname,color=line_color)
            elif line_color=="default" and line_style!="default":
                self.axes.plot(x,y,label = pdname,linestyle=line_style)
            elif line_color=="default" and line_style=="default":
                self.axes.plot(x,y,label = pdname)
            #round(1.234, 2)
            self.axes.annotate(f"({x[max_index]},{round(y[max_index],3)})", xy=(x[max_index], y[max_index]))
            self.axes.annotate(f"({x[min_index]},{round(y[min_index],3)})", xy=(x[min_index], y[min_index]))
            self.axes.plot(x[max_index], y[max_index],'o')
            self.axes.plot(x[min_index], y[min_index],'o')

        if configuration_dictionary['plot_style']=="default":
            for pdname in detectors:
                x = out.x
                y = out[pdname]
                plot(pdname,x,y,configuration_dictionary)
        elif configuration_dictionary['plot_style']=="power":
            for pdname in detectors:
                x = out.x
                y = out[pdname]**2
                plot(pdname,x,y,configuration_dictionary)
                self.axes.set_ylabel(f"({configuration_dictionary['ylabel']})**2")
        elif configuration_dictionary['plot_style']=="abs power":
            for pdname in detectors:
                x = out.x
                y = np.abs(out[pdname])**2
                plot(pdname,x,y,configuration_dictionary)
                self.axes.set_ylabel(f"abs({configuration_dictionary['ylabel']})**2")
        elif configuration_dictionary['plot_style']=="abs":
            for pdname in detectors:
                x = out.x
                y = np.abs(out[pdname])
                plot(pdname,x,y,configuration_dictionary)
                self.axes.set_ylabel(f"abs({configuration_dictionary['ylabel']})")
        elif configuration_dictionary['plot_style']=="angle":
            for pdname in detectors:
                x = out.x
                y = np.angle(out[pdname])
                plot(pdname,x,y,configuration_dictionary)
                self.axes.set_ylabel(f"np.angle({configuration_dictionary['ylabel']})")
        else:
            for pdname in detectors:
                x = out.x
                y = out[pdname]
                plot(pdname,x,y,configuration_dictionary)
        return self.axes

    def sample(self):
        price = [100, 250, 380, 500, 700]
        number = [1, 2, 3, 4, 5]
        self.axes.plot(price, number, marker="x")
        return self.axes

class out_figure:
    """
    Parameter
    --------------------
    configuration_dict : dictionary

    key : configuration_dictionary{
    "figure_title"  : str,
    "axes_dict"     : {
    "axes1_dict"    : {
                        "model"       : base,
                        "out"         : out,
                        "axes_title"  : "default_title",
                        "xlabel"      : "default",
                        "ylabel"      : "default",
                        "scale"       : "linear-linear",
                        "plot_style"  : "default",
                        "detectors_info":{
                            "REFL_amplitude_yaw":{"line_style" : "-", "line_color" : "b"}
                        },
    "axes2_dict"    : axes2_dict,
    "axes3_dict"    : axes3_dict,
    "axes4_dict"    : axes4_dict
    }
    }
    def __init__(self,model,out):
        self.figure = plt.figure()
        self.title  = ""
        self.model = model
        self.out = out
    """
    def __init__(self,configuration_dict,model,out,display_in_gui):
        self.model = model
        self.out = out
        if display_in_gui:
            self.figure = matplotlib.figure.Figure(figsize=(6, 4), dpi=100, tight_layout=True)
            #self.figure = matplotlib.figure.Figure(figsize=(4, 3), dpi=100, tight_layout=True)
        else:
            self.figure = plt.figure(figsize=(6, 4), dpi=100, tight_layout=True)
            #self.figure = plt.figure(figsize=(4, 3), dpi=100, tight_layout=True)
        plt.rcParams["font.size"] = 8
        self.set_figure_title(configuration_dict["figure_title"])
        test_dictionary = {}
        v=1
        h=1
        for i in range(configuration_dict["axes_max_num"]):
            t = i+1
            if h*v<t and h>=v:
                h+=1
            elif h*v<t and h<v:
                v+=1
        for i in range(configuration_dict["axes_max_num"]):
            t = i+1
            test_dictionary[f"ax{t}"] = out_figure_axes(self.figure,[h,v,t],configuration_dict["axes_dict"][f"axes{t}_dict"],self.out).axes
            test_dictionary[f"ax{t}"].legend()
        """
        ax1 = out_figure_axes(self.figure,[2,2,1],axes_dict).axes
        ax2 = out_figure_axes(self.figure,[2,2,2],axes_dict).axes
        ax3 = out_figure_axes(self.figure,[2,2,3],axes_dict).axes
        ax4 = out_figure_axes(self.figure,[2,2,4],axes_dict).axes
        """
        #plt.tight_layout()
        plt.show()
    def add_axes(self,):
        pass
    def set_figure_title(self,title):
        self.figure.suptitle(title)
    def get_figure(self):
        return self.figure

# %%

def reset_simulation_configuration_dict(configuration_dict,out_history_key="current_out"):
    configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "default figure title"
    configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "default ylabels"
    configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "default xlabels"
    configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
    configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"

def set_simulation_configuration_dict(window,values,configuration_dict,simulation_name,out_history_key="current_out"):
    
    err_text = ""
    configuration_dict["out_plot_conf_dict"] = {}
    configuration_dict["out_plot_conf_dict"][out_history_key] = {}
    if simulation_name == "DC_power":
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "PD/QPD power"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "power[W]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "[deg]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"
    if simulation_name == "amplitude":
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "field amplitude"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "sqrt([W])"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "[deg]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"
    if simulation_name == "WFS3":#gouy phase optimization
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "gouy phase optimization"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "[W]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "[deg]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"
    if simulation_name == "WFS4":#demod phase optimization
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "demod phase optimization(g=?)"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "[W]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "[deg]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"
    if simulation_name == "demod_DC_sweep_signal":#demod error signal
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "demod signal"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "[W]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "[deg]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "linear-linear"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "default"
    elif simulation_name == "demod_transfer_function":
        # プロット設定
        configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"]             = "transfer function"
        configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"]                  = "demod signal[A.U.]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"]                  = "Oscillation frequency[Hz]"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"]        = "log-log"
        configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"] = "abs+angle"

    # windowの表示
    window["layout_right_simulation_result_figure_title"].update(value=configuration_dict["out_plot_conf_dict"][out_history_key]["figure_title"])
    window["layout_right_simulation_result_ylabels"].update(value=configuration_dict["out_plot_conf_dict"][out_history_key]["ylabels"])
    window["layout_right_simulation_result_xlabels"].update(value=configuration_dict["out_plot_conf_dict"][out_history_key]["xlabels"])
    window["layout_right_simulation_result_plot_scale"].update(value=configuration_dict["out_plot_conf_dict"][out_history_key]["result_plot_scale"])
    window["layout_right_simulation_result_select_plot_style"].update(value=configuration_dict["out_plot_conf_dict"][out_history_key]["result_select_plot_style"])
    return window,values,configuration_dict,err_text

# %%
def get_sorted_index(searched_line_list,script_text_in_list_format):
    # 検索する文を改行で区切ったlistで受け取り、indexの順に並び替える
    index_list = []
    for s in searched_line_list:
        index_list.append(script_text_in_list_format.index(s))
        #print(script_text_in_list_format.index(s))
    print(index_list)
    sorted_list = sorted(index_list)
    #print(sorted_list)
    return sorted_list
# リストで受け取ったインデックスの場所で分割したリストを返す
def list_by_received_index_location_by_divided_list_returns(sorted_index_list,script_text_in_list_format):
    devided_script_list = []
    if len(sorted_index_list)==1:
        #
        last_part_a = script_text_in_list_format[:sorted_index_list[0]]
        # print
        devided_script_list.append(last_part_a)

        #
        searched_line = [script_text_in_list_format[sorted_index_list[0]]]#last_index = len(list)-1
        devided_script_list.append(searched_line)

        #
        last_part_b = script_text_in_list_format[sorted_index_list[0]:]
        # print
        del last_part_b[0]
        devided_script_list.append(last_part_b)
    else:
        for i in range(len(sorted_index_list)):
            # first
            if i == 0:
                first_part = script_text_in_list_format[:sorted_index_list[i]]
                devided_script_list.append(first_part)
                #print(i)
            # last
            elif i == len(sorted_index_list)-1:
                last_part_a = script_text_in_list_format[sorted_index_list[i-1]:sorted_index_list[i]]
                # cprint
                searched_line = [last_part_a[0]]
                devided_script_list.append(searched_line)
                # print
                del last_part_a[0]
                devided_script_list.append(last_part_a)

                last_part_b = script_text_in_list_format[sorted_index_list[i]:]#last_index = len(list)-1
                # cprint
                searched_line = [last_part_b[0]]
                devided_script_list.append(searched_line)
                # print
                del last_part_b[0]
                devided_script_list.append(last_part_b)
                #print(i)
            # 中間部分
            else:
                middle_part_x = script_text_in_list_format[sorted_index_list[i-1]:sorted_index_list[i]]
                # cprint
                searched_line = [middle_part_x[0]]
                devided_script_list.append(searched_line)
                # print
                del middle_part_x[0]
                devided_script_list.append(middle_part_x)
                #print(i)
    return devided_script_list
    

# %% [markdown]
# # Generate GUI

# %%
#from cgi import test

# Initialization
def Initialization(window,values,configuration_dict):
    base_to_be_copied = configuration_dict["base"]
    err_text = ""# エラーが出たらこの中にテキストを入れる
    configuration_dict["model"] = base_to_be_copied
    # left top
    #   Base settings
    window["base_model_filter_items"].update(values=[])
    window["base_model_filter_current_displayed"].update(value="All kat script")
    #   multilineを更新
    kat_script = utility.remove_header('\n'.join(configuration_dict["model"].generateKatScript()))
    window["base_model_multiline"].update(value=kat_script)

    #   Extra settings
    window["layout_left_1_Extra_Settings_class_name"].update(values=list(utility.classify_model_components(configuration_dict["model"]).keys()))
    window["layout_left_1_Extra_Settings_component_name"].update(values=[])
    window["layout_left_1_Extra_Settings_component_name"].update(value="")
    window["layout_left_1_Extra_Settings_atrribute"].update(values=[])
    window["layout_left_1_Extra_Settings_atrribute"].update(value="")
    window["layout_left_1_Extra_Settings_parameter_multiline"].update(value="")
    if type(configuration_dict["model"]) is not int:
        configuration_dict["model"].maxtem = 1
    window["layout_left_1_Extra_Settings_maxtem"].update(value=configuration_dict["model"].maxtem)
    #   #beam parameter check
    space_components = utility.classify_model_components(configuration_dict["model"])["space"]
    window["bp_check_select_length"].update(values = space_components)
    window["bp_check_select_length"].update(value="")

    # right top
        # dc power
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_dc_power_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
        # amplitude
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_amplitude_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
        # wfs1
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_wfs1_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
        # wfs2 はなし
        
        # wfs3 gouy phase optimization
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_wfs3_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_wfs3_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
        # wfs4 demod phase optimization
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_wfs4_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_wfs4_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
        # wfs5 demod phase optimization
        #   # pd部分
    window,values,configuration_dict,err_text = layout_right_wfs5_pd_setting_initialization(window,values,configuration_dict)
        #   # xaxis setting
    window,values,configuration_dict,err_text = layout_right_wfs5_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict)
    #   # simulation tab
    
    return window,values,configuration_dict,err_text


# matplot
def create_figure():
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    import random
    x = random.randint(0,2)
    fig = matplotlib.figure.Figure(figsize=(1, 1), dpi=100)
    if x == 1:
        fig = matplotlib.figure.Figure(figsize=(4, 3), dpi=100)
        t = np.arange(0, 3, .01)
        fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))
    else:
        fig = matplotlib.figure.Figure(figsize=(4, 3), dpi=100)
        t = np.arange(0, 3, .01)
        fig.add_subplot(111).plot(t, 2 * np.cos(2 * np.pi * t))
        

    return fig

def My_create_figure_copy(out,xscale,yscale,detector_names,title,xlabel,ylabel):
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = matplotlib.figure.Figure(figsize=(4, 3), dpi=100)
    t = np.arange(0, 3, .01)
    for pdname in out.ylabels:
        ax1 = fig.add_subplot(111)
        ax1.plot(out.x, out[pdname])

    return fig
def My_create_bp_figure_interactive(out,scale,figure_title,xlabel,ylabel,display_detector_names,start_text,end_text,rad2deg=True):
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = plt.figure(figsize=(4, 3), dpi=100,tight_layout=True)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    fig.suptitle(figure_title)
    xscale = scale.split("-")[0].replace("\n","").replace(" ","")
    yscale = scale.split("-")[1].replace("\n","").replace(" ","")
    ax1.set_xscale(xscale)
    ax1.set_yscale(yscale)
    end_num = len(out.x)-1
    if rad2deg:# degreeに変換する
        for pdname in display_detector_names:
            ax1.plot(out.x, np.rad2deg(out[pdname]),label = pdname)
            ax1.text(out.x[0], np.rad2deg(out[pdname][0]), start_text)
            ax1.text(out.x[end_num], np.rad2deg(out[pdname][end_num]), end_text)
            ax1.legend()
    else:
        for pdname in display_detector_names:
            ax1.plot(out.x, out[pdname],label = pdname)
            ax1.text(out.x[0], out[pdname][0], start_text)
            ax1.text(out.x[end_num], out[pdname][end_num], end_text)
            ax1.legend()
    plt.show(block=False)
    return fig

def My_create_bp_figure(out,scale,figure_title,xlabel,ylabel,display_detector_names,start_text,end_text,rad2deg=True):
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = matplotlib.figure.Figure(figsize=(4, 3), dpi=100,tight_layout=True)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    fig.suptitle(figure_title)
    xscale = scale.split("-")[0].replace("\n","").replace(" ","")
    yscale = scale.split("-")[1].replace("\n","").replace(" ","")
    ax1.set_xscale(xscale)
    ax1.set_yscale(yscale)
    end_num = len(out.x)-1
    if rad2deg:# degreeに変換する
        for pdname in display_detector_names:
            ax1.plot(out.x, np.rad2deg(out[pdname]),label = pdname)
            ax1.text(out.x[0],       np.rad2deg(out[pdname][0]),       start_text)
            ax1.text(out.x[end_num], np.rad2deg(out[pdname][end_num]), end_text)
            ax1.legend()
    else:
        for pdname in display_detector_names:
            ax1.plot(out.x, out[pdname],label = pdname)
            ax1.text(out.x[0], out[pdname][0], start_text)
            ax1.text(out.x[end_num], out[pdname][end_num], end_text)
            ax1.legend()
    return fig

def My_create_figure2(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict):
    """
    matplotを画像をGUIに埋め込んだcanvasに表示
    """
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = matplotlib.figure.Figure(figsize=(4, 3), dpi=100, tight_layout=True)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    fig.suptitle(figure_title)
    xscale = scale.split("-")[0].replace("\n","").replace(" ","")
    yscale = scale.split("-")[1].replace("\n","").replace(" ","")
    ax1.set_xscale(xscale)
    ax1.set_yscale(yscale)
    for pdname in display_detector_names:
        line_color = configuration_dict["plot_conf_dict"][f"{pdname}_line_color"]
        line_style = configuration_dict["plot_conf_dict"][f"{pdname}_line_style"]
        if line_color!="default" and line_style!="default":
            ax1.plot(out.x, out[pdname],label = pdname,linestyle=line_style,color=line_color)
        elif line_color!="default" and line_style=="default":
            ax1.plot(out.x, out[pdname],label = pdname,color=line_color)
        elif line_color=="default" and line_style!="default":
            ax1.plot(out.x, out[pdname],label = pdname,linestyle=line_style)
        elif line_color=="default" and line_style=="default":
            ax1.plot(out.x, out[pdname],label = pdname)
        ax1.legend()
    return fig

def My_create_figure3(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict):
    """
    matplotlibの別ウィンドウを開く
    """
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = plt.figure(figsize=(4, 3), dpi=100,tight_layout=True)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    fig.suptitle(figure_title)
    xscale = scale.split("-")[0].replace("\n","").replace(" ","")
    yscale = scale.split("-")[1].replace("\n","").replace(" ","")
    ax1.set_xscale(xscale)
    ax1.set_yscale(yscale)
    for pdname in display_detector_names:
        line_color = configuration_dict["plot_conf_dict"][f"{pdname}_line_color"]
        line_style = configuration_dict["plot_conf_dict"][f"{pdname}_line_style"]
        if line_color!="default" and line_style!="default":
            ax1.plot(out.x, out[pdname],label = pdname,linestyle=line_style,color=line_color)
        elif line_color!="default" and line_style=="default":
            ax1.plot(out.x, out[pdname],label = pdname,color=line_color)
        elif line_color=="default" and line_style!="default":
            ax1.plot(out.x, out[pdname],label = pdname,linestyle=line_style)
        elif line_color=="default" and line_style=="default":
            ax1.plot(out.x, out[pdname],label = pdname)
        ax1.legend()
    plt.show(block=False)
    return fig

def My_create_figure4(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict,display_max=False,display_min=False,display_point=-1):
    """
    matplotlibの別ウィンドウを開く
    gouy phase optimizationやdemod phase optimizationなどでシミュレーション結果のタブを開かずに確認する時のやつ
    """
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = plt.figure(figsize=(4, 3), dpi=100,tight_layout=True)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    fig.suptitle(figure_title)
    xscale = scale.split("-")[0].replace("\n","").replace(" ","")
    yscale = scale.split("-")[1].replace("\n","").replace(" ","")
    ax1.set_xscale(xscale)
    ax1.set_yscale(yscale)
    for pdname in display_detector_names:
        ax1.plot(out.x, out[pdname],label = pdname)
        if display_max==True:
            max_index = np.argmax(out[pdname])
            max_num = max_index
            if out.x[max_index]<0:
                max_num = out.x[max_index]+360
            plt.annotate(f"{out.x[max_index]}({max_num})", xy=(out.x[max_index], out[pdname][max_index]))
            plt.plot(out.x[max_index], out[pdname][max_index],'o')
        if display_min==True:
            min_index = np.argmin(out[pdname])
            min_num = min_index
            if out.x[min_index]<0:
                min_num = out.x[min_index]+360
            plt.annotate(f"{out.x[min_index]}({min_num})", xy=(out.x[min_index], out[pdname][min_index]))
            plt.plot(out.x[min_index], out[pdname][min_index],'o')
        if display_point!=-1:
            plt.annotate(f"{out.x[display_point]}", xy=(out.x[display_point], out[pdname][display_point]))
            plt.plot(out.x[display_point], out[pdname][display_point],'o')
        ax1.legend()
    plt.show(block=False)
    return fig

def My_create_figure5(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict,display_max=False,display_min=False,display_point=-1):
    """
    matplotlibの別ウィンドウを開く
    gouy phase optimizationやdemod phase optimizationなどでシミュレーション結果のタブを開かずに確認する時のやつ
    """
    # ------------------------------- START OF YOUR MATPLOTLIB CODE -------------------------------
    fig = plt.figure(figsize=(4, 3), dpi=100,tight_layout=True)
    ax1 = fig.add_subplot(211)
    ax1.set_xlabel("frequency[Hz]")
    ax1.set_ylabel("abs")
    fig.suptitle("WFS sencing matrix")
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax2 = fig.add_subplot(212)
    ax2.set_xscale("log")
    ax2.set_yscale("linear")
    ax2.set_xlabel("frequency[Hz]")
    ax2.set_ylabel("angle[deg]")
    for pdname in display_detector_names:
        # abs
        ax1.plot(out.x, np.abs(out[pdname]),label = pdname)
        if display_max==True:
            max_index = np.argmax(out[pdname])
            max_num = max_index
            if out.x[max_index]<0:
                max_num = out.x[max_index]+360
            ax1.annotate(f"{out.x[max_index]}({max_num})", xy=(out.x[max_index], out[pdname][max_index]))
            ax1.plot(out.x[max_index], out[pdname][max_index],'o')
        if display_min==True:
            min_index = np.argmin(out[pdname])
            min_num = min_index
            if out.x[min_index]<0:
                min_num = out.x[min_index]+360
            ax1.annotate(f"{out.x[min_index]}({min_num})", xy=(out.x[min_index], out[pdname][min_index]))
            ax1.plot(out.x[min_index], out[pdname][min_index],'o')
        if display_point!=-1:
            ax1.annotate(f"{out.x[display_point]}", xy=(out.x[display_point], out[pdname][display_point]))
            ax1.plot(out.x[display_point], out[pdname][display_point],'o')
        ax1.legend()
        # phase
        ax2.plot(out.x, np.angle(out[pdname])/np.pi*360,label = pdname)
        ax2.legend()
    plt.show(block=False)
    return fig


# ----------------------------- The draw figure helpful function -----------------------------
def draw_figure(element, figure):
    """
    Draws the previously created "figure" in the supplied Image Element

    :param element: an Image Element
    :param figure: a Matplotlib figure
    :return: The figure canvas
    """

    plt.close('all')  # erases previously drawn plots
    canv = FigureCanvasAgg(figure)
    buf = io.BytesIO()
    canv.print_figure(buf, format='png')
    if buf is None:
        return None
    buf.seek(0)
    element.update(data=buf.read())
    return canv


dictionary_of_figures = {'Basic Figure': create_figure}


# ----------------------------- The GUI Section -----------------------------
def create_window():
    """
    Defines the window's layout and creates the window object.
    This function is used so that the window's theme can be changed and the window "re-started".

    :return: The Window object
    :rtype: sg.Window
    """

    left_col = [[sg.T('Figures to Draw')],
                [sg.Listbox(list(dictionary_of_figures), default_values=[list(dictionary_of_figures)[0]], size=(15, 5), key='-LB-')]
    ]

    layout = [[sg.T('Matplotlib Example', font='Any 20')],
              [sg.Col(left_col), sg.Image(key='-IMAGE-')],
              [sg.B('Draw'), sg.B('Exit')]]

    window = sg.Window('Matplotlib Embedded Template', layout, finalize=True)

    return window

# Window,Values,model
def base_model_update_base_model(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        # 注意を表示してOKの時だけ動作する
        notification_layout = [
            [sg.Text("""Use "Extra setting" to update parameters.\nThis function should be used to change the whole source model.\nOr, reset the model to default.""")],
            [sg.Button('OK'), sg.Exit()]]      
        notification_window = sg.Window('Window that stays open', notification_layout)      
        while True:                             # The Event Loop
            event, temp_values = notification_window.read() 
            if event == sg.WIN_CLOSED or event == 'Exit':
                break
            if event == "OK":
                try:
                    temp = finesse.kat()
                    temp.parse(values["base_model_multiline"])
                    configuration_dict["base"] = temp
                except Exception as e:
                    print(e)
                    err_text="FINESSE could not read the model.\nPlease check the model for errors.\n"
                    notification_window.close()
                    return window,values,configuration_dict,err_text 
                except:
                    err_text = "sys.exit(),SystemExit"
                    return window,values,configuration_dict,err_text
                break
        notification_window.close()
        # モデルの更新にあわせてwindowを更新する
        window,values,configuration_dict,err_text = Initialization(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
        # ポップアップ
        text_input = "Model updated."
        sg.popup('notification', text_input)
    except Exception as e:
        print(e)
        err_text = "Model could not be updated."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text
def base_model_default_base_model(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    # ポップアップ
    text_input = "未実装なのでBase modelのタブからFPMI,PRFPMI,PRMIなどを選択してアップデートしてください"
    sg.popup('notification', text_input,keep_on_top=True)
    """
    # 注意を表示してOKの時だけ動作する
    notification_layout = [
        [sg.Text("""""")],
        [sg.Button('OK'), sg.Exit()]]      
    notification_window = sg.Window('Window that stays open', notification_layout)      
    while True:                             # The Event Loop
        event, temp_values = notification_window.read() 
        if event == sg.WIN_CLOSED or event == 'Exit':
            break
        if event == "OK":
            # モデルとPysimpleguiのボックスの値を初期状態にする
            window,values,configuration_dict,err_text = Initialization(window,values,configuration_dict)
            window = window.refresh()
            event, values = window.read(10)
            break
    notification_window.close()
    # ポップアップ
    text_input = "Model updated."
    sg.popup('notification', text_input,keep_on_top=True)
    """
    return window,values,configuration_dict,err_text
# Extra setting Component
def Reset_extra_settings_class(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    class_name      = values["layout_left_1_Extra_Settings_class_name"]
    component_name  = values["layout_left_1_Extra_Settings_component_name"]
    attribute_name  = values["layout_left_1_Extra_Settings_atrribute"]
    if class_name not in window["layout_left_1_Extra_Settings_class_name"].Values:
        window,values,configuration_dict,err_text = Default_extra_setting_parameter_modify(window,values,configuration_dict)
        return window,values,configuration_dict,err_text
    else:
        pass
    # componentを初期化
    window["layout_left_1_Extra_Settings_component_name"].update(values=list(utility.classify_model_components(configuration_dict["model"])[values["layout_left_1_Extra_Settings_class_name"]]))
    window["layout_left_1_Extra_Settings_component_name"].update(value="")
    # attributeを初期化
    window["layout_left_1_Extra_Settings_atrribute"].update(values=[])
    window["layout_left_1_Extra_Settings_atrribute"].update(value="")
    # multilineを初期化
    window["layout_left_1_Extra_Settings_parameter_multiline"].update(value="")
    return window,values,configuration_dict,err_text

def Reset_extra_settings_component(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    class_name = values["layout_left_1_Extra_Settings_class_name"]
    component_name = values["layout_left_1_Extra_Settings_component_name"]
    attribute_name = values["layout_left_1_Extra_Settings_atrribute"]
    if class_name not in window["layout_left_1_Extra_Settings_class_name"].Values or component_name not in window["layout_left_1_Extra_Settings_component_name"].Values:
        window,values,configuration_dict,err_text = Default_extra_setting_parameter_modify(window,values,configuration_dict)
        return window,values,configuration_dict,err_text
    else:
        pass
    # attributeを初期化
    component_name  = values["layout_left_1_Extra_Settings_component_name"]
    component       = configuration_dict["model"].components.get(component_name)
    window["layout_left_1_Extra_Settings_atrribute"].update(values=list(utility.Get_component_attributes(configuration_dict["model"],component).keys()))
    window["layout_left_1_Extra_Settings_atrribute"].update(value="")
    # multilineを初期化
    window["layout_left_1_Extra_Settings_parameter_multiline"].update(value="")
    return window,values,configuration_dict,err_text

def Reset_extra_settings_attribute(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    class_name      = values["layout_left_1_Extra_Settings_class_name"]
    component_name  = values["layout_left_1_Extra_Settings_component_name"]
    attribute_name  = values["layout_left_1_Extra_Settings_atrribute"]
    if class_name not in window["layout_left_1_Extra_Settings_class_name"].Values or component_name not in window["layout_left_1_Extra_Settings_component_name"].Values or attribute_name not in window["layout_left_1_Extra_Settings_atrribute"].Values:
        window,values,configuration_dict,err_text = Default_extra_setting_parameter_modify(window,values,configuration_dict)
        return window,values,configuration_dict,err_text
    else:
        pass
    component_name = values["layout_left_1_Extra_Settings_component_name"]
    component = configuration_dict["model"].components.get(component_name)
    attribute_name = values["layout_left_1_Extra_Settings_atrribute"]
    window["layout_left_1_Extra_Settings_parameter_multiline"].update(value=utility.Get_component_attributes(configuration_dict["model"],component)[attribute_name])
    return window,values,configuration_dict,err_text

def Default_extra_setting_parameter_modify(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    # class
    window["layout_left_1_Extra_Settings_class_name"].update(value=window["layout_left_1_Extra_Settings_class_name"].Values[0])
    window = window.refresh()
    event, values = window.read(10)
    class_name = window["layout_left_1_Extra_Settings_class_name"].Values[0]
    
    # component
    window["layout_left_1_Extra_Settings_component_name"].update(values=list(utility.classify_model_components(configuration_dict["model"])[class_name]))
    window = window.refresh()
    event, values = window.read(10)
    window["layout_left_1_Extra_Settings_component_name"].update(value=window["layout_left_1_Extra_Settings_component_name"].Values[0])
    component_name = window["layout_left_1_Extra_Settings_component_name"].Values[0]
    component = configuration_dict["model"].components.get(component_name)
    window = window.refresh()
    event, values = window.read(10)

    # attribute
    window["layout_left_1_Extra_Settings_atrribute"].update(values=list(utility.Get_component_attributes(configuration_dict["model"],component).keys()))
    window = window.refresh()
    event, values = window.read(10)
    window["layout_left_1_Extra_Settings_atrribute"].update(value=window["layout_left_1_Extra_Settings_atrribute"].Values[0])
    window = window.refresh()
    event, values = window.read(10)
    attribute_name = window["layout_left_1_Extra_Settings_atrribute"].Values[0]

    # multilineを更新
    window["layout_left_1_Extra_Settings_parameter_multiline"].update(value=utility.Get_component_attributes(configuration_dict["model"],component)[attribute_name])
    window = window.refresh()
    event, values = window.read(10)

    return window,values,configuration_dict,err_text

# Beam parameter check
def bp_check_open_interactive_window(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        bp_model = configuration_dict["model"].deepcopy()
        # ボタンを押した時のエラーチェック
            # length、parameter、directionをそれぞれ調べていく
            # 1 lengthのエラーリスト
                # 1_1 値が空白 -> リストにない値が入ったらデフォルトの値を入れる
            # 2 parameterのエラーリスト
                # 2_1 値が空白
            # 3 directionのエラーリスト
                # 3_1 値が空白
            # 4 maxtemのエラー -> maxtem offの時は動作しない
            # 5 BSを差し込んで二つに分けたlengthの屈折率が、自動的に差し込まれるspaceコンポーネントの屈折率と異なる
        # 1_1
        length = values["bp_check_select_length"]
        if values["bp_check_select_length"] not in window["bp_check_select_length"].Values:
            text_input = f"""The name of the selected "{length}" is not in the list.\nUse the default value."""
            window["bp_check_select_length"].update(value=window["bp_check_select_length"].Values[0])
            length_name = window["bp_check_select_length"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            length_name = values['bp_check_select_length']
        # 2_1
        parameter = values["bp_check_select_parameter"]
        if parameter not in window["bp_check_select_parameter"].Values:
            text_input = f"""The name of the selected "{parameter}" is not in the list.\nUse the default value.
            """
            window["bp_check_select_parameter"].update(value=window["bp_check_select_parameter"].Values[0])
            parameter = window["bp_check_select_parameter"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            parameter = values['bp_check_select_parameter']
        # 3_1
        direction = values["bp_check_select_direction"]
        if values["bp_check_select_direction"] not in window["bp_check_select_direction"].Values:
            text_input = f"""The name of the selected "{direction}" is not in the list.\nUse the default value."""
            window["bp_check_select_direction"].update(value=window["bp_check_select_direction"].Values[0])
            direction = window["bp_check_select_direction"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            direction = values['bp_check_select_direction']
        # 4
        maxtem = bp_model.maxtem
        if maxtem == -1:#offは-1
            text_input = f"""If Maxtem is off, beam parameters cannot be examined. \nUse the default value (maxtem=1)."""
            bp_model.maxtem = 1
            sg.popup('Error', text_input,keep_on_top=True)
        # 動作
        component = bp_model.components[length_name]
        ports = My_simplified_optics.My_simplified_space(component).Get_pick_off_port()
        text = values["bp_check_added_pd_multiline"]
        added_text = f"bp pick_off_{length_name} {direction} {parameter} {ports[0]}"
        window["bp_check_added_pd_multiline"].update(value=added_text)
        bp_model = My_simplified_optics.My_simplified_space(component).Parse_pick_off_bs(parse_xaxis=True)
        bp_model.parse(added_text)
        #print(model)
        # Result informationの更新
        xaxis_lines = bp_model.xaxis.getFinesseText()
        #   #使ったxaxisの部分を追加
        text_input = "xaxis line\n------------------\n"+'\n'.join(xaxis_lines)+"\n\n"
        #   #使ったdetectorの部分を追加
        text_input += "detectors line\n------------------\n"
        detector_names = list(bp_model.detectors.keys())
        for detector_name in detector_names:
            pd_lines = bp_model.detectors[detector_name].getFinesseText()
            text_input+='\n'.join(pd_lines)
            text_input+="\n"
        text_input += "\n"
        #   #動かしたコンポーネントの部分を追加
        text_input += "related line\n------------------\n"
        bp_model_moto = configuration_dict["model"].deepcopy()
        component = bp_model_moto.components[length_name]
        length_network = My_simplified_optics.My_simplified_space(component).Connected_length_network()
        node1 = list(length_network.keys())[0]
        component1 = length_network[node1]
        node2 = list(length_network.keys())[1]
        component2 = length_network[node2]
        related_line = f"""The x-axis of the result is the length between {component1._Component__name} with {node1._Node__name} and {component2._Component__name} with {node2._Node__name}.\nThe starting point is {component1._Component__name}."""
        text_input += related_line
        #   bp_model確認用のmultilineの更新
        window["bp_check_bp_model_multiline"].update(value=bp_model)
        
        #   # 表示を更新
        window["bp_check_result_information"].update(value=text_input)

        # plot
        figure_title = values["bp_check_figure_title"]
        xlabel       = values["bp_check_xlabels"]
        ylabel       = values["bp_check_ylabels"]
        scale        = values["bp_check_plot_scale"]
        display_detector_names = list(bp_model.detectors.keys())
        out = bp_model.run()
        #out.plot()
        # matplotlib boxの更新
        fig = My_create_bp_figure_interactive(out,scale,figure_title,xlabel,ylabel,display_detector_names,component1._Component__name,component2._Component__name)
        plt.plot(block=False)
    except Exception as e:
        print(e)
        err_text = "たまにエラーが起こる\nおそらくAS_gouy_tunerなど両側にコンポーネントがついていなくて、かつ長さが0のコンポーネントを選択した"
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def bp_check_check_beam_parameter(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        bp_model = configuration_dict["model"].deepcopy()
        # ボタンを押した時のエラーチェック
            # length、parameter、directionをそれぞれ調べていく
            # 1 lengthのエラーリスト
                # 1_1 値が空白 -> リストにない値が入ったらデフォルトの値を入れる
            # 2 parameterのエラーリスト
                # 2_1 値が空白
            # 3 directionのエラーリスト
                # 3_1 値が空白
            # 4 maxtemのエラー -> maxtem offの時は動作しない
            # 5 BSを差し込んで二つに分けたlengthの屈折率が、自動的に差し込まれるspaceコンポーネントの屈折率と異なる

        # 1_1
        length = values["bp_check_select_length"]
        if values["bp_check_select_length"] not in window["bp_check_select_length"].Values:
            text_input = f"""The name of the selected "{length}" is not in the list.\nUse the default value."""
            window["bp_check_select_length"].update(value=window["bp_check_select_length"].Values[0])
            length_name = window["bp_check_select_length"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            length_name = values['bp_check_select_length']
        # 2_1
        parameter = values["bp_check_select_parameter"]
        if parameter not in window["bp_check_select_parameter"].Values:
            text_input = f"""The name of the selected "{parameter}" is not in the list.\nUse the default value.
            """
            window["bp_check_select_parameter"].update(value=window["bp_check_select_parameter"].Values[0])
            parameter = window["bp_check_select_parameter"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            parameter = values['bp_check_select_parameter']
        # 3_1
        direction = values["bp_check_select_direction"]
        if values["bp_check_select_direction"] not in window["bp_check_select_direction"].Values:
            text_input = f"""The name of the selected "{direction}" is not in the list.\nUse the default value."""
            window["bp_check_select_direction"].update(value=window["bp_check_select_direction"].Values[0])
            direction = window["bp_check_select_direction"].Values[0]
            sg.popup('Error', text_input,keep_on_top=True)
        else:
            direction = values['bp_check_select_direction']
        # 4
        maxtem = bp_model.maxtem
        if maxtem == -1:#offは-1
            text_input = f"""If Maxtem is off, beam parameters cannot be examined. \nUse the default value (maxtem=1)."""
            bp_model.maxtem = 1
            sg.popup('Error', text_input,keep_on_top=True)
        # 動作
        component = bp_model.components[length_name]
        ports = My_simplified_optics.My_simplified_space(component).Get_pick_off_port()
        text = values["bp_check_added_pd_multiline"]
        added_text = f"bp pick_off_{length_name} {direction} {parameter} {ports[0]}"
        window["bp_check_added_pd_multiline"].update(value=added_text)
        bp_model = My_simplified_optics.My_simplified_space(component).Parse_pick_off_bs(parse_xaxis=True)
        bp_model.parse(added_text)
        # Result informationの更新
        xaxis_lines = bp_model.xaxis.getFinesseText()
        #   #使ったxaxisの部分を追加
        text_input = "xaxis line\n------------------\n"+'\n'.join(xaxis_lines)+"\n\n"
        #   #使ったdetectorの部分を追加
        text_input += "detectors line\n------------------\n"
        detector_names = list(bp_model.detectors.keys())
        for detector_name in detector_names:
            pd_lines = bp_model.detectors[detector_name].getFinesseText()
            text_input+='\n'.join(pd_lines)
            text_input+="\n"
        text_input += "\n"
        #   #動かしたコンポーネントの部分を追加
        text_input += "related line\n------------------\n"
        bp_model_moto = configuration_dict["model"].deepcopy()
        component = bp_model_moto.components[length_name]
        length_network = My_simplified_optics.My_simplified_space(component).Connected_length_network()
        node1 = list(length_network.keys())[0]
        component1 = length_network[node1]
        node2 = list(length_network.keys())[1]
        component2 = length_network[node2]
        related_line = f"""The x-axis of the result is the length between {component1._Component__name} with {node1._Node__name} and {component2._Component__name} with {node2._Node__name}.\nThe starting point is {component1._Component__name}."""
        text_input += related_line
        #   bp_model確認用のmultilineの更新
        window["bp_check_bp_model_multiline"].update(value=bp_model)
        
        #   # 表示を更新
        window["bp_check_result_information"].update(value=text_input)

        # plot
        figure_title = values["bp_check_figure_title"]
        xlabel       = values["bp_check_xlabels"]
        ylabel       = values["bp_check_ylabels"]
        scale        = values["bp_check_plot_scale"]
        display_detector_names = list(bp_model.detectors.keys())
        out = bp_model.run()
        out.plot()
        # matplotlib boxの更新
        draw_figure(window['-IMAGE2-'], My_create_bp_figure(out,scale,figure_title,xlabel,ylabel,display_detector_names,component1._Component__name,component2._Component__name))
    except Exception as e:
        print(e)
        err_text = "たまにエラーが起こる\nおそらくAS_gouy_tunerなど両側にコンポーネントがついていなくて、かつ長さが0のコンポーネントを選択した"
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text
def bp_check_select_parameter(window,values,configuration_dict):
    err_text = ""
    parameter_name = values["bp_check_select_parameter"]
    window["bp_check_figure_title"].update(value=f"{parameter_name}")
    if parameter_name=="g":
        window["bp_check_ylabels"].update(value="gouy phase[deg]")
    else:
        window["bp_check_ylabels"].update(value="under devel")
    return window,values,configuration_dict,err_text
def layout_left_1_Extra_Settings_parameter_modify(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        # ボタンを押した時のエラーチェック
            # class_name、component_name、attribute_nameをそれぞれ調べていく
            # 1 class_nameのエラーリスト
                # 1_1 値が空白 -> リストにない値が入ったらデフォルトの値を入れる
            # 2 component_nameのエラーリスト
                # 2_1 値が空白 -> リストにない値が入ったらデフォルトの値を入れる
            # 3 attribute_nameのエラーリスト
                # 3_1 値が空白 -> リストにない値が入ったらデフォルトの値を入れる
                # 3_2 値がNone　-> Noneの時は何もしないでpopupをする
        # 1_1, 2_1, 3_1
        class_name = values["layout_left_1_Extra_Settings_class_name"]
        component_name = values["layout_left_1_Extra_Settings_component_name"]
        attribute_name = values["layout_left_1_Extra_Settings_atrribute"]
        if class_name not in window["layout_left_1_Extra_Settings_class_name"].Values or component_name not in window["layout_left_1_Extra_Settings_component_name"].Values or attribute_name not in window["layout_left_1_Extra_Settings_atrribute"].Values:
            # ポップアップ
            text_input = f"""The name of the selected "{class_name}""{component_name}""{attribute_name}" is not in the list.\nUse the default value."""
            sg.popup('Error', text_input,keep_on_top=True)
            # 値を更新
            window,values,configuration_dict,err_text = Default_extra_setting_parameter_modify(window,values,configuration_dict)
            class_name = values["layout_left_1_Extra_Settings_class_name"]
            component_name = values["layout_left_1_Extra_Settings_component_name"]
            attribute_name = values["layout_left_1_Extra_Settings_atrribute"]
        else:
            pass
        requested_value = values["layout_left_1_Extra_Settings_parameter_multiline"].replace("\n","")
        if requested_value=="None":
            text_input = """No action is taken because the value is None."""
            sg.popup('Error', text_input,keep_on_top=True)
            return window,values,configuration_dict,err_text
        # 動作
        My_class       = My_simplified_optics.Get_my_optics_class(class_name)
        My_component   = My_class(configuration_dict["model"].components[component_name])
        #configuration_dict["model"] = My_component.Change_parameter(attribute_name,int(float(requested_value)))
        configuration_dict["base"] = My_component.Change_parameter(attribute_name,int(float(requested_value)))
        # エラーログの表示用
        #configuration_dict["base"].generateKatScript()
        # モデルのボックスを更新
        window,values,configuration_dict,err_text = Initialization(window,values,configuration_dict)
    
    except Exception as e:
        print(e)
        err_text = "Parameters could not be updated."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def layout_left_1_Extra_Settings_maxtem_modify(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    # ボタンを押した時のエラーチェック
        # 1 class_nameのエラーリスト
            # 1_1 値がリストにない -> リストにない値が入ったらデフォルトの値を入れる
    maxtem = values["layout_left_1_Extra_Settings_maxtem"]
    if maxtem not in window["layout_left_1_Extra_Settings_maxtem"].Values:
        text_input = f"""The selected maxtem "{maxtem}" is not in the list.\nUse the default value."""
        sg.popup('Error', text_input,keep_on_top=True)
        configuration_dict["model"].maxtem = 1
    else:
        configuration_dict["model"].maxtem = maxtem
    # エラーログの表示用
    configuration_dict["base"].generateKatScript()
    # モデルのボックスを更新
    window["base_model_multiline"].update(value=configuration_dict["model"])
    return window,values,configuration_dict,err_text

def base_model_filter_categories(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        selected_name = values["base_model_filter_categories"]
        items = utility.Get_selected_class_components_list(selected_name,configuration_dict["model"])
        window["base_model_filter_items"].update(values=items)
    except Exception as e:
        print(e)
        err_text = "Model could not be loaded."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def base_model_filter_items(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        # ボタンを押した時のエラーチェック
            # 1 item_nameのエラーリスト
                # 1_1 値がリストにない -> リストにない値が入ったらデフォルトの値を入れる
        # 1_1
        if values["base_model_filter_items"] not in window["base_model_filter_items"].Values:
            name = list(configuration_dict["model"].components.keys())[0]
        else:
            name = values["base_model_filter_items"]

        if values["base_model_filter_categories"]=="commands":
            text = configuration_dict["model"].commands[name].getFinesseText()
        else:
            text = configuration_dict["model"].components[name].getFinesseText()
            text = '\n'.join(text)
        # currentry displayedを更新する
        category = values["base_model_filter_categories"]
        window["base_model_filter_current_displayed"].update(value=f"{category} {name}")
        # multilineを更新する

        script_text_in_list_format = configuration_dict["model"].generateKatScript()#改行付き
        X = text
        # おそらく改行がないのでむりやり作っている。改行がないので検索に引っ掛からなかった。
        X = X.splitlines()
        X = [e for e in X if e != "\n"]
        new_X = []
        for t in X:
            new_X.append(t+"\n")
        #動作
        X = new_X
        sorted_index_list = get_sorted_index(X,script_text_in_list_format)
        s = list_by_received_index_location_by_divided_list_returns(sorted_index_list,script_text_in_list_format)

        window["base_model_multiline"].update(value="")
        window = window.refresh()
        event, values = window.read(10)
        window["base_model_multiline"].reroute_stdout_to_here()
        window["base_model_multiline"].reroute_stderr_to_here()
        sg.cprint_set_output_destination(window, "base_model_multiline")
        for i in range(len(s)):
            if i % 2 == 0:
                for line in s[i]:
                    print(line,end = "")
            else:
                sg.cprint(f"{s[i][0]}",end = "", c='white on red')
        window["base_model_multiline"].restore_stdout()
        window["base_model_multiline"].restore_stderr()
        sg.cprint_set_output_destination(window, "layout_left_5_stdout_multiline")
        #window["base_model_multiline"].update(value=text)
        window = window.refresh()
        event, values = window.read(10)
    except Exception as e:
        print(e)
        err_text = "Model could not be loaded."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def base_model_filter_all(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    try:
        window["base_model_filter_current_displayed"].update(value="All kat script")
        kat_script = '\n'.join(configuration_dict["model"].generateKatScript())
        kat_script = utility.remove_header(kat_script)
        window["base_model_multiline"].update(value=kat_script)
    except Exception as e:
        print(e)
        err_text = "Model could not be loaded."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def base_model_filter_others(window,values,configuration_dict):
    try:
        err_text = ""# エラーが出たらこの中にテキストを入れる
        window["base_model_filter_current_displayed"].update(value="others")
        text = utility.remainings(configuration_dict["model"])
        kat_script = utility.remove_header(text)
        script_text_in_list_format = configuration_dict["model"].generateKatScript()#改行付き
        X = kat_script
        # おそらく改行がないのでむりやり作っている。改行がないので検索に引っ掛からなかった。
        X = X.splitlines()
        X = [e for e in X if e != "\n"]
        new_X = []
        for t in X:
            new_X.append(t+"\n")
        #動作
        X = new_X
        sorted_index_list = get_sorted_index(X,script_text_in_list_format)
        s = list_by_received_index_location_by_divided_list_returns(sorted_index_list,script_text_in_list_format)
        
        window["base_model_multiline"].update(value="")
        window = window.refresh()
        event, values = window.read(10)
        window["base_model_multiline"].reroute_stdout_to_here()
        window["base_model_multiline"].reroute_stderr_to_here()
        sg.cprint_set_output_destination(window, "base_model_multiline")
        for i in range(len(s)):
            if i % 2 == 0:
                for line in s[i]:
                    print(line,end = "")
            else:
                sg.cprint(f"{s[i][0]}",end = "", c='white on red')
        window["base_model_multiline"].restore_stdout()
        window["base_model_multiline"].restore_stderr()
        sg.cprint_set_output_destination(window, "layout_left_5_stdout_multiline")
        #window["base_model_multiline"].update(value=kat_script)
    except Exception as e:
        print(e)
        err_text = "Model could not be loaded."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    return window,values,configuration_dict,err_text

def base_setting_base_update(window,values,configuration_dict):
    err_text = ""# エラーが出たらこの中にテキストを入れる
    detector_name    = "KAGRA"#values["base_setting_base_detector"]
    model_name       = values["base_setting_base_model"]
    print(detector_name,model_name)
    if detector_name =="KAGRA" and model_name=="DRFPMI":
        new_base_text = Asc_base_model.Get_DRFPMI_base_model()
    elif detector_name =="KAGRA" and model_name=="PRMI":
        new_base_text = Asc_base_model.Get_PRMI_base_model()
    elif detector_name =="KAGRA" and model_name=="PRFPMI":
        new_base_text = Asc_base_model.Get_PRFPMI_base_model()
    elif detector_name =="KAGRA" and model_name=="FPMI":
        new_base_text = Asc_base_model.Get_FPMI_base_model()
    new_base = finesse.kat()
    new_base.parse(new_base_text)
    configuration_dict["base"]=new_base
    window,values,configuration_dict,err_text = Initialization(window,values,configuration_dict)
    text_input = f"""model updated."""
    sg.popup('', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_simulation_result_plot_run(window,values,configuration_dict):
    err_text=""
    try:
        out_history_key         = values["layout_right_simulation_result_out_history_name"]
        out                     = configuration_dict["out_dict"][out_history_key]
    except Exception as e:
        print(e)
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    display_detector_names  = window["layout_right_simulation_result_detectors_list"].GetListValues()
    figure_title            = values["layout_right_simulation_result_figure_title"]
    xlabel                  = values["layout_right_simulation_result_xlabels"]
    ylabel                  = values["layout_right_simulation_result_ylabels"]
    scale                   = values["layout_right_simulation_result_plot_scale"]

    #test = values["layout_right_simulation_result_select_pd_legend"]
    #test = values["layout_right_simulation_result_select_line_color"]
    #test = values["layout_right_simulation_result_select_line_style"]

    print(out)
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    if values["layout_right_simulation_result_select_plot_style"]=="abs+angle":
        axes_max_num = 2
        axes1_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : "log-log",
            "plot_style"    : "abs",
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes2_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : "log-linear",
            "plot_style"    : "angle",
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes_dict = {
            "axes1_dict" : axes1_dict,
            "axes2_dict" : axes2_dict,
        }
    else:
        axes_max_num = 1
        axes1_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : scale,
            "plot_style"    : values["layout_right_simulation_result_select_plot_style"],
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes_dict = {
            "axes1_dict" : axes1_dict,
        }

    plot_dict = {
        "figure_title"  : figure_title,
        "axes_dict"     : axes_dict,
        "axes_max_num"  : axes_max_num,
    }
    fig = out_figure(plot_dict,configuration_dict["model"],out,True)
    fig = fig.get_figure()
    draw_figure(window['-IMAGE3-'], fig)
    #draw_figure(window['-IMAGE3-'], My_create_figure2(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict))
    return window,values,configuration_dict,err_text

def layout_right_simulation_result_open_figure_window(window,values,configuration_dict):
    err_text = ""
    out_history_key         = values["layout_right_simulation_result_out_history_name"]
    out                     = configuration_dict["out_dict"][out_history_key]
    display_detector_names  = window["layout_right_simulation_result_detectors_list"].GetListValues()

    figure_title            = values["layout_right_simulation_result_figure_title"]
    xlabel                  = values["layout_right_simulation_result_xlabels"]
    ylabel                  = values["layout_right_simulation_result_ylabels"]
    scale                   = values["layout_right_simulation_result_plot_scale"]

    #test = values["layout_right_simulation_result_select_pd_legend"]
    #test = values["layout_right_simulation_result_select_line_color"]
    #test = values["layout_right_simulation_result_select_line_style"]

    print(out)
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    if values["layout_right_simulation_result_select_plot_style"]=="abs+angle":
        axes_max_num = 2
        axes1_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : "log-log",
            "plot_style"    : "abs",
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes2_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : "log-linear",
            "plot_style"    : "angle",
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes_dict = {
            "axes1_dict" : axes1_dict,
            "axes2_dict" : axes2_dict,
        }
    else:
        axes_max_num = 1
        axes1_dict = {
            "model"         : configuration_dict["model"],
            "out"           : out,
            "axes_title"    : "",
            "xlabel"        : xlabel,
            "ylabel"        : ylabel,
            "scale"         : scale,
            "plot_style"    : values["layout_right_simulation_result_select_plot_style"],
            "detectors_info": configuration_dict["detectors_info"]
        }
        axes_dict = {
            "axes1_dict" : axes1_dict,
        }

    plot_dict = {
        "figure_title"  : figure_title,
        "axes_dict"     : axes_dict,
        "axes_max_num"  : axes_max_num,
    }
    fig = out_figure(plot_dict,configuration_dict["model"],out,False)
    fig.get_figure()
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    #fig = My_create_figure3(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict)
    #plt.plot(block=False)
    return window,values,configuration_dict,err_text

def layout_right_simulation_result_clear_axes(window,values,configuration_dict):
    err_text    = ""
    configuration_dict["detectors_info"] = {}
    window["layout_right_simulation_result_detectors_list"].update(values=[])
    return window,values,configuration_dict,err_text
def layout_right_simulation_result_clear_figure(window,values,configuration_dict):
    err_text    = ""
    configuration_dict["detectors_info"] = {}
    window["layout_right_simulation_result_detectors_list"].update(values=[])
    return window,values,configuration_dict,err_text
def plot_setting_tab_reset(window,values,configuration_dict,plot_conf_dict_l):
    plot_conf_dict_l = {

    }
    window["layout_right_simulation_result_detectors_list"].update(values=[])
    configuration_dict["detectors_info"] = {}
    window["layout_right_simulation_result_select_detectors_combo"].update(values=[])
    window["layout_right_simulation_result_figure_title"].update(value="test_figure_title")
    #window["layout_right_simulation_result_axes_title"].update(value="test_axes_title")
    window["layout_right_simulation_result_xlabels"].update(value="test_xlabels")
    window["layout_right_simulation_result_ylabels"].update(value="test_ylabels")
    window["layout_right_simulation_result_plot_scale"].update(value="linear-linear")
    # titleなど-> あとで
    return window,values,configuration_dict,plot_conf_dict_l
def layout_right_simulation_result_select_detectors_add(window,values,configuration_dict):
    err_text    = ""
    # pdname,linecolir,linestyle
    pdname = values["layout_right_simulation_result_select_detectors_combo"]
    configuration_dict["plot_conf_dict"][f"{pdname}_line_color"] = values["layout_right_simulation_result_select_line_color"]
    configuration_dict["plot_conf_dict"][f"{pdname}_line_style"] = values["layout_right_simulation_result_select_line_style"]
    # detectors info
    configuration_dict["detectors_info"][pdname] = {"line_style" : values["layout_right_simulation_result_select_line_style"], "line_color" : values["layout_right_simulation_result_select_line_color"]}
    # pdnameを一覧で見られるようにlistに追加
    current_pdnames = window["layout_right_simulation_result_detectors_list"].Values
    current_pdnames.append(pdname)
    window["layout_right_simulation_result_detectors_list"].update(values=current_pdnames)

    return window,values,configuration_dict,err_text
def layout_right_simulation_result_all_detectors_add(window,values,configuration_dict):
    err_text    = ""
    # pdname,linecolir,linestyle
    pdnames = window["layout_right_simulation_result_select_detectors_combo"].Values
    for pdname in pdnames:
        configuration_dict["plot_conf_dict"][f"{pdname}_line_color"] = "default"
        configuration_dict["plot_conf_dict"][f"{pdname}_line_style"] = "default"
        # detectors info
        configuration_dict["detectors_info"][pdname] = {"line_style" : values["layout_right_simulation_result_select_line_style"], "line_color" : values["layout_right_simulation_result_select_line_color"]}
        # pdnameを一覧で見られるようにlistに追加
        current_pdnames = window["layout_right_simulation_result_detectors_list"].Values
        current_pdnames.append(pdname)
        window["layout_right_simulation_result_detectors_list"].update(values=current_pdnames)

    return window,values,configuration_dict,err_text
def layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=True):
    err_text=""
    try:
        out_history_key         = values["layout_right_simulation_result_out_history_name"]
        out                     = configuration_dict["out_dict"][out_history_key]
        if out=="default":
            err_text="Simulation results could not be fetched.\nPlease check the name."
            return window,values,configuration_dict,err_text
    except Exception as e:
        print(e)
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 一回tabを初期化する
    window,values,configuration_dict,configuration_dict["plot_conf_dict"] = plot_setting_tab_reset(window,values,configuration_dict,{})
    # pdnameのリストをpd選択用comboの選択肢としてセットする
    temp_model_script = configuration_dict["out_dict"][out_history_key].katScript
    temp_model = finesse.kat()
    temp_model.parse(temp_model_script)
    window["layout_right_simulation_result_select_detectors_combo"].update(values=list(temp_model.detectors.keys()))
    #   # pd選択用comboの値が空欄にならないようにlistの最初の値をセットする
    window = window.refresh()
    event, values = window.read(10)
    window["layout_right_simulation_result_select_detectors_combo"].update(value=list(temp_model.detectors.keys())[0])
    # リストが更新されたことを通知する
    if notification:
        text_input = "The simulation result was updated."
        sg.popup("Notification", text_input,keep_on_top=True)

    return window,values,configuration_dict,err_text
def plot_setting_default(out):
    #layout_right_simulation_result_xlabels
    #layout_right_simulation_result_ylabels
    pass

def layout_right_simulation_result_axes_title(window,values,configuration_dict):
    # ポップアップ
    err_text=""
    text_input = "現在つかえません"
    sg.popup('開発中', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text
def layout_right_simulation_result_axes_number(window,values,configuration_dict):
    # ポップアップ
    err_text=""
    text_input = "現在つかえません"
    sg.popup('開発中', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text



# %% [markdown]
# # DC power

# %%
# DC power
# pdを置くところ

def layout_right_dc_power_pd_setting_initialization(window,values,configuration_dict):
    """
    dc_powerのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    #   #DC power
    window["layout_right_dc_power_pd_setting_node"].update(values=kyotu)
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window["layout_right_dc_power_pd_setting_node"].update(value=kyotu[0])
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window["layout_right_dc_power_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにtemplateの名前をつける
    """
    err_text = ""
    try:
        qpd_flag    = values["layout_right_dc_power_use_qpd_check"]
        split_plane = values["layout_right_dc_power_use_qpd_plane_combo"]
        if values["layout_right_dc_power_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values["layout_right_dc_power_use_astarisk_check"]:
                node_name = window["layout_right_dc_power_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window["layout_right_dc_power_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window["layout_right_dc_power_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values["layout_right_dc_power_use_astarisk_check"]:
                node_name = values["layout_right_dc_power_pd_setting_node"]+"*"
            else:
                node_name = values["layout_right_dc_power_pd_setting_node"]
        #window["layout_right_dc_power_pd_setting_name"].update(values=[node_name])
        window = window.refresh()
        event, values = window.read(10)
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_dc_power_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_dc_power_pitch"
        else:
            pdname = f"{node_name}_dc_power"
        window["layout_right_dc_power_pd_setting_name"].update(values=[])
        window["layout_right_dc_power_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_dc_power_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text = ""
    old_text    = values["layout_right_dc_power_add_pd_source_script"]
    pdname      = values["layout_right_dc_power_pd_setting_name"]
    node        = values["layout_right_dc_power_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)

    old_text    = values["layout_right_dc_power_add_pd_source_script"]
    pdname      = values["layout_right_dc_power_pd_setting_name"]
    if values["layout_right_dc_power_use_astarisk_check"]==True:
        node    = values["layout_right_dc_power_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values["layout_right_dc_power_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        added_line  = f"pd {pdname} {node}"
    else:
        added_line  = old_text + f"pd {pdname} {node}"
    qpd_flag    = values["layout_right_dc_power_use_qpd_check"]
    split_plane = values["layout_right_dc_power_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    window["layout_right_dc_power_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_dc_power_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    window["layout_right_dc_power_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_dc_power_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_dc_power_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

# xaxis
def xaxis_seting_dc_mirror_combo_initialization(window,values,configuration_dict,header):
    """
    xaxisを設定する部分のGUIの初期化
    headerを受け取れば同じ動作ができる？
    """
    # add dofだけ初期化する
    err_text = ""
    components = []
    try:
        mirrors         = utility.classify_model_components(configuration_dict["model"])["mirror"]
        components      = components + mirrors
    except KeyError:
        print("Key error 'mirror' in 'xaxis_seting_dc_mirror_combo_initialization'")
    try:
        beam_splitters  = utility.classify_model_components(configuration_dict["model"])["beamSplitter"]
        components      = components + beam_splitters
    except KeyError:
        print("Key error 'beamSplitter' in 'xaxis_seting_dc_mirror_combo_initialization'")
    # comboのリストにmirrorとbeamsplitterを入れて更新する
    window[f"{header}_xaxis_setting_DC_mirror_select"].update(values=components)
    window = window.refresh()
    event, values = window.read(10)
    # デフォルトで表示される値をセットする
    window[f"{header}_xaxis_setting_DC_mirror_select"].update(value=components[0])
    # listboxをリセットする
    window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].update(values=["",""])
    window[f"{header}_xaxis_setting_DC_selected_put_list"].update(values=[])
    # source scriptをリセットする
    window[f"{header}_xaxis_setting_DC_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_dc_mirror_combo_initialization(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text

def xaxis_setting_dc_add_xaxis(window,values,configuration_dict,header):
    err_text    = ""
    mirror      = values[f"{header}_xaxis_setting_DC_mirror_select"]
    parameter   = values[f"{header}_xaxis_setting_DC_parameter_select"]
    put_astarisk= values[f"{header}_xaxis_setting_DC_put_astarisk_check"]
    put_command = "put"
    if put_astarisk:
        put_command = "put*"
    direction   = values[f"{header}_xaxis_setting_DC_direction_select"]
    direction_variables = {
        "+":"$x1",
        "-":"$mx1"
    }
    range_min   = values[f"{header}_xaxis_setting_DC_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_DC_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_DC_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = str(float(words[0])*words[1])
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = str(float(words[0])*words[1])
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = str(int(float(words[0])*words[1]))#これだけintじゃないとfinesseがエラーを出す
    # source scriptの作成
    source_script = values[f"{header}_xaxis_setting_DC_source_script"]
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    #   # xaxisを探す
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    flag,index = serch_word_in_source_script("xaxis",source_script)
    lines = source_script.splitlines()
    if flag:#xaxisがある
        # 既にあるので変更しない
        source_script = '\n'.join(lines)
        added_line = f"""{put_command} {mirror} {parameter} {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line
    else:#xaxisがない
        source_script=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\n\n"""
        added_line = f"""{put_command} {mirror} {parameter} {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line

    # 追加したxaxisとputをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].update(values=new_list)
    #   # put listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_DC_selected_put_list"].Values)
    new_list = current_list
    new_list.append(f"{mirror} {parameter} {direction}")
    if '' in new_list:
        new_list.remove('')
    window[f"{header}_xaxis_setting_DC_selected_put_list"].update(values=new_list)

    # 追加したxaxisをsource script multilineに表示する
    window[f"{header}_xaxis_setting_DC_source_script"].update(value=source_script)
    return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_setting_DC_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_dc_add_xaxis(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text

def layout_right_dc_power_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text
def layout_right_dc_power_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text
def layout_right_dc_power_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text
def layout_right_dc_power_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_dc_power_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def xaxis_setting_DC_save_as_template(window,values,configuration_dict,header):
    err_text = ""
    name        = values[f"{header}_xaxis_setting_DC_set_template_name"]
    xaxis_list  = window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].Values
    put_list    = window[f"{header}_xaxis_setting_DC_selected_put_list"].Values
    script      = values[f"{header}_xaxis_setting_DC_source_script"]
    # templateを選択するcomboにnameを追加する
    #   # 同じnameは追加しない(上書きする)
    combo_options = window[f"{header}_xaxis_setting_DC_template_select_name"].Values
    if name in combo_options:
        text_input = "Template updated."
        sg.popup('notification', text_input,keep_on_top=True)
    else:
        text_input = "A template was added."
        sg.popup('notification', text_input,keep_on_top=True)
        combo_options.append(name)
    #   # comboの更新
    window[f"{header}_xaxis_setting_DC_template_select_name"].update(values=combo_options)
    window[f"{header}_xaxis_setting_DC_template_select_name"].update(value=name)
    # source scriptの更新
    window[f"{header}_xaxis_setting_DC_template_source_script"].update(value=script)
    # listboxの更新
    window[f"{header}_xaxis_setting_DC_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_DC_template_put_list"].update(values=put_list)
    # 辞書に保存する
    configuration_dict["xaxis_template_dict"][f"{header}_{name}"]       = script
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_xaxis"] = xaxis_list
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_put"]   = put_list
    return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_setting_DC_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_save_as_template(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text

def xaxis_setting_DC_template_load(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_DC_template_select_name"]
    # key error
    try:
        saved_script = configuration_dict["xaxis_template_dict"][f"{header}_{key}"]
        xaxis_list   = configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"]
        put_list     = configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"]
    except Exception as e:
        print(e)
        err_text = "The KEY to loading simulation results is in error."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    window[f"{header}_xaxis_setting_DC_template_source_script"].update(value=saved_script)
    window[f"{header}_xaxis_setting_DC_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_DC_template_put_list"].update(values=put_list)
    return window,values,configuration_dict,err_text

def xaxis_setting_DC_template_clear(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_DC_template_select_name"]
    check_key = f"{header}_{key}"
    if check_key in configuration_dict["xaxis_template_dict"].keys():
        # 辞書から削除
        del configuration_dict["xaxis_template_dict"][f"{header}_{key}"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"] 
        # 表示の削除
        names_list = window[f"{header}_xaxis_setting_DC_template_select_name"].Values
        names_list.remove(key)
        window[f"{header}_xaxis_setting_DC_template_select_name"].update(values=names_list)
        window[f"{header}_xaxis_setting_DC_template_source_script"].update(value="")
        window[f"{header}_xaxis_setting_DC_template_xaxis_list"].update(values=["",""])
        window[f"{header}_xaxis_setting_DC_template_put_list"].update(values=[])
        return window,values,configuration_dict,err_text
    else:
        err_text=f'There is no template named "{key}".'
        return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_setting_DC_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_load(window,values,configuration_dict,"layout_right_dc_power")
    print(err_text)
    return window,values,configuration_dict,err_text
def layout_right_dc_power_xaxis_setting_DC_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_clear(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text

def xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict,header):
    err_text    = ""
    range_min   = values[f"{header}_xaxis_setting_DC_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_DC_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_DC_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = float(words[0])*words[1]
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = float(words[0])*words[1]
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = int(float(words[0])*words[1])#これだけintじゃないとfinesseがエラーを出す
    # source scriptの作成
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    source_script = values[f"{header}_xaxis_setting_DC_source_script"]
    #multilineのテキストを一行ずつにする
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)#1行ずつにしたsource scriptの何行目にxaxisの行があるのかを保存する
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    flag,index = serch_word_in_source_script("xaxis",source_script)
    lines = source_script.splitlines()
    if flag:#xaxisがある
        lines[index] = f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        source_script = '\n'.join(lines)
    else:
        added_line=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\n"""
        lines.insert(0, added_line)# xaxisの行は先頭に表示する
        source_script = '\n'.join(lines)

    # 追加したxaxisをsource script multilineに表示する
    window[f"{header}_xaxis_setting_DC_source_script"].update(value=source_script)

    # 新しいxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].update(values=new_list)

    return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text
    
def xaxis_setting_DC_source_script_clear(window,values,configuration_dict,header):
    err_text    = ""
    window[f"{header}_xaxis_setting_DC_selected_xaxis_list"].update(values=["",""])
    window[f"{header}_xaxis_setting_DC_selected_put_list"].update(values=[])
    window[f"{header}_xaxis_setting_DC_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_dc_power_xaxis_setting_DC_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_source_script_clear(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text

def DC_power_add_dof_run(window,values,configuration_dict,header):
    pd_text     = values[f"{header}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"DC_power",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    
    return window,values,configuration_dict,err_text

def layout_right_dc_power_add_dof_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = DC_power_add_dof_run(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text
    
def DC_power_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    pd_text     = values[f"{header}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_DC_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"DC_power",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_dc_power_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = DC_power_template_run(window,values,configuration_dict,"layout_right_dc_power")
    return window,values,configuration_dict,err_text



# %% [markdown]
# # amplitude

# %%

# amplitude
# pdを置くところ

def layout_right_amplitude_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window["layout_right_amplitude_pd_setting_node"].update(values=kyotu)
    # frequency
    frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window["layout_right_amplitude_pd_setting_frequency"].update(values=frequencies)
    # tem all 特に何もしない
    # tem_x
    maxtem = configuration_dict["model"].maxtem
    temlist = []
    temlist.append(0)
    for i in range(maxtem):
        temlist.append(i+1)
    window["layout_right_amplitude_pd_setting_tem_x"].update(values=temlist)
    window["layout_right_amplitude_pd_setting_tem_y"].update(values=temlist)
    window["layout_right_amplitude_pd_setting_tem_x"].update(value=temlist[0])#tem_x
    window["layout_right_amplitude_pd_setting_tem_y"].update(value=temlist[0])#tem_y
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window["layout_right_amplitude_pd_setting_node"].update(value=kyotu[0])#node
    window["layout_right_amplitude_pd_setting_frequency"].update(value=frequencies[0])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window["layout_right_amplitude_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    try:
        qpd_flag    = values["layout_right_amplitude_use_qpd_check"]
        split_plane = values["layout_right_amplitude_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values["layout_right_amplitude_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values["layout_right_amplitude_use_astarisk_check"]:
                node_name = window["layout_right_amplitude_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window["layout_right_amplitude_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window["layout_right_amplitude_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values["layout_right_amplitude_use_astarisk_check"]:
                node_name = values["layout_right_amplitude_pd_setting_node"]+"*"
            else:
                node_name = values["layout_right_amplitude_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        all_tem_flag    = values["layout_right_amplitude_use_all_tem_check"]
        tem_x           = values["layout_right_amplitude_pd_setting_tem_x"]
        tem_y           = values["layout_right_amplitude_pd_setting_tem_y"]
        frequency       = values["layout_right_amplitude_pd_setting_frequency"]
        
        if all_tem_flag:
            tem_xy = "all"
        else:
            tem_xy = f"{tem_x}{tem_y}"
        
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_amplitude_{tem_xy}_{frequency}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_amplitude_{tem_xy}_{frequency}_pitch"
        else:
                pdname = f"{node_name}_amplitude_{tem_xy}_{frequency}"
        # pdの名前ができたので表示する
        window["layout_right_amplitude_pd_setting_name"].update(values=[])
        window["layout_right_amplitude_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    old_text     = values["layout_right_amplitude_add_pd_source_script"]
    pdname       = values["layout_right_amplitude_pd_setting_name"]
    node         = values["layout_right_amplitude_pd_setting_node"]
    all_tem_flag = values["layout_right_amplitude_use_all_tem_check"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # tem_xとtem_yの合計がmaxtemを超えていないかチェックする
    if all_tem_flag:
        pass
    else:
        maxtem      = configuration_dict["model"].maxtem
        tem_x       = values["layout_right_amplitude_pd_setting_tem_x"]
        tem_y       = values["layout_right_amplitude_pd_setting_tem_y"]
        if maxtem <int(tem_x)+int(tem_y):
            err_text = "The sum of tem_x and tem_y must not be greater than maxtem."
            return window,values,configuration_dict,err_text
    # ノードに*入れるなら入れる
    old_text    = values["layout_right_amplitude_add_pd_source_script"]
    pdname      = values["layout_right_amplitude_pd_setting_name"]
    if values["layout_right_amplitude_use_astarisk_check"]==True:
        node    = values["layout_right_amplitude_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values["layout_right_amplitude_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        frequency   = values["layout_right_amplitude_pd_setting_frequency"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            if all_tem_flag:#temのチェック
                added_line  = f"ad {pdname} {num} {node}"
            else:
                added_line  = f"ad {pdname} {tem_x} {tem_y} {num} {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        frequency   = values["layout_right_amplitude_pd_setting_frequency"]
        words = utility.unit_convert(frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            if all_tem_flag:#temのチェック
                new_line  = f"ad {pdname} {num} {node}"
            else:
                new_line  = f"ad {pdname} {tem_x} {tem_y} {num} {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values["layout_right_amplitude_use_qpd_check"]
    split_plane = values["layout_right_amplitude_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    window["layout_right_amplitude_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_amplitude_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    window["layout_right_amplitude_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_use_all_tem_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_tem_x(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_pd_setting_tem_y(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_amplitude_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis

def layout_right_amplitude_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_dc_mirror_combo_initialization(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_xaxis_setting_DC_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_dc_add_xaxis(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_xaxis_setting_DC_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_save_as_template(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_xaxis_setting_DC_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_load(window,values,configuration_dict,"layout_right_amplitude")
    print(err_text)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_xaxis_setting_DC_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_clear(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def layout_right_amplitude_xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text
    
def layout_right_amplitude_xaxis_setting_DC_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_source_script_clear(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def amplitude_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    pd_text     = values[f"{header}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_DC_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"amplitude",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_amplitude_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = amplitude_template_run(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text

def amplitude_add_dof_run(window,values,configuration_dict,header):
    pd_text     = values[f"{header}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"amplitude",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    
    return window,values,configuration_dict,err_text

def layout_right_amplitude_add_dof_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = amplitude_add_dof_run(window,values,configuration_dict,"layout_right_amplitude")
    return window,values,configuration_dict,err_text
    

# %% [markdown]
# # WFS 1

# %%
# wfs1
# pdを置くところ

def layout_right_wfs1_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window["layout_right_wfs1_pd_setting_node"].update(values=kyotu)
    # frequency
    demod_frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window["layout_right_wfs1_pd_setting_demod_frequency"].update(values=demod_frequencies)
    # tem all 特に何もしない
    # tem_x
    demod_phases = ["0","-45","-90","-135","-180","45","90","135","180","max"]
    window["layout_right_wfs1_pd_setting_demod_phase"].update(values=demod_phases)
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window["layout_right_wfs1_pd_setting_node"].update(value=kyotu[0])#node
    window["layout_right_wfs1_pd_setting_demod_frequency"].update(value=demod_frequencies[0])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window["layout_right_wfs1_pd_setting_demod_phase"].update(value=demod_phases[0])
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window["layout_right_wfs1_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    try:
        qpd_flag    = values["layout_right_wfs1_use_qpd_check"]
        split_plane = values["layout_right_wfs1_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values["layout_right_wfs1_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values["layout_right_wfs1_use_astarisk_check"]:
                node_name = window["layout_right_wfs1_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window["layout_right_wfs1_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window["layout_right_wfs1_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values["layout_right_wfs1_use_astarisk_check"]:
                node_name = values["layout_right_wfs1_pd_setting_node"]+"*"
            else:
                node_name = values["layout_right_wfs1_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        demod_frequency = values["layout_right_wfs1_pd_setting_demod_frequency"]
        demod_phase     = values["layout_right_wfs1_pd_setting_demod_phase"]
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_wfs1_{demod_frequency}_{demod_phase}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_wfs1_{demod_frequency}_{demod_phase}_pitch"
        else:
                pdname = f"{node_name}_wfs1_{demod_frequency}_{demod_phase}"
        # pdの名前ができたので表示する
        window["layout_right_wfs1_pd_setting_name"].update(values=[])
        window["layout_right_wfs1_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    old_text     = values["layout_right_wfs1_add_pd_source_script"]
    pdname       = values["layout_right_wfs1_pd_setting_name"]
    node         = values["layout_right_wfs1_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # ノードに*入れるなら入れる
    old_text    = values["layout_right_wfs1_add_pd_source_script"]
    pdname      = values["layout_right_wfs1_pd_setting_name"]
    if values["layout_right_wfs1_use_astarisk_check"]==True:
        node    = values["layout_right_wfs1_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values["layout_right_wfs1_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        demod_frequency   = values["layout_right_wfs1_pd_setting_demod_frequency"]
        demod_phase       = values["layout_right_wfs1_pd_setting_demod_phase"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            added_line  = f"pd1 {pdname} {num} {demod_phase} {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        demod_frequency   = values["layout_right_wfs1_pd_setting_demod_frequency"]
        demod_phase       = values["layout_right_wfs1_pd_setting_demod_phase"]
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            new_line  = f"pd1 {pdname} {num} {demod_phase} {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values["layout_right_wfs1_use_qpd_check"]
    split_plane = values["layout_right_wfs1_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    window["layout_right_wfs1_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_wfs1_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    window["layout_right_wfs1_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_demod_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_pd_setting_demod_phase(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs1_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis

def layout_right_wfs1_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_dc_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_xaxis_setting_DC_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_dc_add_xaxis(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_xaxis_setting_DC_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_save_as_template(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_xaxis_setting_DC_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_load(window,values,configuration_dict,"layout_right_wfs1")
    print(err_text)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_xaxis_setting_DC_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_clear(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def layout_right_wfs1_xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text
    
def layout_right_wfs1_xaxis_setting_DC_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_source_script_clear(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def WFS1_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    pd_text     = values[f"{header}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_DC_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"demod_DC_sweep_signal",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs1_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = WFS1_template_run(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text

def WFS1_add_dof_run(window,values,configuration_dict,header):
    pd_text     = values[f"{header}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_DC_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"demod_DC_sweep_signal",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    
    return window,values,configuration_dict,err_text

def layout_right_wfs1_add_dof_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = WFS1_add_dof_run(window,values,configuration_dict,"layout_right_wfs1")
    return window,values,configuration_dict,err_text
    

# %% [markdown]
# # WFS2

# %%
# wfs2
# pdを置くところ

def layout_right_wfs2_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window["layout_right_wfs2_pd_setting_node"].update(values=kyotu)
    # frequency
    demod_frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window["layout_right_wfs2_pd_setting_demod_frequency"].update(values=demod_frequencies)
    # tem all 特に何もしない
    # tem_x
    demod_phases = ["0","-45","-90","-135","-180","45","90","135","180","max"]
    window["layout_right_wfs2_pd_setting_demod_phase"].update(values=demod_phases)
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window["layout_right_wfs2_pd_setting_node"].update(value=kyotu[0])#node
    window["layout_right_wfs2_pd_setting_demod_frequency"].update(value=demod_frequencies[0])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window["layout_right_wfs2_pd_setting_demod_phase"].update(value=demod_phases[0])
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window["layout_right_wfs2_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    try:
        qpd_flag    = values["layout_right_wfs2_use_qpd_check"]
        split_plane = values["layout_right_wfs2_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values["layout_right_wfs2_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values["layout_right_wfs2_use_astarisk_check"]:
                node_name = window["layout_right_wfs2_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window["layout_right_wfs2_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window["layout_right_wfs2_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values["layout_right_wfs2_use_astarisk_check"]:
                node_name = values["layout_right_wfs2_pd_setting_node"]+"*"
            else:
                node_name = values["layout_right_wfs2_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        demod_frequency = values["layout_right_wfs2_pd_setting_demod_frequency"]
        demod_phase     = values["layout_right_wfs2_pd_setting_demod_phase"]
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_wfs2_{demod_frequency}_{demod_phase}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_wfs2_{demod_frequency}_{demod_phase}_pitch"
        else:
                pdname = f"{node_name}_wfs2_{demod_frequency}_{demod_phase}"
        # pdの名前ができたので表示する
        window["layout_right_wfs2_pd_setting_name"].update(values=[])
        window["layout_right_wfs2_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    old_text     = values["layout_right_wfs2_add_pd_source_script"]
    pdname       = values["layout_right_wfs2_pd_setting_name"]
    node         = values["layout_right_wfs2_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # ノードに*入れるなら入れる
    old_text    = values["layout_right_wfs2_add_pd_source_script"]
    pdname      = values["layout_right_wfs2_pd_setting_name"]
    if values["layout_right_wfs2_use_astarisk_check"]==True:
        node    = values["layout_right_wfs2_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values["layout_right_wfs2_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        demod_frequency   = values["layout_right_wfs2_pd_setting_demod_frequency"]
        demod_phase       = values["layout_right_wfs2_pd_setting_demod_phase"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            added_line  = f"pd1 {pdname} {num} {demod_phase} {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        demod_frequency   = values["layout_right_wfs2_pd_setting_demod_frequency"]
        demod_phase       = values["layout_right_wfs2_pd_setting_demod_phase"]
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            new_line  = f"pd1 {pdname} {num} {demod_phase} {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values["layout_right_wfs2_use_qpd_check"]
    split_plane = values["layout_right_wfs2_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    window["layout_right_wfs2_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_wfs2_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    window["layout_right_wfs2_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_demod_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_pd_setting_demod_phase(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs2_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis

def layout_right_wfs2_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_dc_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_xaxis_setting_DC_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_dc_add_xaxis(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_xaxis_setting_DC_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_save_as_template(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_xaxis_setting_DC_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_load(window,values,configuration_dict,"layout_right_wfs2")
    print(err_text)
    return window,values,configuration_dict,err_text

def layout_right_wfs2_xaxis_setting_DC_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_template_clear(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_line_xaxis_set(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text
    
def layout_right_wfs2_xaxis_setting_DC_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_DC_source_script_clear(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = template_run(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text

def layout_right_wfs2_add_dof_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = add_dof_run(window,values,configuration_dict,"layout_right_wfs2")
    return window,values,configuration_dict,err_text
    

# %% [markdown]
# # WFS 3

# %%
# wfs3 gouy phase optimization
# pdを置くところ

def layout_right_wfs3_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    tab_category = "gouy_tuner"
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window[f"layout_right_wfs3_{tab_category}_pd_setting_node"].update(values=kyotu)
    # demod frequency
    demod_frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"].update(values=demod_frequencies)
    window[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])
    # demod phase
    demod_phases = ["-180","-135","-90","-45","0","45","90","135","180","max"]
    window[f"layout_right_wfs3_{tab_category}_pd_setting_demod_phase"].update(values=demod_phases)
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window[f"layout_right_wfs3_{tab_category}_pd_setting_node"].update(value=kyotu[0])#node
    window[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window[f"layout_right_wfs3_{tab_category}_pd_setting_demod_phase"].update(value="0")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window[f"layout_right_wfs3_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    tab_category = "gouy_tuner"
    try:
        qpd_flag    = values[f"layout_right_wfs3_{tab_category}_use_qpd_check"]
        split_plane = values[f"layout_right_wfs3_{tab_category}_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values[f"layout_right_wfs3_{tab_category}_use_astarisk_check"]:
                node_name = window[f"layout_right_wfs3_{tab_category}_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window[f"layout_right_wfs3_{tab_category}_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window[f"layout_right_wfs3_{tab_category}_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values[f"layout_right_wfs3_{tab_category}_use_astarisk_check"]:
                node_name = values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]+"*"
            else:
                node_name = values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        demod_frequency = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"]
        demod_phase     = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_phase"]
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_wfs3_{tab_category}_{demod_frequency}_{demod_phase}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_wfs3_{tab_category}_{demod_frequency}_{demod_phase}_pitch"
        else:
                pdname = f"{node_name}_wfs3_{tab_category}_{demod_frequency}_{demod_phase}"
        # pdの名前ができたので表示する
        window[f"layout_right_wfs3_{tab_category}_pd_setting_name"].update(values=[])
        window[f"layout_right_wfs3_{tab_category}_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    tab_category = "gouy_tuner"
    old_text     = values[f"layout_right_wfs3_{tab_category}_add_pd_source_script"]
    pdname       = values[f"layout_right_wfs3_{tab_category}_pd_setting_name"]
    node         = values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # ノードに*入れるなら入れる
    old_text    = values[f"layout_right_wfs3_{tab_category}_add_pd_source_script"]
    pdname      = values[f"layout_right_wfs3_{tab_category}_pd_setting_name"]
    if values[f"layout_right_wfs3_{tab_category}_use_astarisk_check"]==True:
        node    = values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values[f"layout_right_wfs3_{tab_category}_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        demod_frequency   = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_phase"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            added_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        demod_frequency   = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs3_{tab_category}_pd_setting_demod_phase"]
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            new_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values[f"layout_right_wfs3_{tab_category}_use_qpd_check"]
    split_plane = values[f"layout_right_wfs3_{tab_category}_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    window[f"layout_right_wfs3_{tab_category}_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_wfs3_gouy_tuner_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    tab_category = "gouy_tuner"
    window[f"layout_right_wfs3_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_demod_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_pd_setting_demod_phase(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis
#
def xaxis_seting_gouy_tuner_mirror_combo_initialization(window,values,configuration_dict,header):
    """
    xaxisを設定する部分のGUIの初期化
    headerを受け取れば同じ動作ができる？
    """
    err_text = ""
    # xaxisのrangeを初期化する
    range_list = ["-180","-90","0","90","180"]
    window[f"{header}_xaxis_setting_gouy_tuner_range_min_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_gouy_tuner_range_min_select"].update(value="-180")
    window[f"{header}_xaxis_setting_gouy_tuner_range_max_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_gouy_tuner_range_max_select"].update(value="180")
    window[f"{header}_xaxis_setting_gouy_tuner_sampling_num_select"].update(values=["1000"])
    window[f"{header}_xaxis_setting_gouy_tuner_sampling_num_select"].update(value="1000")
    # comboのリストにmirrorとbeamsplitterを入れて更新する
    components = []
    try:
        mirrors         = utility.classify_model_components(configuration_dict["model"])["mirror"]
        components      = components + mirrors
    except KeyError:
        print("Key error 'mirror' in 'xaxis_seting_dc_mirror_combo_initialization'")
    try:
        beam_splitters  = utility.classify_model_components(configuration_dict["model"])["beamSplitter"]
        components      = components + beam_splitters
    except KeyError:
        print("Key error 'beamSplitter' in 'xaxis_seting_dc_mirror_combo_initialization'")
    window[f"{header}_xaxis_setting_gouy_tuner_mirror_select"].update(values=components)
    window = window.refresh()
    event, values = window.read(10)
    # comboのリストにtuningするノードのspaceを入れて更新する
    genzai_available    = ["REFL","AS","TMSX","TMSY","POP","POP2"]
    tuning_node         = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kyoutuu             = list(set(genzai_available) & set(tuning_node))
    window[f"{header}_xaxis_setting_gouy_tuner_tuning_node_select"].update(values=kyoutuu)
    if kyoutuu!=[]:
        window[f"{header}_xaxis_setting_gouy_tuner_tuning_node_select"].update(value=kyoutuu[0])
    # デフォルトで表示されるmirrorをセットする
    window[f"{header}_xaxis_setting_gouy_tuner_mirror_select"].update(value=components[0])
    # listboxをリセットする
    window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].update(values=["","","",""])
    window[f"{header}_xaxis_setting_gouy_tuner_selected_put_list"].update(values=[])
    # source scriptをリセットする
    window[f"{header}_xaxis_setting_gouy_tuner_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_gouy_tuner_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_add_xaxis(window,values,configuration_dict,header):
    err_text    = ""
    mirror      = values[f"{header}_xaxis_setting_gouy_tuner_mirror_select"]
    parameter   = values[f"{header}_xaxis_setting_gouy_tuner_parameter_select"]
    #put_astarisk= values[f"{header}_xaxis_setting_gouy_tuner_put_astarisk_check"]
    put_command = "put"
    #if put_astarisk:
    #    put_command = "put*"
    direction   = values[f"{header}_xaxis_setting_gouy_tuner_direction_select"]
    direction_variables = {
        "+":"0",
        "-":"180"
    }
    range_min   = values[f"{header}_xaxis_setting_gouy_tuner_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_gouy_tuner_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_gouy_tuner_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = str(float(words[0])*words[1])
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = str(float(words[0])*words[1])
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = str(int(float(words[0])*words[1]))#これだけintじゃないとfinesseがエラーを出す
    # 動作 source scriptに追加する文章の作成
    source_script = values[f"{header}_xaxis_setting_gouy_tuner_source_script"]
    tuning_space        = f'{values[f"{header}_xaxis_setting_gouy_tuner_tuning_node_select"]}_gouy_tuner'
    tuning_space_list  = utility.classify_model_components(configuration_dict["model"])["space"]
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    #   # xaxisを探す
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    flag,index = serch_word_in_source_script("xaxis",source_script)
    lines = source_script.splitlines()
    if flag:#xaxisがある
        # 既にあるので変更しない
        source_script = '\n'.join(lines)
        #added_line = f"""{put_command} {mirror} {parameter} {direction_variables[direction]}\n"""
        added_line = f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line
    else:#xaxisがない
        # xaxis追加
        if tuning_space not in tuning_space_list:
            err_text = "Currently only REFL, AS, TSMX, TSMY, POP, and POP2 can be selected."
            return window,values,configuration_dict,err_text
        source_script=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\nput {tuning_space} gx $x1\nput {tuning_space} gy $x1\n\n"""
        # ミラー追加
        added_line = f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line

    # 追加したxaxisとputをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        new_list[2]=f"put {tuning_space} gx $x1"
        new_list[3]=f"put {tuning_space} gy $x1"
        
    window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].update(values=new_list)
    #   # put listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_gouy_tuner_selected_put_list"].Values)
    new_list = current_list
    new_list.append(f"{mirror} {parameter} {direction}")
    if '' in new_list:
        new_list.remove('')
    window[f"{header}_xaxis_setting_gouy_tuner_selected_put_list"].update(values=new_list)

    # 追加したxaxisをsource script multilineに表示する
    window[f"{header}_xaxis_setting_gouy_tuner_source_script"].update(value=source_script)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_add_xaxis(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_save_as_template(window,values,configuration_dict,header):
    err_text = ""
    name        = values[f"{header}_xaxis_setting_gouy_tuner_set_template_name"]
    xaxis_list  = window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].Values
    put_list    = window[f"{header}_xaxis_setting_gouy_tuner_selected_put_list"].Values
    script      = values[f"{header}_xaxis_setting_gouy_tuner_source_script"]
    # templateを選択するcomboにnameを追加する
    #   # 同じnameは追加しない(上書きする)
    combo_options = window[f"{header}_xaxis_setting_gouy_tuner_template_select_name"].Values
    if name in combo_options:
        text_input = "Template updated."
        sg.popup('notification', text_input,keep_on_top=True)
    else:
        text_input = "A template was added."
        sg.popup('notification', text_input,keep_on_top=True)
        combo_options.append(name)
    #   # comboの更新
    window[f"{header}_xaxis_setting_gouy_tuner_template_select_name"].update(values=combo_options)
    window[f"{header}_xaxis_setting_gouy_tuner_template_select_name"].update(value=name)
    # source scriptの更新
    window[f"{header}_xaxis_setting_gouy_tuner_template_source_script"].update(value=script)
    # listboxの更新
    window[f"{header}_xaxis_setting_gouy_tuner_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_gouy_tuner_template_put_list"].update(values=put_list)
    # 辞書に保存する
    configuration_dict["xaxis_template_dict"][f"{header}_{name}"]       = script
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_xaxis"] = xaxis_list
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_put"]   = put_list
    #テスト
    print(configuration_dict["xaxis_template_dict"].keys())
    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_save_as_template(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_template_load(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_gouy_tuner_template_select_name"]
    # key error
    try:
        saved_script = configuration_dict["xaxis_template_dict"][f"{header}_{key}"]
        xaxis_list   = configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"]
        put_list     = configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"]
    except Exception as e:
        print(e)
        err_text = "The KEY to loading simulation results is in error."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    window[f"{header}_xaxis_setting_gouy_tuner_template_source_script"].update(value=saved_script)
    window[f"{header}_xaxis_setting_gouy_tuner_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_gouy_tuner_template_put_list"].update(values=put_list)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_template_load(window,values,configuration_dict,"layout_right_wfs3")
    print(err_text)
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_template_clear(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_gouy_tuner_template_select_name"]
    check_key = f"{header}_{key}"
    if check_key in configuration_dict["xaxis_template_dict"].keys():
        # 辞書から削除
        del configuration_dict["xaxis_template_dict"][f"{header}_{key}"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"] 
        # 表示の削除
        names_list = window[f"{header}_xaxis_setting_gouy_tuner_template_select_name"].Values
        names_list.remove(key)
        window[f"{header}_xaxis_setting_gouy_tuner_template_select_name"].update(values=names_list)
        window[f"{header}_xaxis_setting_gouy_tuner_template_source_script"].update(value="")
        window[f"{header}_xaxis_setting_gouy_tuner_template_xaxis_list"].update(values=["","","",""])
        window[f"{header}_xaxis_setting_gouy_tuner_template_put_list"].update(values=[])
        return window,values,configuration_dict,err_text
    else:
        err_text=f'There is no template named "{key}".'
        return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_template_clear(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_line_xaxis_set(window,values,configuration_dict,header):
    err_text    = ""
    range_min   = values[f"{header}_xaxis_setting_gouy_tuner_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_gouy_tuner_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_gouy_tuner_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = float(words[0])*words[1]
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = float(words[0])*words[1]
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = int(float(words[0])*words[1])#これだけintじゃないとfinesseがエラーを出す
    # source scriptの作成
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    source_script = values[f"{header}_xaxis_setting_gouy_tuner_source_script"]
    #multilineのテキストを一行ずつにする
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)#1行ずつにしたsource scriptの何行目にxaxisの行があるのかを保存する
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    xaxis_lines = list(window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].Values)
    test_str = ""
    if xaxis_lines != ["","","",""]:
        test_str = '\n'.join(xaxis_lines)
    source_script = source_script.replace(test_str,"")
    lines = source_script.splitlines()
    flag,index = serch_word_in_source_script("xaxis",source_script)

    tuning_space        = f'{values[f"{header}_xaxis_setting_gouy_tuner_tuning_node_select"]}_gouy_tuner'
    tuning_space_list  = utility.classify_model_components(configuration_dict["model"])["space"]
    if flag:#xaxisがある
        lines[index] = f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        source_script = '\n'.join(lines)
    else:
        #added_line=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\n"""
        if tuning_space not in tuning_space_list:
            err_text = "Currently only REFL, AS, TSMX, TSMY, POP, and POP2 can be selected."
            return window,values,configuration_dict,err_text
        added_line=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\nput {tuning_space} gx $x1\nput {tuning_space} gy $x1\n\n"""
        lines.insert(0, added_line)# xaxisの行は先頭に表示する
        source_script = '\n'.join(lines)

    # 追加したxaxisをsource script multilineに表示する
    source_script = source_script.replace("\n\n\n","")
    window[f"{header}_xaxis_setting_gouy_tuner_source_script"].update(value=source_script)

    # 新しいxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        new_list[2]=f"put {tuning_space} gx $x1"
        new_list[3]=f"put {tuning_space} gy $x1"
    window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].update(values=new_list)

    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_line_xaxis_set(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_gouy_tuner_source_script_clear(window,values,configuration_dict,header):
    err_text    = ""
    #window,values,configuration_dict,err_text = xaxis_seting_gouy_tuner_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs3")
    window[f"{header}_xaxis_setting_gouy_tuner_selected_xaxis_list"].update(values=["","","",""])
    window[f"{header}_xaxis_setting_gouy_tuner_selected_put_list"].update(values=[])
    window[f"{header}_xaxis_setting_gouy_tuner_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs3_xaxis_setting_gouy_tuner_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_gouy_tuner_source_script_clear(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_simulation_result_plot_run(window,values,configuration_dict):
    err_text = ""
    out_history_key         = values["layout_right_simulation_result_out_history_name"]
    out                     = configuration_dict["out_dict"][out_history_key]
    display_detector_names  = window["layout_right_simulation_result_detectors_list"].GetListValues()

    figure_title            = "gouy phase DC sweep" #values["layout_right_simulation_result_figure_title"]
    xlabel                  = "gouy phase[deg]"     #values["layout_right_simulation_result_xlabels"]
    ylabel                  = "wfs signal[shiranai]"#values["layout_right_simulation_result_ylabels"]
    scale                   = "linear-linear"       #values["layout_right_simulation_result_plot_scale"]

    #test = values["layout_right_simulation_result_select_pd_legend"]
    #test = values["layout_right_simulation_result_select_line_color"]
    #test = values["layout_right_simulation_result_select_line_style"]

    print(out)
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    fig = My_create_figure4(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict,display_max=True)
    plt.plot(block=False)
    return window,values,configuration_dict,err_text

def gouy_tuner_run(window,values,configuration_dict,header):
    err_text = ""
    tab_category = "gouy_tuner"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_gouy_tuner_source_script"]
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_gouy_tuner_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"WFS3",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_simulation_result_plot_run(window,values,configuration_dict)
    # 最大になるgouy phaseをボックスに表示する
    #max_index = out

    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = gouy_tuner_run(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text

def gouy_tuner_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    tab_category = "gouy_tuner"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_{tab_category}_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"WFS3",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs3_gouy_tuner_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs3_gouy_tuner_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = gouy_tuner_template_run(window,values,configuration_dict,"layout_right_wfs3")
    return window,values,configuration_dict,err_text


# %% [markdown]
# # WFS 4

# %%
# wfs4 godemoduy phase optimization
# pdを置くところ

def layout_right_wfs4_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    tab_category = "demod_phase_optimization"
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window[f"layout_right_wfs4_{tab_category}_pd_setting_node"].update(values=kyotu)
    # demod frequency
    demod_frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"].update(values=demod_frequencies)
    window[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])
    # demod phase
    demod_phases = ["-180","-135","-90","-45","0","45","90","135","180","all"]
    window[f"layout_right_wfs4_{tab_category}_pd_setting_demod_phase"].update(values=demod_phases)
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window[f"layout_right_wfs4_{tab_category}_pd_setting_node"].update(value=kyotu[0])#node
    window[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window[f"layout_right_wfs4_{tab_category}_pd_setting_demod_phase"].update(value="0")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window[f"layout_right_wfs4_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    tab_category = "demod_phase_optimization"
    try:
        qpd_flag    = values[f"layout_right_wfs4_{tab_category}_use_qpd_check"]
        split_plane = values[f"layout_right_wfs4_{tab_category}_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values[f"layout_right_wfs4_{tab_category}_use_astarisk_check"]:
                node_name = window[f"layout_right_wfs4_{tab_category}_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window[f"layout_right_wfs4_{tab_category}_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window[f"layout_right_wfs4_{tab_category}_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values[f"layout_right_wfs4_{tab_category}_use_astarisk_check"]:
                node_name = values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]+"*"
            else:
                node_name = values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        demod_frequency = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"]
        demod_phase     = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_phase"]
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_wfs4_{tab_category}_{demod_frequency}_{demod_phase}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_wfs4_{tab_category}_{demod_frequency}_{demod_phase}_pitch"
        else:
                pdname = f"{node_name}_wfs4_{tab_category}_{demod_frequency}_{demod_phase}"
        # pdの名前ができたので表示する
        window[f"layout_right_wfs4_{tab_category}_pd_setting_name"].update(values=[])
        window[f"layout_right_wfs4_{tab_category}_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    tab_category = "demod_phase_optimization"
    old_text     = values[f"layout_right_wfs4_{tab_category}_add_pd_source_script"]
    pdname       = values[f"layout_right_wfs4_{tab_category}_pd_setting_name"]
    node         = values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # ノードに*入れるなら入れる
    old_text    = values[f"layout_right_wfs4_{tab_category}_add_pd_source_script"]
    pdname      = values[f"layout_right_wfs4_{tab_category}_pd_setting_name"]
    if values[f"layout_right_wfs4_{tab_category}_use_astarisk_check"]==True:
        node    = values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values[f"layout_right_wfs4_{tab_category}_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        demod_frequency   = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_phase"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            added_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        demod_frequency   = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs4_{tab_category}_pd_setting_demod_phase"]
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            new_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values[f"layout_right_wfs4_{tab_category}_use_qpd_check"]
    split_plane = values[f"layout_right_wfs4_{tab_category}_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    # phaseをスイープさせるところ　ひとまずここに入れておく
    added_line = added_line +"\n"+f"put {pdname} phase1 $x1"
    # 表示を更新する
    window[f"layout_right_wfs4_{tab_category}_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_wfs4_demod_phase_optimization_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    tab_category = "demod_phase_optimization"
    window[f"layout_right_wfs4_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_demod_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_pd_setting_demod_phase(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis
#
def xaxis_seting_demod_phase_optimization_mirror_combo_initialization(window,values,configuration_dict,header):
    """
    xaxisを設定する部分のGUIの初期化
    headerを受け取れば同じ動作ができる？
    """
    err_text = ""
    # xaxisのrangeを初期化する
    range_list = ["-180","-90","0","90","180"]
    window[f"{header}_xaxis_setting_demod_phase_optimization_range_min_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_demod_phase_optimization_range_min_select"].update(value="-180")
    window[f"{header}_xaxis_setting_demod_phase_optimization_range_max_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_demod_phase_optimization_range_max_select"].update(value="180")
    window[f"{header}_xaxis_setting_demod_phase_optimization_sampling_num_select"].update(values=["1000"])
    window[f"{header}_xaxis_setting_demod_phase_optimization_sampling_num_select"].update(value="1000")
    # comboのリストにmirrorとbeamsplitterを入れて更新する
    components = []
    try:
        mirrors         = utility.classify_model_components(configuration_dict["model"])["mirror"]
        components      = components + mirrors
    except KeyError:
        print("Key error 'mirror' in 'xaxis_seting_dc_mirror_combo_initialization'")
    try:
        beam_splitters  = utility.classify_model_components(configuration_dict["model"])["beamSplitter"]
        components      = components + beam_splitters
    except KeyError:
        print("Key error 'beamSplitter' in 'xaxis_seting_dc_mirror_combo_initialization'")
    window[f"{header}_xaxis_setting_demod_phase_optimization_mirror_select"].update(values=components)
    window = window.refresh()
    event, values = window.read(10)
    # デフォルトで表示されるmirrorをセットする
    window[f"{header}_xaxis_setting_demod_phase_optimization_mirror_select"].update(value=components[0])
    # listboxをリセットする
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].update(values=["","","",""])
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_put_list"].update(values=[])
    # source scriptをリセットする
    window[f"{header}_xaxis_setting_demod_phase_optimization_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_demod_phase_optimization_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_add_xaxis(window,values,configuration_dict,header):
    err_text    = ""
    mirror      = values[f"{header}_xaxis_setting_demod_phase_optimization_mirror_select"]
    parameter   = values[f"{header}_xaxis_setting_demod_phase_optimization_parameter_select"]
    #put_astarisk= values[f"{header}_xaxis_setting_demod_phase_optimization_put_astarisk_check"]
    put_command = "put"
    #if put_astarisk:
    #    put_command = "put*"
    direction   = values[f"{header}_xaxis_setting_demod_phase_optimization_direction_select"]
    direction_variables = {
        "+":"0",
        "-":"180"
    }
    range_min   = values[f"{header}_xaxis_setting_demod_phase_optimization_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_demod_phase_optimization_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_demod_phase_optimization_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = str(float(words[0])*words[1])
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = str(float(words[0])*words[1])
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = str(int(float(words[0])*words[1]))#これだけintじゃないとfinesseがエラーを出す
    # 動作 source scriptに追加する文章の作成
    source_script = values[f"{header}_xaxis_setting_demod_phase_optimization_source_script"]
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    #   # xaxisを探す
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    flag,index = serch_word_in_source_script("xaxis",source_script)
    lines = source_script.splitlines()
    if flag:#xaxisがある
        # 既にあるので変更しない
        source_script = '\n'.join(lines)
        #added_line = f"""{put_command} {mirror} {parameter} {direction_variables[direction]}\n"""
        added_line = f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line
    else:#xaxisがない
        # xaxis追加
        source_script=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\n\n"""
        # ミラー追加
        added_line = f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line

    # 追加したxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].update(values=new_list)
    #   # put listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_demod_phase_optimization_selected_put_list"].Values)
    new_list = current_list
    new_list.append(f"{mirror} {parameter} {direction}")
    if '' in new_list:
        new_list.remove('')
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_put_list"].update(values=new_list)

    # 追加したxaxisをsource script multilineに表示する
    window[f"{header}_xaxis_setting_demod_phase_optimization_source_script"].update(value=source_script)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_add_xaxis(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_save_as_template(window,values,configuration_dict,header):
    err_text = ""
    name        = values[f"{header}_xaxis_setting_demod_phase_optimization_set_template_name"]
    xaxis_list  = window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].Values
    put_list    = window[f"{header}_xaxis_setting_demod_phase_optimization_selected_put_list"].Values
    script      = values[f"{header}_xaxis_setting_demod_phase_optimization_source_script"]
    # templateを選択するcomboにnameを追加する
    #   # 同じnameは追加しない(上書きする)
    combo_options = window[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"].Values
    if name in combo_options:
        text_input = "Template updated."
        sg.popup('notification', text_input,keep_on_top=True)
    else:
        text_input = "A template was added."
        sg.popup('notification', text_input,keep_on_top=True)
        combo_options.append(name)
    #   # comboの更新
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"].update(values=combo_options)
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"].update(value=name)
    # source scriptの更新
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_source_script"].update(value=script)
    # listboxの更新
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_put_list"].update(values=put_list)
    # 辞書に保存する
    configuration_dict["xaxis_template_dict"][f"{header}_{name}"]       = script
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_xaxis"] = xaxis_list
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_put"]   = put_list
    #テスト
    print(configuration_dict["xaxis_template_dict"].keys())
    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_save_as_template(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_template_load(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"]
    # key error
    try:
        saved_script = configuration_dict["xaxis_template_dict"][f"{header}_{key}"]
        xaxis_list   = configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"]
        put_list     = configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"]
    except Exception as e:
        print(e)
        err_text = "The KEY to loading simulation results is in error."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_source_script"].update(value=saved_script)
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_demod_phase_optimization_template_put_list"].update(values=put_list)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_template_load(window,values,configuration_dict,"layout_right_wfs4")
    print(err_text)
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_template_clear(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"]
    check_key = f"{header}_{key}"
    if check_key in configuration_dict["xaxis_template_dict"].keys():
        # 辞書から削除
        del configuration_dict["xaxis_template_dict"][f"{header}_{key}"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"] 
        # 表示の削除
        names_list = window[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"].Values
        names_list.remove(key)
        window[f"{header}_xaxis_setting_demod_phase_optimization_template_select_name"].update(values=names_list)
        window[f"{header}_xaxis_setting_demod_phase_optimization_template_source_script"].update(value="")
        window[f"{header}_xaxis_setting_demod_phase_optimization_template_xaxis_list"].update(values=["",""])
        window[f"{header}_xaxis_setting_demod_phase_optimization_template_put_list"].update(values=[])
        return window,values,configuration_dict,err_text
    else:
        err_text=f'There is no template named "{key}".'
        return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_template_clear(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_line_xaxis_set(window,values,configuration_dict,header):
    err_text    = ""
    range_min   = values[f"{header}_xaxis_setting_demod_phase_optimization_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_demod_phase_optimization_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_demod_phase_optimization_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = float(words[0])*words[1]
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = float(words[0])*words[1]
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = int(float(words[0])*words[1])#これだけintじゃないとfinesseがエラーを出す
    # source scriptの作成
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    source_script = values[f"{header}_xaxis_setting_demod_phase_optimization_source_script"]
    #multilineのテキストを一行ずつにする
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if 'xaxis' in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)#1行ずつにしたsource scriptの何行目にxaxisの行があるのかを保存する
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    xaxis_lines = list(window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].Values)
    test_str = ""
    if xaxis_lines != ["","","",""]:
        test_str = '\n'.join(xaxis_lines)
    source_script = source_script.replace(test_str,"")
    lines = source_script.splitlines()
    flag,index = serch_word_in_source_script("xaxis",source_script)
    if flag:#xaxisがある
        lines[index] = f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
        source_script = '\n'.join(lines)
    else:
        added_line=f"""variable deltax 1\nxaxis deltax abs lin {range_min} {range_max} {sampling_num}\n"""
        lines.insert(0, added_line)# xaxisの行は先頭に表示する
        source_script = '\n'.join(lines)

    # 追加したxaxisをsource script multilineに表示する
    source_script = source_script.replace("\n\n\n","")
    window[f"{header}_xaxis_setting_demod_phase_optimization_source_script"].update(value=source_script)

    # 新しいxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs lin {range_min} {range_max} {sampling_num}"
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].update(values=new_list)

    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_line_xaxis_set(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_demod_phase_optimization_source_script_clear(window,values,configuration_dict,header):
    err_text    = ""
    #window,values,configuration_dict,err_text = xaxis_seting_demod_phase_optimization_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs4")
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_xaxis_list"].update(values=["",""])
    window[f"{header}_xaxis_setting_demod_phase_optimization_selected_put_list"].update(values=[])
    window[f"{header}_xaxis_setting_demod_phase_optimization_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs4_xaxis_setting_demod_phase_optimization_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_demod_phase_optimization_source_script_clear(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_simulation_result_plot_run(window,values,configuration_dict):
    err_text = ""
    out_history_key         = values["layout_right_simulation_result_out_history_name"]
    out                     = configuration_dict["out_dict"][out_history_key]
    display_detector_names  = window["layout_right_simulation_result_detectors_list"].GetListValues()

    figure_title            = "demod phase DC sweep" #values["layout_right_simulation_result_figure_title"]
    xlabel                  = "demod phase[deg]"     #values["layout_right_simulation_result_xlabels"]
    ylabel                  = "wfs signal[shiranai]"#values["layout_right_simulation_result_ylabels"]
    scale                   = "linear-linear"       #values["layout_right_simulation_result_plot_scale"]

    #test = values["layout_right_simulation_result_select_pd_legend"]
    #test = values["layout_right_simulation_result_select_line_color"]
    #test = values["layout_right_simulation_result_select_line_style"]

    print(out)
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    fig = My_create_figure4(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict,display_max=True)
    plt.plot(block=False)
    return window,values,configuration_dict,err_text

def demod_phase_optimization_run(window,values,configuration_dict,header):
    err_text = ""
    tab_category = "demod_phase_optimization"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_demod_phase_optimization_source_script"]
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_demod_phase_optimization_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"WFS4",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_simulation_result_plot_run(window,values,configuration_dict)

    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = demod_phase_optimization_run(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text

def demod_phase_optimization_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    tab_category = "demod_phase_optimization"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_{tab_category}_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"WFS4",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs4_demod_phase_optimization_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs4_demod_phase_optimization_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = demod_phase_optimization_template_run(window,values,configuration_dict,"layout_right_wfs4")
    return window,values,configuration_dict,err_text


# %% [markdown]
# # WFS 5

# %%
# wfs5 godemoduy phase optimization
# pdを置くところ

def layout_right_wfs5_pd_setting_initialization(window,values,configuration_dict):
    """
    amplitudeのタブのpdを追加する部分のinitializationをする
    """
    err_text = ""
    tab_category = "wfs_sencing_matrix"
    # node
    all = list(configuration_dict["model"].nodes._NodeNetwork__nodes.keys())
    kensaku = ["REFL","AS","POP","POP2","TMSX","TMSY"]
    #kyotu = list(set(all) & set(kensaku))
    kyotu = all
    window[f"layout_right_wfs5_{tab_category}_pd_setting_node"].update(values=kyotu)
    # demod frequency
    demod_frequencies = ["0","16.881M","45.0159M","28.1349M","-16.881M","-45.0159M","-28.1349M"]
    window[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"].update(values=demod_frequencies)
    window[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])
    # demod phase
    demod_phases = ["-180","-135","-90","-45","0","45","90","135","180","max"]
    window[f"layout_right_wfs5_{tab_category}_pd_setting_demod_phase"].update(values=demod_phases)
    # tem_y
        # pd
            # pdの名前デフォルトの値を最初から入れておく
    window[f"layout_right_wfs5_{tab_category}_pd_setting_node"].update(value=kyotu[0])#node
    window[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"].update(value=demod_frequencies[1])#frequency eomから周波数を取ってきたいけどよくわからないのでf1とf2をとりあえず入れておく
    window[f"layout_right_wfs5_{tab_category}_pd_setting_demod_phase"].update(value="0")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
            # source scriptを初期化する
    window[f"layout_right_wfs5_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict):
    """
    pdにつける名前を作成する
    """
    err_text = ""
    tab_category = "wfs_sencing_matrix"
    try:
        qpd_flag    = values[f"layout_right_wfs5_{tab_category}_use_qpd_check"]
        split_plane = values[f"layout_right_wfs5_{tab_category}_use_qpd_plane_combo"]
        # この辺がエラーチェック
        if values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]=="":
            text_input = "node name is empty.\ndefault value will be set."
            sg.popup('notification', text_input,keep_on_top=True)
            if values[f"layout_right_wfs5_{tab_category}_use_astarisk_check"]:
                node_name = window[f"layout_right_wfs5_{tab_category}_pd_setting_node"].Values[0]+"*"
            else:
                node_name = window[f"layout_right_wfs5_{tab_category}_pd_setting_node"].Values[0]
            window = window.refresh()
            event, values = window.read(10)
            window[f"layout_right_wfs5_{tab_category}_pd_setting_node"].update(value=node_name)
            window = window.refresh()
            event, values = window.read(10)
        else:
            if values[f"layout_right_wfs5_{tab_category}_use_astarisk_check"]:
                node_name = values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]+"*"
            else:
                node_name = values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]
        # このへんが動作部分
        window          = window.refresh()#消さない
        event, values   = window.read(10)
        demod_frequency = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"]
        demod_phase     = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_phase"]
        if qpd_flag:
            if split_plane == "yaw(x-xplit)":
                pdname = f"{node_name}_wfs5_{tab_category}_{demod_frequency}_{demod_phase}_yaw"
            elif split_plane == "pitch(y-xplit)":
                pdname = f"{node_name}_wfs5_{tab_category}_{demod_frequency}_{demod_phase}_pitch"
        else:
                pdname = f"{node_name}_wfs5_{tab_category}_{demod_frequency}_{demod_phase}"
        # pdの名前ができたので表示する
        window[f"layout_right_wfs5_{tab_category}_pd_setting_name"].update(values=[])
        window[f"layout_right_wfs5_{tab_category}_pd_setting_name"].update(value=pdname)
    except Exception as e:
        print(e)
        err_text = "something wrong"#原因不明
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_add_pd(window,values,configuration_dict):
    """
    pdを追加する文を作成してボックスにadd_lineする
    """
    err_text     = ""
    tab_category = "wfs_sencing_matrix"
    old_text     = values[f"layout_right_wfs5_{tab_category}_add_pd_source_script"]
    pdname       = values[f"layout_right_wfs5_{tab_category}_pd_setting_name"]
    node         = values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]
    # pdnameとnodeが設定されてなかったらデフォルトの値をいれる エラーチェック
    if pdname=="" or node=="" or pdname==None or node==None:
        window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
        window = window.refresh()
        event, values = window.read(10)
    # ノードに*入れるなら入れる
    old_text    = values[f"layout_right_wfs5_{tab_category}_add_pd_source_script"]
    pdname      = values[f"layout_right_wfs5_{tab_category}_pd_setting_name"]
    if values[f"layout_right_wfs5_{tab_category}_use_astarisk_check"]==True:
        node    = values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]+"*"
        print(node)
    else:
        node    = values[f"layout_right_wfs5_{tab_category}_pd_setting_node"]
        print(node)
    # 同じ名前のpdがあったら名前を変更する
    lines = old_text.splitlines()  #=> ['AAA', 'BBB', 'CCC']
    for line in lines:
        test_pd_name = line.split(' ')
        if pdname in test_pd_name:
            err_text = "There is another pd with the same name, so please rename it."
            return window,values,configuration_dict,err_text
    #動作
    if old_text=="\n":
        demod_frequency   = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_phase"]
        # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            added_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
    else:
        demod_frequency   = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_frequency"]
        demod_phase       = values[f"layout_right_wfs5_{tab_category}_pd_setting_demod_phase"]
        words = utility.unit_convert(demod_frequency)
        if utility.isfloat(words[0]) == True:#words = 7(=words[0]) * 1e-9(=words[1])
            num_head = float(words[0])
            num_tail = words[1]
            num = num_head*num_tail
            new_line  = f"pd2 {pdname} {num} {demod_phase} 10 0 {node}"
            
        else:
            err_text = "Unable to execute simulation because frequency characters contain invalid characters."
            return window,values,configuration_dict,err_text
        added_line  = old_text + new_line
    qpd_flag    = values[f"layout_right_wfs5_{tab_category}_use_qpd_check"]
    split_plane = values[f"layout_right_wfs5_{tab_category}_use_qpd_plane_combo"]
    if qpd_flag:
        if split_plane == "yaw(x-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} x-split"
        elif split_plane == "pitch(y-xplit)":
            added_line  = added_line +"\n"+ f"pdtype {pdname} y-split"
    # demod f2をスイープさせるところ　ひとまずここに入れておく
    added_line = added_line +"\n"+f"put {pdname} f2 $x1"
    # 表示を更新する
    window[f"layout_right_wfs5_{tab_category}_add_pd_source_script"].update(value=added_line)
    return window,values,configuration_dict,err_text
    
def layout_right_wfs5_wfs_sencing_matrix_pd_setting_clear_pd(window,values,configuration_dict):
    """
    ボックスに追加したpdの文を""で更新して消す
    """
    err_text = ""
    tab_category = "wfs_sencing_matrix"
    window[f"layout_right_wfs5_{tab_category}_add_pd_source_script"].update(value="")
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_help(window,values,configuration_dict):
    """
    今は使えない
    """
    err_text = ""
    text_input = "Adjustment of the Gouy phase is done by changing the parameters of the SPACE component to which the port is connected.\nFor example, if you want to place a PD on REFL where the gouy phase is 0 and 90 degrees, you need to set the gouy phase of REFL_gouy_tuner to 0 degrees, run the simulation once, set it to 90 degrees, and run the simulation again.\nIn other words, the simulation is performed twice."
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_update(window,values,configuration_dict):
    """
    今は使えないのでポップアップを出すだけ
    """
    err_text = ""
    text_input = "現在使えないのでExtra settingsの欄から変更してください。\n画面左の2番目のタブです。"
    sg.popup('Help', text_input,keep_on_top=True)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_use_astarisk_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_node(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_use_qpd_plane_combo(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_use_qpd_check(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_frequency(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_phase(window,values,configuration_dict):
    window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

# xaxis
#
def xaxis_seting_wfs_sencing_matrix_mirror_combo_initialization(window,values,configuration_dict,header):
    """
    xaxisを設定する部分のGUIの初期化
    headerを受け取れば同じ動作ができる？
    """
    err_text = ""
    # xaxisのrangeを初期化する
    range_list = ["0.01","0.1","1","10","100","1000"]
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_range_min_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_range_min_select"].update(value="0.01")
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_range_max_select"].update(values=range_list)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_range_max_select"].update(value="1000")
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_sampling_num_select"].update(values=["1000"])
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_sampling_num_select"].update(value="1000")
    # comboのリストにmirrorとbeamsplitterを入れて更新する
    components = []
    try:
        mirrors         = utility.classify_model_components(configuration_dict["model"])["mirror"]
        components      = components + mirrors
    except KeyError:
        print("Key error 'mirror' in 'xaxis_seting_dc_mirror_combo_initialization'")
    try:
        beam_splitters  = utility.classify_model_components(configuration_dict["model"])["beamSplitter"]
        components      = components + beam_splitters
    except KeyError:
        print("Key error 'beamSplitter' in 'xaxis_seting_dc_mirror_combo_initialization'")
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_mirror_select"].update(values=components)
    window = window.refresh()
    event, values = window.read(10)
    # デフォルトで表示されるmirrorをセットする
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_mirror_select"].update(value=components[0])
    # listboxをリセットする
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].update(values=["","","",""])
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_put_list"].update(values=[])
    # source scriptをリセットする
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_seting_mirror_combo_initialization(window,values,configuration_dict):
    err_text = ""
    window,values,configuration_dict,err_text = xaxis_seting_wfs_sencing_matrix_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_add_xaxis(window,values,configuration_dict,header):
    err_text    = ""
    mirror      = values[f"{header}_xaxis_setting_wfs_sencing_matrix_mirror_select"]
    parameter   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_parameter_select"]
    #put_astarisk= values[f"{header}_xaxis_setting_wfs_sencing_matrix_put_astarisk_check"]
    put_command = "put"
    #if put_astarisk:
    #    put_command = "put*"
    direction   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_direction_select"]
    direction_variables = {
        "+":"0",
        "-":"180"
    }
    range_min   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_wfs_sencing_matrix_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = str(float(words[0])*words[1])
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = str(float(words[0])*words[1])
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = str(int(float(words[0])*words[1]))#これだけintじゃないとfinesseがエラーを出す
    # 動作 source scriptに追加する文章の作成
    source_script = values[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"]
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    #   # wordを探す
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if word in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    flag,index = serch_word_in_source_script("xaxis",source_script)
    lines = source_script.splitlines()
    if flag:#xaxisがある
        # 既にあるので変更しない
        source_script = '\n'.join(lines)
        #added_line = f"""{put_command} {mirror} {parameter} {direction_variables[direction]}\n"""
        added_line = f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line
    else:#xaxisがない
        # xaxis追加
        source_script=f"""variable deltax 1\nxaxis deltax abs log {range_min} {range_max} {sampling_num}\nput sig1 f $x1\n\n"""
        # ミラー追加
        added_line =f"""fsig sig1 {mirror} {parameter} 10 {direction_variables[direction]}\n"""
        if added_line in source_script:#すでに同じ文章がある場合にはエラーを返す.
            err_text = "This condition already exists."
            return window,values,configuration_dict,err_text
        source_script += added_line

    # 追加したxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs log {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs log {range_min} {range_max} {sampling_num}"
        
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].update(values=new_list)
    #   # put listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_put_list"].Values)
    new_list = current_list
    new_list.append(f"{mirror} {parameter} {direction}")
    if '' in new_list:
        new_list.remove('')
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_put_list"].update(values=new_list)

    # 追加したxaxisをsource script multilineに表示する
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"].update(value=source_script)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_add_xaxis(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_add_xaxis(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_save_as_template(window,values,configuration_dict,header):
    err_text = ""
    name        = values[f"{header}_xaxis_setting_wfs_sencing_matrix_set_template_name"]
    xaxis_list  = window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].Values
    put_list    = window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_put_list"].Values
    script      = values[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"]
    # templateを選択するcomboにnameを追加する
    #   # 同じnameは追加しない(上書きする)
    combo_options = window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"].Values
    if name in combo_options:
        text_input = "Template updated."
        sg.popup('notification', text_input,keep_on_top=True)
    else:
        text_input = "A template was added."
        sg.popup('notification', text_input,keep_on_top=True)
        combo_options.append(name)
    #   # comboの更新
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"].update(values=combo_options)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"].update(value=name)
    # source scriptの更新
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_source_script"].update(value=script)
    # listboxの更新
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_put_list"].update(values=put_list)
    # 辞書に保存する
    configuration_dict["xaxis_template_dict"][f"{header}_{name}"]       = script
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_xaxis"] = xaxis_list
    configuration_dict["xaxis_template_dict"][f"{header}_{name}_put"]   = put_list
    #テスト
    print(configuration_dict["xaxis_template_dict"].keys())
    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_save_as_template(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_save_as_template(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_template_load(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"]
    # key error
    try:
        saved_script = configuration_dict["xaxis_template_dict"][f"{header}_{key}"]
        xaxis_list   = configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"]
        put_list     = configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"]
    except Exception as e:
        print(e)
        err_text = "The KEY to loading simulation results is in error."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_source_script"].update(value=saved_script)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_xaxis_list"].update(values=xaxis_list)
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_put_list"].update(values=put_list)
    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_load(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_template_load(window,values,configuration_dict,"layout_right_wfs5")
    print(err_text)
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_template_clear(window,values,configuration_dict,header):
    err_text=""
    key = values[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"]
    check_key = f"{header}_{key}"
    if check_key in configuration_dict["xaxis_template_dict"].keys():
        # 辞書から削除
        del configuration_dict["xaxis_template_dict"][f"{header}_{key}"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_xaxis"],configuration_dict["xaxis_template_dict"][f"{header}_{key}_put"] 
        # 表示の削除
        names_list = window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"].Values
        names_list.remove(key)
        window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_select_name"].update(values=names_list)
        window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_source_script"].update(value="")
        window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_xaxis_list"].update(values=["",""])
        window[f"{header}_xaxis_setting_wfs_sencing_matrix_template_put_list"].update(values=[])
        return window,values,configuration_dict,err_text
    else:
        err_text=f'There is no template named "{key}".'
        return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_template_clear(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_line_xaxis_set(window,values,configuration_dict,header):
    err_text    = ""
    range_min   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_range_min_select"]
    range_max   = values[f"{header}_xaxis_setting_wfs_sencing_matrix_range_max_select"]
    sampling_num= values[f"{header}_xaxis_setting_wfs_sencing_matrix_sampling_num_select"]
    # range_min,range_max,sampling_numに数字以外が入ってないかチェックする
    words = utility.unit_convert(range_min)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_min = float(words[0])*words[1]
    words = utility.unit_convert(range_max)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    range_max = float(words[0])*words[1]
    words = utility.unit_convert(sampling_num)
    if utility.isfloat(words[0]) != True:
        err_text = "The xaxis range contains unavailable characters.\nPlease enter a number."
        return window,values,configuration_dict,err_text
    sampling_num = int(float(words[0])*words[1])#これだけintじゃないとfinesseがエラーを出す
    # source scriptの作成
    #   # xaxisが既にある場合とない場合で追加のさせ方を変えないとエラーになる
    source_script = values[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"]
    #multilineのテキストを一行ずつにする
    def serch_word_in_source_script(word,source_script):#word = "xaxis"
        lines = source_script.splitlines()
        for one_gyou in lines:
            # xaxis PRM phi lin -180 180 1000のようなxaxisがある行を探す
            splitted_gyou = one_gyou.split()
            if word in splitted_gyou:#ある(二回目以降)
                current_gyou = lines.index(one_gyou)#1行ずつにしたsource scriptの何行目にxaxisの行があるのかを保存する
                return True,current_gyou
            else:#ない(初回)
                pass
        return False,0
    xaxis_lines = list(window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].Values)
    test_str = ""
    if xaxis_lines != ["","","",""]:
        test_str = '\n'.join(xaxis_lines)
    source_script = source_script.replace(test_str,"")
    lines = source_script.splitlines()
    flag,index = serch_word_in_source_script("xaxis",source_script)
    if flag:#xaxisがある
        lines[index] = f"xaxis deltax abs log {range_min} {range_max} {sampling_num}\nput sig1 f $x1\n"
        source_script = '\n'.join(lines)
    else:
        added_line=f"""variable deltax 1\nxaxis deltax abs log {range_min} {range_max} {sampling_num}\nput sig1 f $x1\n"""
        lines.insert(0, added_line)# xaxisの行は先頭に表示する
        source_script = '\n'.join(lines)

    # 追加したxaxisをsource script multilineに表示する
    source_script = source_script.replace("\n\n\n","")
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"].update(value=source_script)

    # 新しいxaxisをlistboxに表示する
    #   # xaxis listboxに追加
    current_list = list(window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].Values)
    new_list = current_list
    if flag:#xaxisがある
        new_list[1]=f"xaxis deltax abs log {range_min} {range_max} {sampling_num}"
    else:
        new_list[0]="variable deltax 1"
        new_list[1]=f"xaxis deltax abs log {range_min} {range_max} {sampling_num}"
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].update(values=new_list)

    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_line_xaxis_set(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_line_xaxis_set(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text
#
def xaxis_setting_wfs_sencing_matrix_source_script_clear(window,values,configuration_dict,header):
    err_text    = ""
    #window,values,configuration_dict,err_text = xaxis_seting_wfs_sencing_matrix_mirror_combo_initialization(window,values,configuration_dict,"layout_right_wfs5")
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_xaxis_list"].update(values=["",""])
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_selected_put_list"].update(values=[])
    window[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"].update(value="")
    return window,values,configuration_dict,err_text
#
def layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_source_script_clear(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = xaxis_setting_wfs_sencing_matrix_source_script_clear(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_simulation_result_plot_run(window,values,configuration_dict):
    err_text = ""
    out_history_key                                              = "current_out"#values["layout_right_simulation_result_out_history_name"]
    #configuration_dict["out_history_name_dict"][out_history_key] = "demod_transfer_function"
    out                                                          = configuration_dict["out_dict"][out_history_key]
    display_detector_names                                       = window["layout_right_simulation_result_detectors_list"].GetListValues()

    figure_title                                                 = values["layout_right_simulation_result_figure_title"] #"demod phase DC sweep" 
    xlabel                                                       = values["layout_right_simulation_result_xlabels"]      #"demod phase[deg]"     
    ylabel                                                       = values["layout_right_simulation_result_ylabels"]      #"wfs signal[shiranai]" 
    scale                                                        = values["layout_right_simulation_result_plot_scale"]   #"linear-linear"        

    #test = values["layout_right_simulation_result_select_pd_legend"]
    #test = values["layout_right_simulation_result_select_line_color"]
    #test = values["layout_right_simulation_result_select_line_style"]

    print(out)
    if out=="default":
        err_text="Simulation results could not be fetched.\nPlease check the name."
        return window,values,configuration_dict,err_text
    fig = My_create_figure5(out,scale,figure_title,xlabel,ylabel,display_detector_names,configuration_dict,display_point=0)
    plt.plot(block=False)
    return window,values,configuration_dict,err_text

def wfs_sencing_matrix_run(window,values,configuration_dict,header):
    err_text = ""
    tab_category = "wfs_sencing_matrix"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    xaxis_text  = values[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"]
    # error check
    #   # pd部分のsource scriptがない
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # xaxis部分のsource scriptがない
    elif xaxis_text=="":
        err_text    = "Xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
        xaxis_text  = values[f"{header}_xaxis_setting_wfs_sencing_matrix_source_script"]
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # 一回初期化
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    # プロット設定
    window["layout_right_simulation_result_figure_title"].update(value="WFS transfer function")
    window["layout_right_simulation_result_ylabels"].update(value="WFS signal[W/rad]")
    window["layout_right_simulation_result_xlabels"].update(value="Oscillation frequency[Hz]")
    window["layout_right_simulation_result_plot_scale"].update(value="log-log")
    window["layout_right_simulation_result_select_plot_style"].update(value="abs+angle")
    # plotを実行する
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"demod_transfer_function",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_simulation_result_plot_run(window,values,configuration_dict)

    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_run(window,values,configuration_dict):
    err_text    = ""
    # 動作
    window,values,configuration_dict,err_text = wfs_sencing_matrix_run(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text

def wfs_sencing_matrix_template_run(window,values,configuration_dict,header):
    err_text = ""
    # error check
    #   # pd部分のsource scriptがない
    tab_category = "wfs_sencing_matrix"
    pd_text     = values[f"{header}_{tab_category}_add_pd_source_script"]
    if pd_text=="":
        err_text    = "PD setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptがない
    xaxis_text     = values[f"{header}_xaxis_setting_{tab_category}_template_source_script"]
    if xaxis_text=="":
        err_text    = "xaxis setting is empty."
        return window,values,configuration_dict,err_text
    #   # source scriptをfinesseで読み込むことができなかった
    try:
        test_model = configuration_dict["model"].deepcopy()
        test_model.parse(pd_text)
        test_model.parse(xaxis_text)
        out = test_model.run()
    except Exception as e:
        print(e)
        err_text    = "The simulation could not be executed, please check the settings."
        return window,values,configuration_dict,err_text
    except:
        err_text = "sys.exit(),SystemExit"
        return window,values,configuration_dict,err_text
    # 動作
    err_text    = ""
    configuration_dict["out_dict"]["current_out"]=out
    # 一回初期化する
    window,values,configuration_dict,err_text = layout_right_simulation_result_out_history_load(window,values,configuration_dict,notification=False)
    window = window.refresh()
    event, values = window.read(10)
    # プロット設定
    window["layout_right_simulation_result_figure_title"].update(value="WFS transfer function")
    window["layout_right_simulation_result_ylabels"].update(value="WFS signal[W/rad]")
    window["layout_right_simulation_result_xlabels"].update(value="Oscillation frequency[Hz]")
    window["layout_right_simulation_result_plot_scale"].update(value="log-log")
    window["layout_right_simulation_result_select_plot_style"].update(value="abs+angle")
    # plotを実行する
    window,values,configuration_dict,err_text = layout_right_simulation_result_all_detectors_add(window,values,configuration_dict)
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = set_simulation_configuration_dict(window,values,configuration_dict,"demod_transfer_function",out_history_key="current_out")
    window = window.refresh()
    event, values = window.read(10)
    window,values,configuration_dict,err_text = layout_right_simulation_result_plot_run(window,values,configuration_dict)
    #window,values,configuration_dict,err_text = layout_right_wfs5_wfs_sencing_matrix_simulation_result_plot_run(window,values,configuration_dict)
    return window,values,configuration_dict,err_text

def layout_right_wfs5_wfs_sencing_matrix_template_run(window,values,configuration_dict):
    err_text    = ""
    window,values,configuration_dict,err_text = wfs_sencing_matrix_template_run(window,values,configuration_dict,"layout_right_wfs5")
    return window,values,configuration_dict,err_text


# %% [markdown]
# # Run

# %%

# global variables
text_temp = Asc_base_model.Get_PRMI_base_model()
kat_script_temp = utility.remove_header(text_temp)
base = finesse.kat()
base.parse(kat_script_temp)
model_g = finesse.kat()

configuration_dict_g = {
    "base"                  : base,
    "model"                 : model_g,
    # simulation configration
    #   # xaxis
    "xaxis_template_dict"   : {"sample":"sample"},
    "out_dict"              : {"sample":"sample"},
    "plot_conf_dict"        : {"sample":"sample"},
    #"out_plot_conf_dict"    : {"sample":{}},
}

# Lookup dictionary that maps button to function to call
wvm_dictionary = {
    # left layout
    #   # Base model
    'base_model_filter_all'                                     :base_model_filter_all,
    'base_model_filter_others'                                  :base_model_filter_others,
    'base_model_filter_categories'                              :base_model_filter_categories,
    'base_model_filter_items'                                   :base_model_filter_items,
    'base_model_update_base_model'                              :base_model_update_base_model,
    'base_model_default_base_model'                             :base_model_default_base_model,
    'base_setting_base_update'                                  :base_setting_base_update,
    #   # parameter 
    'layout_left_1_Extra_Settings_class_name'                   :Reset_extra_settings_class,
    'layout_left_1_Extra_Settings_component_name'               :Reset_extra_settings_component,
    'layout_left_1_Extra_Settings_atrribute'                    :Reset_extra_settings_attribute,
    'layout_left_1_Extra_Settings_parameter_modify'             :layout_left_1_Extra_Settings_parameter_modify,
    'layout_left_1_Extra_Settings_maxtem_modify'                :layout_left_1_Extra_Settings_maxtem_modify,
    #   # bp check  
    'bp_check_check_beam_parameter'                             :bp_check_check_beam_parameter,
    'bp_check_open_interactive_window'                          :bp_check_open_interactive_window,
    'bp_check_select_parameter'                                 :bp_check_select_parameter,
    # right layout
    #   # dc power
    #   #   # pd部分
    'layout_right_dc_power_pd_setting_name_template'            :layout_right_dc_power_pd_setting_name_template,#
    'layout_right_dc_power_pd_setting_node'                     :layout_right_dc_power_pd_setting_node,#
    'layout_right_dc_power_use_qpd_plane_combo'                 :layout_right_dc_power_use_qpd_plane_combo,#
    'layout_right_dc_power_use_qpd_check'                       :layout_right_dc_power_use_qpd_check,#
    'layout_right_dc_power_use_astarisk_check'                  :layout_right_dc_power_use_astarisk_check,#
    'layout_right_dc_power_pd_setting_add_pd'                   :layout_right_dc_power_pd_setting_add_pd,#
    'layout_right_dc_power_pd_setting_clear_pd'                 :layout_right_dc_power_pd_setting_clear_pd,#
    'layout_right_dc_power_pd_setting_gouy_tuner_help'          :layout_right_dc_power_pd_setting_gouy_tuner_help,
    'layout_right_dc_power_pd_setting_gouy_tuner_update'        :layout_right_dc_power_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_dc_power_xaxis_setting_DC_add_xaxis'          :layout_right_dc_power_xaxis_setting_DC_add_xaxis,#
    'layout_right_dc_power_xaxis_setting_DC_save_as_template'   :layout_right_dc_power_xaxis_setting_DC_save_as_template,#
    'layout_right_dc_power_xaxis_setting_DC_template_load'      :layout_right_dc_power_xaxis_setting_DC_template_load,#
    'layout_right_dc_power_xaxis_setting_DC_template_clear'     :layout_right_dc_power_xaxis_setting_DC_template_clear,#
    'layout_right_dc_power_xaxis_setting_DC_line_xaxis_set'     :layout_right_dc_power_xaxis_setting_DC_line_xaxis_set,#
    'layout_right_dc_power_xaxis_setting_DC_source_script_clear':layout_right_dc_power_xaxis_setting_DC_source_script_clear,#
    'layout_right_dc_power_add_dof_run'                         :layout_right_dc_power_add_dof_run,#
    'layout_right_dc_power_template_run'                        :layout_right_dc_power_template_run,#
    #   # amplitude
    #   #   # pd部分
    'layout_right_amplitude_pd_setting_name_template'            :layout_right_amplitude_pd_setting_name_template,#
    'layout_right_amplitude_pd_setting_node'                     :layout_right_amplitude_pd_setting_node,#
    'layout_right_amplitude_use_qpd_plane_combo'                 :layout_right_amplitude_use_qpd_plane_combo,#
    'layout_right_amplitude_use_qpd_check'                       :layout_right_amplitude_use_qpd_check,#
    'layout_right_amplitude_use_astarisk_check'                  :layout_right_amplitude_use_astarisk_check,#
    'layout_right_amplitude_pd_setting_frequency'                :layout_right_amplitude_pd_setting_frequency,
    'layout_right_amplitude_use_all_tem_check'                   :layout_right_amplitude_use_all_tem_check,
    'layout_right_amplitude_pd_setting_tem_x'                    :layout_right_amplitude_pd_setting_tem_x,
    'layout_right_amplitude_pd_setting_tem_y'                    :layout_right_amplitude_pd_setting_tem_y,
    'layout_right_amplitude_pd_setting_add_pd'                   :layout_right_amplitude_pd_setting_add_pd,#
    'layout_right_amplitude_pd_setting_clear_pd'                 :layout_right_amplitude_pd_setting_clear_pd,#
    'layout_right_amplitude_pd_setting_gouy_tuner_help'          :layout_right_amplitude_pd_setting_gouy_tuner_help,
    'layout_right_amplitude_pd_setting_gouy_tuner_update'        :layout_right_amplitude_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_amplitude_xaxis_setting_DC_add_xaxis'          :layout_right_amplitude_xaxis_setting_DC_add_xaxis,#
    'layout_right_amplitude_xaxis_setting_DC_save_as_template'   :layout_right_amplitude_xaxis_setting_DC_save_as_template,#
    'layout_right_amplitude_xaxis_setting_DC_template_load'      :layout_right_amplitude_xaxis_setting_DC_template_load,#
    'layout_right_amplitude_xaxis_setting_DC_template_clear'     :layout_right_amplitude_xaxis_setting_DC_template_clear,#
    'layout_right_amplitude_xaxis_setting_DC_line_xaxis_set'     :layout_right_amplitude_xaxis_setting_DC_line_xaxis_set,#
    'layout_right_amplitude_xaxis_setting_DC_source_script_clear':layout_right_amplitude_xaxis_setting_DC_source_script_clear,#
    'layout_right_amplitude_add_dof_run'                         :layout_right_amplitude_add_dof_run,#
    'layout_right_amplitude_template_run'                        :layout_right_amplitude_template_run,#
    #   # Wfs3 gouy phase optimization
    #   #   # pd部分
    'layout_right_wfs3_gouy_tuner_pd_setting_name_template'                    :layout_right_wfs3_gouy_tuner_pd_setting_name_template,#
    'layout_right_wfs3_gouy_tuner_pd_setting_node'                             :layout_right_wfs3_gouy_tuner_pd_setting_node,#
    'layout_right_wfs3_gouy_tuner_use_qpd_plane_combo'                         :layout_right_wfs3_gouy_tuner_use_qpd_plane_combo,#
    'layout_right_wfs3_gouy_tuner_use_qpd_check'                               :layout_right_wfs3_gouy_tuner_use_qpd_check,#
    'layout_right_wfs3_gouy_tuner_use_astarisk_check'                          :layout_right_wfs3_gouy_tuner_use_astarisk_check,#
    'layout_right_wfs3_gouy_tuner_pd_setting_demod_frequency'                  :layout_right_wfs3_gouy_tuner_pd_setting_demod_frequency,
    'layout_right_wfs3_gouy_tuner_pd_setting_demod_phase'                      :layout_right_wfs3_gouy_tuner_pd_setting_demod_phase,
    'layout_right_wfs3_gouy_tuner_pd_setting_add_pd'                           :layout_right_wfs3_gouy_tuner_pd_setting_add_pd,#
    'layout_right_wfs3_gouy_tuner_pd_setting_clear_pd'                         :layout_right_wfs3_gouy_tuner_pd_setting_clear_pd,#
    'layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_help'                  :layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_help,
    'layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_update'                :layout_right_wfs3_gouy_tuner_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_wfs3_xaxis_setting_gouy_tuner_add_xaxis'                     :layout_right_wfs3_xaxis_setting_gouy_tuner_add_xaxis,#
    'layout_right_wfs3_xaxis_setting_gouy_tuner_save_as_template'              :layout_right_wfs3_xaxis_setting_gouy_tuner_save_as_template,#
    'layout_right_wfs3_xaxis_setting_gouy_tuner_template_load'                 :layout_right_wfs3_xaxis_setting_gouy_tuner_template_load,#
    'layout_right_wfs3_xaxis_setting_gouy_tuner_template_clear'                :layout_right_wfs3_xaxis_setting_gouy_tuner_template_clear,#
    'layout_right_wfs3_xaxis_setting_gouy_tuner_line_xaxis_set'                :layout_right_wfs3_xaxis_setting_gouy_tuner_line_xaxis_set,#
    'layout_right_wfs3_xaxis_setting_gouy_tuner_source_script_clear'           :layout_right_wfs3_xaxis_setting_gouy_tuner_source_script_clear,#
    'layout_right_wfs3_gouy_tuner_run'                                         :layout_right_wfs3_gouy_tuner_run,#
    'layout_right_wfs3_gouy_tuner_template_run'                                :layout_right_wfs3_gouy_tuner_template_run,#
    #   # Wfs4 demod phase optimization
    #   #   # pd部分
    'layout_right_wfs4_demod_phase_optimization_pd_setting_name_template'           :layout_right_wfs4_demod_phase_optimization_pd_setting_name_template,#
    'layout_right_wfs4_demod_phase_optimization_pd_setting_node'                    :layout_right_wfs4_demod_phase_optimization_pd_setting_node,#
    'layout_right_wfs4_demod_phase_optimization_use_qpd_plane_combo'                :layout_right_wfs4_demod_phase_optimization_use_qpd_plane_combo,#
    'layout_right_wfs4_demod_phase_optimization_use_qpd_check'                      :layout_right_wfs4_demod_phase_optimization_use_qpd_check,#
    'layout_right_wfs4_demod_phase_optimization_use_astarisk_check'                 :layout_right_wfs4_demod_phase_optimization_use_astarisk_check,#
    'layout_right_wfs4_demod_phase_optimization_pd_setting_demod_frequency'         :layout_right_wfs4_demod_phase_optimization_pd_setting_demod_frequency,
    'layout_right_wfs4_demod_phase_optimization_pd_setting_demod_phase'             :layout_right_wfs4_demod_phase_optimization_pd_setting_demod_phase,
    'layout_right_wfs4_demod_phase_optimization_pd_setting_add_pd'                  :layout_right_wfs4_demod_phase_optimization_pd_setting_add_pd,#
    'layout_right_wfs4_demod_phase_optimization_pd_setting_clear_pd'                :layout_right_wfs4_demod_phase_optimization_pd_setting_clear_pd,#
    'layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_help'         :layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_help,
    'layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_update'       :layout_right_wfs4_demod_phase_optimization_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_add_xaxis'            :layout_right_wfs4_xaxis_setting_demod_phase_optimization_add_xaxis,#
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_save_as_template'     :layout_right_wfs4_xaxis_setting_demod_phase_optimization_save_as_template,#
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_load'        :layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_load,#
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_clear'       :layout_right_wfs4_xaxis_setting_demod_phase_optimization_template_clear,#
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_line_xaxis_set'       :layout_right_wfs4_xaxis_setting_demod_phase_optimization_line_xaxis_set,#
    'layout_right_wfs4_xaxis_setting_demod_phase_optimization_source_script_clear'  :layout_right_wfs4_xaxis_setting_demod_phase_optimization_source_script_clear,#
    'layout_right_wfs4_demod_phase_optimization_run'                                :layout_right_wfs4_demod_phase_optimization_run,#
    'layout_right_wfs4_demod_phase_optimization_template_run'                       :layout_right_wfs4_demod_phase_optimization_template_run,#
    #   # Wfs1
    #   #   # pd部分
    'layout_right_wfs1_pd_setting_name_template'            :layout_right_wfs1_pd_setting_name_template,#
    'layout_right_wfs1_pd_setting_node'                     :layout_right_wfs1_pd_setting_node,#
    'layout_right_wfs1_use_qpd_plane_combo'                 :layout_right_wfs1_use_qpd_plane_combo,#
    'layout_right_wfs1_use_qpd_check'                       :layout_right_wfs1_use_qpd_check,#
    'layout_right_wfs1_use_astarisk_check'                  :layout_right_wfs1_use_astarisk_check,#
    'layout_right_wfs1_pd_setting_demod_frequency'          :layout_right_wfs1_pd_setting_demod_frequency,
    'layout_right_wfs1_pd_setting_demod_phase'              :layout_right_wfs1_pd_setting_demod_phase,
    'layout_right_wfs1_pd_setting_add_pd'                   :layout_right_wfs1_pd_setting_add_pd,#
    'layout_right_wfs1_pd_setting_clear_pd'                 :layout_right_wfs1_pd_setting_clear_pd,#
    'layout_right_wfs1_pd_setting_gouy_tuner_help'          :layout_right_wfs1_pd_setting_gouy_tuner_help,
    'layout_right_wfs1_pd_setting_gouy_tuner_update'        :layout_right_wfs1_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_wfs1_xaxis_setting_DC_add_xaxis'          :layout_right_wfs1_xaxis_setting_DC_add_xaxis,#
    'layout_right_wfs1_xaxis_setting_DC_save_as_template'   :layout_right_wfs1_xaxis_setting_DC_save_as_template,#
    'layout_right_wfs1_xaxis_setting_DC_template_load'      :layout_right_wfs1_xaxis_setting_DC_template_load,#
    'layout_right_wfs1_xaxis_setting_DC_template_clear'     :layout_right_wfs1_xaxis_setting_DC_template_clear,#
    'layout_right_wfs1_xaxis_setting_DC_line_xaxis_set'     :layout_right_wfs1_xaxis_setting_DC_line_xaxis_set,#
    'layout_right_wfs1_xaxis_setting_DC_source_script_clear':layout_right_wfs1_xaxis_setting_DC_source_script_clear,#
    'layout_right_wfs1_add_dof_run'                         :layout_right_wfs1_add_dof_run,#
    'layout_right_wfs1_template_run'                        :layout_right_wfs1_template_run,#

    '''
    #   # Wfs2
    #   #   # pd部分
    'layout_right_wfs2_pd_setting_name_template'            :layout_right_wfs2_pd_setting_name_template,#
    'layout_right_wfs2_pd_setting_node'                     :layout_right_wfs2_pd_setting_node,#
    'layout_right_wfs2_use_qpd_plane_combo'                 :layout_right_wfs2_use_qpd_plane_combo,#
    'layout_right_wfs2_use_qpd_check'                       :layout_right_wfs2_use_qpd_check,#
    'layout_right_wfs2_use_astarisk_check'                  :layout_right_wfs2_use_astarisk_check,#
    'layout_right_wfs2_pd_setting_demod_frequency'          :layout_right_wfs2_pd_setting_demod_frequency,
    'layout_right_wfs2_pd_setting_demod_phase'              :layout_right_wfs2_pd_setting_demod_phase,
    'layout_right_wfs2_pd_setting_add_pd'                   :layout_right_wfs2_pd_setting_add_pd,#
    'layout_right_wfs2_pd_setting_clear_pd'                 :layout_right_wfs2_pd_setting_clear_pd,#
    'layout_right_wfs2_pd_setting_gouy_tuner_help'          :layout_right_wfs2_pd_setting_gouy_tuner_help,
    'layout_right_wfs2_pd_setting_gouy_tuner_update'        :layout_right_wfs2_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_wfs2_xaxis_setting_DC_add_xaxis'          :layout_right_wfs2_xaxis_setting_DC_add_xaxis,#
    'layout_right_wfs2_xaxis_setting_DC_save_as_template'   :layout_right_wfs2_xaxis_setting_DC_save_as_template,#
    'layout_right_wfs2_xaxis_setting_DC_template_load'      :layout_right_wfs2_xaxis_setting_DC_template_load,#
    'layout_right_wfs2_xaxis_setting_DC_template_clear'     :layout_right_wfs2_xaxis_setting_DC_template_clear,#
    'layout_right_wfs2_xaxis_setting_DC_line_xaxis_set'     :layout_right_wfs2_xaxis_setting_DC_line_xaxis_set,#
    'layout_right_wfs2_xaxis_setting_DC_source_script_clear':layout_right_wfs2_xaxis_setting_DC_source_script_clear,#
    'layout_right_wfs2_add_dof_run'                         :layout_right_wfs2_add_dof_run,#
    'layout_right_wfs2_template_run'                        :layout_right_wfs2_template_run,#
    '''
    
    #   # Wfs5 Sensing matrix
    #   #   # pd部分
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template'           :layout_right_wfs5_wfs_sencing_matrix_pd_setting_name_template,#
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_node'                    :layout_right_wfs5_wfs_sencing_matrix_pd_setting_node,#
    'layout_right_wfs5_wfs_sencing_matrix_use_qpd_plane_combo'                :layout_right_wfs5_wfs_sencing_matrix_use_qpd_plane_combo,#
    'layout_right_wfs5_wfs_sencing_matrix_use_qpd_check'                      :layout_right_wfs5_wfs_sencing_matrix_use_qpd_check,#
    'layout_right_wfs5_wfs_sencing_matrix_use_astarisk_check'                 :layout_right_wfs5_wfs_sencing_matrix_use_astarisk_check,#
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_frequency'         :layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_frequency,
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_phase'             :layout_right_wfs5_wfs_sencing_matrix_pd_setting_demod_phase,
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_add_pd'                  :layout_right_wfs5_wfs_sencing_matrix_pd_setting_add_pd,#
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_clear_pd'                :layout_right_wfs5_wfs_sencing_matrix_pd_setting_clear_pd,#
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_help'         :layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_help,
    'layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_update'       :layout_right_wfs5_wfs_sencing_matrix_pd_setting_gouy_tuner_update,
    #   #   # xaxis部分
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_add_xaxis'            :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_add_xaxis,#
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_save_as_template'     :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_save_as_template,#
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_load'        :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_load,#
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_clear'       :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_template_clear,#
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_line_xaxis_set'       :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_line_xaxis_set,#
    'layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_source_script_clear'  :layout_right_wfs5_xaxis_setting_wfs_sencing_matrix_source_script_clear,#
    'layout_right_wfs5_wfs_sencing_matrix_run'                                :layout_right_wfs5_wfs_sencing_matrix_run,#
    'layout_right_wfs5_wfs_sencing_matrix_template_run'                       :layout_right_wfs5_wfs_sencing_matrix_template_run,#

    #   # simulation tab
    'layout_right_simulation_result_plot_run'                                 :layout_right_simulation_result_plot_run,
    'layout_right_simulation_result_clear_axes'                               :layout_right_simulation_result_clear_axes,
    'layout_right_simulation_result_clear_figure'                             :layout_right_simulation_result_clear_figure,
    'layout_right_simulation_result_select_detectors_add'                     :layout_right_simulation_result_select_detectors_add,
    'layout_right_simulation_result_out_history_load'                         :layout_right_simulation_result_out_history_load,
    'layout_right_simulation_result_open_figure_window'                       :layout_right_simulation_result_open_figure_window,
    # 途中のやつ
    'layout_right_simulation_result_axes_title'                               :layout_right_simulation_result_axes_title,
    'layout_right_simulation_result_axes_number'                              :layout_right_simulation_result_axes_number,
    }



# Show the Window to the user 1366×768
window_g = sg.Window('Button callback example',layout,size=(1280,768),resizable=True,finalize=True)
window_g = window_g.refresh()
event_g, values_g = window_g.read(10)
window_g,values_g,configuration_dict_g,err_text = Initialization(window_g,values_g,configuration_dict_g)
# stdoutをmultilineに表示するように設定する
window_g["layout_left_5_stdout_multiline"].reroute_stdout_to_here()
window_g["layout_left_5_stdout_multiline"].reroute_stderr_to_here()
#sg.cprint_set_output_destination(window_g, "base_model_multiline")
sg.cprint_set_output_destination(window_g, "layout_left_5_stdout_multiline")
window_g = window_g.refresh()
event_g, values_g = window_g.read(10)

# Event loop. Read buttons, make callbacks
while True:
    # Read the Window
    event_g, values_g = window_g.read()
    #print(event_g)
    sg.cprint(f"event= {event_g}", c='white on green')
    if event_g in ('Quit', sg.WIN_CLOSED):
        break
    # Lookup event in function dictionary
    if event_g in wvm_dictionary:
        #if event_g in [1,2]:
        func_to_call = wvm_dictionary[event_g]   # get function from dispatch dictionary
        window_g,values_g,configuration_dict_g,return_text = func_to_call(window_g,values_g,configuration_dict_g)
        if return_text != "":
            text_input = "something wrong"
            sg.popup(text_input, return_text,keep_on_top=True)
    else:
        print('Event {} not in dispatch dictionary'.format(event_g))

window_g.close()
window_g["layout_left_5_stdout_multiline"].restore_stdout()
window_g["layout_left_5_stdout_multiline"].restore_stderr()