
class My_simplified_laser():
    def __init__(self, finesse_laser):
        self._Component__name               = finesse_laser._Component__name
        self._requested_node_names          = finesse_laser._requested_node_names
        self._Component__id                 = finesse_laser._Component__id
        self.model                          = finesse_laser._kat
        self.simplified_params              = self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params                   =  {
        "P"                                            :  model.components[self._Component__name].P,      
        #"_laser__f_offset"                            :  model.components[self._Component__name]._laser__f_offset,      
        "f"                                            :  model.components[self._Component__name].f,      
        #"_laser__w0"                                  :  model.components[self._Component__name]._laser__w0,    
        #"_laser__z"                                   :  model.components[self._Component__name]._laser__z,    
        #"_laser__noise"                               :  model.components[self._Component__name]._laser__noise
        }  
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = parameter_value")
            return model
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

class My_simplified_mirror():
    def __init__(self, finesse_component):
        self._Component__name                  =  finesse_component._Component__name
        self._requested_node_names             =  finesse_component._requested_node_names            
        self._Component__id                    =  finesse_component._Component__id             
        self.model                             =  finesse_component._kat
        self.simplified_params                 =  self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params                 =  {
        "R"                                    :  model.components[self._Component__name]._AbstractMirrorComponent__R,      
        "T"                                    :  model.components[self._Component__name]._AbstractMirrorComponent__T,      
        "L"                                    :  model.components[self._Component__name]._AbstractMirrorComponent__L,      
        "phi"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__phi,    
        "Rcx"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Rcx,    
        "Rcy"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Rcy,    
        "xbeta"                                :  model.components[self._Component__name]._AbstractMirrorComponent__xbeta,  
        "ybeta"                                :  model.components[self._Component__name]._AbstractMirrorComponent__ybeta,  
        "mass"                                 :  model.components[self._Component__name]._AbstractMirrorComponent__mass, 
        "Ix"                                   :  model.components[self._Component__name]._AbstractMirrorComponent__Ix,     
        "Iy"                                   :  model.components[self._Component__name]._AbstractMirrorComponent__Iy,     
        "r_ap"                                 :  model.components[self._Component__name]._AbstractMirrorComponent__r_ap,   
        "zmech"                                :  model.components[self._Component__name]._AbstractMirrorComponent__zmech,  
        "rxmech"                               :  model.components[self._Component__name]._AbstractMirrorComponent__rxmech, 
        "rymech"                               :  model.components[self._Component__name]._AbstractMirrorComponent__rymech, 
        "z"                                    :  model.components[self._Component__name]._AbstractMirrorComponent__z,      
        "rx"                                   :  model.components[self._Component__name]._AbstractMirrorComponent__rx,     
        "ry"                                   :  model.components[self._Component__name]._AbstractMirrorComponent__ry,     
        "Fz"                                   :  model.components[self._Component__name]._AbstractMirrorComponent__Fz,     
        "Frx"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Frx,    
        "Fry"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Fry,    
        "Fs0"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Fs0,    
        "Fs1"                                  :  model.components[self._Component__name]._AbstractMirrorComponent__Fs1
        }  
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = parameter_value")
            return model
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
          
class My_simplified_beamSplitter():
    def __init__(self, finesse_component):
        self._Component__name                  =  finesse_component._Component__name
        self._requested_node_names             =  finesse_component._requested_node_names
        self._Component__id                    =  finesse_component._Component__id
        self.model                          =  finesse_component._kat
        self.simplified_params                 =  self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params                 =  {
        "R"                                 :  model.components[self._Component__name]._params[0],
        "T"                                 :  model.components[self._Component__name]._params[1],
        "L"                                 :  model.components[self._Component__name]._params[2],
        "phi"                               :  model.components[self._Component__name]._params[3],
        "Rcx"                               :  model.components[self._Component__name]._params[4],
        "Rcy"                               :  model.components[self._Component__name]._params[5],
        "xbeta"                             :  model.components[self._Component__name]._params[6],
        "ybeta"                             :  model.components[self._Component__name]._params[7],
        "mass"                              :  model.components[self._Component__name]._params[8],
        "Ix"                                :  model.components[self._Component__name]._params[9],#yawの回転の慣性モーメント
        "Iy"                                :  model.components[self._Component__name]._params[10],#yawの回転の慣性モーメント
        "r_ap"                              :  model.components[self._Component__name]._params[11],#mirrorの大きさ[m]
        "zmech"                             :  model.components[self._Component__name]._params[12],#zmech = transfer function of Longitudinal motion
        "rxmech"                            :  model.components[self._Component__name]._params[13],#rxmech = transfer function of yaw motion
        "rymech"                            :  model.components[self._Component__name]._params[14],#rymech = transfer function of pitch motion
        "z"                                 :  model.components[self._Component__name]._params[15],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: z = longitudinal motion
        "rx"                                :  model.components[self._Component__name]._params[16],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: rx, yaw = x-z plane rotation
        "ry"                                :  model.components[self._Component__name]._params[17],#Motion names used in xd detectors, slinks and xlinks to select particular motions of a suspended optic: ry, pitch = y-z plane rotation
        "Fz"                                :  model.components[self._Component__name]._params[18],# target property of the fsig to Fz, i.e. a force to the z motion.
        "Frx"                               :  model.components[self._Component__name]._params[19],#force in the rotational x-z plane motion.
        "Fry"                               :  model.components[self._Component__name]._params[20],#force in the rotational y-z plane motion.
        "Fs0"                               :  model.components[self._Component__name]._params[21],#よくわからない,おそらくsurface motionのtransfer functionのなにか
        "Fs1"                               :  model.components[self._Component__name]._params[22],#よくわからない,おそらくsurface motionのtransfer functionのなにか
        "s0"                                :  model.components[self._Component__name]._params[23],#よくわからない,おそらくsurface motionのなにか
        "s1"                                :  model.components[self._Component__name]._params[24],#よくわからない,おそらくsurface　motionのなにか
        "alpha"                             :  model.components[self._Component__name]._params[25]
        }  
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = parameter_value")
            return model
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
        
class My_simplified_space():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.Component                      =  finesse_component
        self.model                          =  finesse_component._kat
        self.simplified_params              =  self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params              =  {
        "L"                                 :  model.components[self._Component__name].L,
        "n"                                 :  model.components[self._Component__name].n,
        "gouyx"                             :  model.components[self._Component__name].gouyx,
        "gouyy"                             :  model.components[self._Component__name].gouyy,
        }
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = int(parameter_value)")
            return model

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

    def Connected_length_network(self):
        """
        spaceコンポーネントのnode1,node2に接続されているコンポーネントを辞書形式で受け取る
        return
        ----------
        length_network_dict : {node kat.node : component kat.component}
        """
        component_name = self._Component__name
        # 関連するコンポーネントを調べる
        test = self.Component.connectingComponents()
        if len(test)<2:
            # 一つしかコンポーネントがつながっていない場合

            pass
        else:
            # 選択したノードとコンポーネントが繋がっているか調べる
            def Am_i_connected_to(node,component):
                connection = list(component.nodes)
                if node in connection:
                    return True
                else:
                    return False
            # 繋がっているノードとコンポーネントを辞書にする
            component = test[0]
            length_network_dict = {}
            for component in test:
                for node in self.Component.nodes:
                    if Am_i_connected_to(node,component)==True:
                        length_network_dict[node]=component
                    else:
                        pass
            return length_network_dict
    
    def Parse_pick_off_bs(self,parse_xaxis=False):
        connecting_components = self.Component.connectingComponents()
        connecting_components = [s for s in connecting_components if s != None]
        port_num = len(connecting_components)
        if port_num<2:
            # 以下のような場合bsは挿入せずポートの名前だけ変更する
            """
            m n1 n2
            s_moto n2 n3
            """
            """
            m n1 n2
            s_moto n2 nn1
            """
            # モデルにノードを追加
            model = self.model
            component = self.Component
            length_name = self._Component__name
            node_names = model.getNewNodeNames(f"pick_offport_{component.name}_",1)
            for node in node_names:
                model.nodes.createNode(node)
            # 1つしかつながっていないノードを取得
            connecting_components = list(component.connectingComponents())
            #print(connecting_components)
            nodes = list(component.nodes)
            #print(nodes)
            for connecting_component in connecting_components:
                if connecting_component != None:
                    connecting_component_nodes = list(connecting_component.nodes)
                    for connecting_component_node in connecting_component_nodes:
                        if connecting_component_node in nodes:
                            nodes.remove(connecting_component_node)
            #print(nodes)#[n3]という1つしかないリストが得られる
            model.nodes.replaceNode(component,nodes[0],f"pick_offport_{component.name}_1")
            # xaxisの追加
            if parse_xaxis==True:
                model.parse(f"xaxis {component.name}  L lin 0 {self.Component.L} 1000")
            return model
        else:
            model = self.model
            length_name = self._Component__name
            # モデルにノードを追加
            node_names = model.getNewNodeNames(f"pickoff_node_{length_name}_",2)
            for node in node_names:
                model.nodes.createNode(node)
            # spaceの周りのノードを追加したノードと交換する
            component = model.components[length_name]
            dict = My_simplified_space(component).Connected_length_network()
            node1 = list(dict.keys())[0]
            node2 = list(dict.keys())[1]
            model.nodes.replaceNode(component,node1,model.nodes[node_names[1]])
            # pick off用のBSとspaceを追加
            """
            m n1 n2
            s n2 n3
            m n3 n4

            m n1 n2
            s n2 nn1
            bs nn1 nn1_r nn2 dump
            s_moto nn2 n3
            m n3 n4
            """
            model.parse(f"s pick_off_dummy_space_{component.name} 0 {node1.name} {node_names[0]}")
            model.parse(f"bs pick_off_bs_{component.name} 0 1 0 0 {node_names[0]} {node_names[0]}_r {node_names[1]} {node_names[1]}_r")
            model.parse(f"s pick_off_gouy_tuner1_{component.name} 0 {node_names[0]}_r pick_offport_{component.name}_1")
            model.parse(f"s pick_off_gouy_tuner2_{component.name} 0 {node_names[1]}_r pick_offport_{component.name}_2")
            # detectorの追加

            # xaxisの追加
            if parse_xaxis==True:
                model.parse(f"xaxis pick_off_dummy_space_{component.name}  L lin 0 {self.Component.L} 1000\nput* {component.name} L $mx1")
            return model
    
    def Get_pick_off_port(self):
        #ports = [self.model.nodes[f"pick_offport_1_{self._Component__name}"],self.model.nodes[f"pick_offport_2_{self._Component__name}"]]
        ports = [f"pick_offport_{self._Component__name}_1",f"pick_offport_{self._Component__name}_2"]
        return ports

    def parameter_change_test(self,value):
        self.Component.L = value
        
        return self.model 

# modulator
class My_simplified_modulator():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.model                          =  finesse_component._kat
        self.simplified_params              =  self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params                   =  {
        "f"                                 :  model.components[self._Component__name].f,
        "midx"                              :  model.components[self._Component__name].midx,
        "phase"                             :  model.components[self._Component__name].phase,
        "order"                             :  model.components[self._Component__name].order,
        "type"                              :  model.components[self._Component__name].type,
        }
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = parameter_value")
            return model
    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num
# lens
class My_simplified_lens():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.model                          =  finesse_component._kat
        self.simplified_params              =  self.get_simplified_params(self.model)
    def get_simplified_params(self,model):
        simplified_params              =  {
        "f"                                 :  model.components[self._Component__name].f,
        "p"                                 :  model.components[self._Component__name].p,
        }
        return simplified_params
    def Change_parameter(self,parameter_name,parameter_value):
        model = self.model
        params = self.get_simplified_params(model)
        if parameter_name not in params.keys():
            print("not in key [ My_simplified_space.Change_parameter() ]")
        else:
            exec(f"model.components[self._Component__name].{parameter_name} = parameter_value")
            return model

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

# lens
class My_simplified_pd0():
    def __init__(self, finesse_component):
        self._Component__name               =  finesse_component._Component__name
        self._requested_node_names          =  finesse_component._requested_node_names
        self._Component__id                 =  finesse_component._Component__id
        self.model                          =  finesse_component._kat
        self.simplified_params              =  {
        "f"                                 :  finesse_component.f,
        "p"                                 :  finesse_component.p,
        }

    def node_connection_num(self):
        num = len(self._requested_node_names)
        return num

# utilities
def Get_my_optics_class(class_name):
    """
    My_simplified_{class_name}を返す
    Parameters
    ----------
    class_name  : str 
        space,mirror,...
    return      : class My_simplified_space,...
    """
    if class_name=="space":
        return My_simplified_space
    elif class_name=="mirror":
        return My_simplified_mirror
    elif class_name=="beamSplitter":
        return My_simplified_beamSplitter
    elif class_name=="laser":
        return My_simplified_laser
    elif class_name=="modulator":
        return My_simplified_modulator
    elif class_name=="lens":
        return My_simplified_lens
    pass