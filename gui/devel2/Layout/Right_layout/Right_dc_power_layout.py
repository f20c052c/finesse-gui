import PySimpleGUI as sg
def Return_layout_right():
    key_head = "layout_right_dc_power"
    Components_list = ["mirror","space","beam splitter","laser","modulator"]
    Hermite_gauss_extension = ["cav","maxtem"]

    Mysize = (1,720)
    Base_models = ["DRFPMI"]
    Base_detectors = ["KAGRA"]
    #Categories = ["TEM","Mirror/Beam splitter","All"]
    #TEM_list = ["OFF","0","1","2"]
    #Port_list = ["AS","REFL","POP","POP2","TSMX","TSMY"]
    #Mirror_Beam_splitter_list = ["ITMX","ITMY"]
    #Mirror_parameter_list = ["R"]
    #Category_dict = {
    #    Categories[0]:[""],
    #    Categories[1]:Mirror_Beam_splitter_list
    #}

    # ,key=f"{key_head}_"

    # PD
    Layout_right_1_1 = [
        [sg.Text("・Putting on Photo detector")],
        [sg.Text("Node"),sg.Combo(["AS","REFL","POP","POP2","TSMX","TSMY"],size = (15,1),key=f"{key_head}_pd_setting_node",readonly=True,enable_events=True,background_color="silver"),sg.Text("・PD name"),sg.Combo([""],size = (30,1),key=f"{key_head}_pd_setting_name",background_color="silver"),sg.Button("Template name",button_color="silver",key=f"{key_head}_pd_setting_name_template")],
        [sg.Checkbox("Use QPD?",default=True, key=f"{key_head}_use_qpd_check",enable_events=True,background_color="silver"),sg.Text("Split plane"),sg.Combo(["yaw(x-xplit)","pitch(y-xplit)"],default_value="yaw(x-xplit)",size=(15,1),readonly=True,key=f"{key_head}_use_qpd_plane_combo",enable_events=True,background_color="silver"),sg.Checkbox("Receive inverse direction of propagation",default=False, key=f"{key_head}_use_astarisk_check",enable_events=True,background_color="silver")],
        [sg.Multiline(size=(80,10),expand_x=True,expand_y=True,key=f"{key_head}_add_pd_source_script")],
        [sg.Button("Add photo detector",expand_x=True,expand_y=True,button_color="#333333",enable_events=True,key=f"{key_head}_pd_setting_add_pd"),sg.Button("Clear photo detector",expand_x=True,expand_y=True,button_color="silver",enable_events=True,key=f"{key_head}_pd_setting_clear_pd")],
        [sg.Sizer(h_pixels = 0, v_pixels =1)],
        [sg.Text('To adjust the gouy phase, the model itself must be modified.\nPlease Change the "gouyx" and "gouyy" parameters of the space component named [{variable:port name}_gouy_tuner]')],
        [sg.Text("Corresponding gouy tuner component"),sg.Button("?",enable_events=True,key=f"{key_head}_pd_setting_gouy_tuner_help",button_color="silver"),sg.Combo([],size = (15,1),default_value="REFL_gouy_tuner"),sg.Combo(["yaw","pitch"],size = (5,1),default_value="yaw"),sg.Combo([],size = (5,1),default_value=0),sg.Text("[deg]"),sg.Button("Update",enable_events=True,key=f"{key_head}_pd_setting_gouy_tuner_update",button_color="silver")],

    ]
    Layout_right_1 = [
        [sg.Column(Layout_right_1_1,scrollable=True,expand_x=True,expand_y=True,size=(800,300))]
    ]
    # Xaxis
    # DC
    #   # template
    Layout_right_2_1_1_1 = [
        [sg.Text("Template"),sg.Combo([],size=(30,1),key=f"{key_head}_xaxis_setting_DC_template_select_name",readonly=True),sg.Button("Load",key=f"{key_head}_xaxis_setting_DC_template_load",enable_events=True,button_color="silver"),sg.Button("Clear",key=f"{key_head}_xaxis_setting_DC_template_clear",enable_events=True,button_color="silver")],
        [sg.Listbox(["",""]                      ,size=(30,8),expand_x=True,key=f"{key_head}_xaxis_setting_DC_template_xaxis_list")
        ,sg.Listbox([],size=(30,8),expand_x=True,key=f"{key_head}_xaxis_setting_DC_template_put_list")],

        [sg.Text("Source Script")],
        [sg.Multiline(size=(80,8),expand_x=True,expand_y=True,key=f"{key_head}_xaxis_setting_DC_template_source_script")],
        [sg.Button("Run",size=(30,1),button_color="#333333",expand_x=True,expand_y=False,key=f"{key_head}_template_run")],
    ]
    #   # add dof
    Layout_right_2_1_1_2_1 = [
        [sg.Text("・mirror")],
        [sg.Combo([],default_value="ITMX",size=(20,1)                       ,key=f"{key_head}_xaxis_setting_DC_mirror_select",readonly=True)],
        [sg.Text("・parameter")],
        [sg.Combo(["xbeta","ybeta","phi"],default_value="xbeta",size=(10,1) ,key=f"{key_head}_xaxis_setting_DC_parameter_select",readonly=True)],
        [sg.Checkbox("・Inherit initial values",default=True                ,key=f"{key_head}_xaxis_setting_DC_put_astarisk_check")],
        [sg.Text("・direction")],
        [sg.Combo(["+","-"],default_value="+",size=(10,1)                   ,key=f"{key_head}_xaxis_setting_DC_direction_select",readonly=True)],
        [sg.Button("Add",enable_events=True,button_color="#333333",expand_x=True,size=(None,2),key=f"{key_head}_xaxis_setting_DC_add_xaxis")]
    ]
    Layout_right_2_1_1_2_2 = [
        [sg.Listbox(["",""],size=(30,2),expand_x=True,key=f"{key_head}_xaxis_setting_DC_selected_xaxis_list")],
        [sg.Listbox([],size=(30,8),expand_x=True,key=f"{key_head}_xaxis_setting_DC_selected_put_list")],
        [sg.Button("Clear",button_color="silver",key=f"{key_head}_xaxis_setting_DC_source_script_clear",expand_x=True,size=(None,2))],
        [sg.Combo([],default_value="Template name",size=(20,1),key=f"{key_head}_xaxis_setting_DC_set_template_name"),sg.Button("Save as Template",enable_events=True,button_color="silver",key=f"{key_head}_xaxis_setting_DC_save_as_template")]
    ]
    Layout_right_2_1_1_2 = [
        [sg.Text("・Range [deg]"),sg.Combo([],default_value="-1e-6",size=(10,1),key=f"{key_head}_xaxis_setting_DC_range_min_select"),sg.Text("〜"),sg.Combo([],default_value="1e-6",size=(10,1),key=f"{key_head}_xaxis_setting_DC_range_max_select"),sg.Text("Sampling num"),sg.Combo([],key=f"{key_head}_xaxis_setting_DC_sampling_num_select",size=(10,1),default_value="1000"),sg.Button("Set",button_color="silver",key=f"{key_head}_xaxis_setting_DC_line_xaxis_set",expand_x=True,size=(None,2),enable_events=True)],
        [sg.Text("  -> All mirrors move the same amount.")],
        [sg.Frame("",Layout_right_2_1_1_2_1,expand_x=True,expand_y=True),sg.Frame("",Layout_right_2_1_1_2_2,expand_x=True,expand_y=True)],
        [sg.Text("Source Script")],
        [sg.Multiline(size=(80,8),expand_x=True,expand_y=True,key=f"{key_head}_xaxis_setting_DC_source_script")],
        [sg.Button("Run",size=(30,1),button_color="#333333",expand_x=True,expand_y=True,key=f"{key_head}_add_dof_run")],
        
    ]
    Layout_right_2_1_1 = [
        [sg.TabGroup([[sg.Tab("Add DoF",Layout_right_2_1_1_2)],[sg.Tab("Template",Layout_right_2_1_1_1)]])],
    ]
    # AC
    #   # template
    Layout_right_2_1_2_1 = [
        [sg.Text("Template"),sg.Combo([],size=(15,2))],
        [sg.Multiline(size=(80,10),expand_x=True,expand_y=True)],
        [sg.Text("Source Script")],
        [sg.Multiline(size=(80,10),expand_x=True,expand_y=True)],
    ]
    #   # add dof
    Layout_right_2_1_2_2_1 = [
        [sg.Text("・mirror")],
        [sg.Combo([],default_value="ITMX",size=(20,1)                       ,key=f"{key_head}_xaxis_setting_AC_mirror_select",readonly=True)],
        [sg.Text("・parameter")],
        [sg.Combo(["xbeta","ybeta","phi"],default_value="xbeta",size=(10,1) ,key=f"{key_head}_xaxis_setting_AC_parameter_select",readonly=True)],
        [sg.Text("・direction")],
        [sg.Combo(["+","-"],default_value="+",size=(10,1)                   ,key=f"{key_head}_xaxis_setting_AC_direction_select",readonly=True)],
        [sg.Button("Add",button_color="#333333",expand_x=True,size=(None,2))]
    ]
    Layout_right_2_1_2_2_2 = [
        [sg.Listbox(["ITMX","ITMY","ITMY","ITMY"],size=(30,10),expand_x=True)],
        [sg.Combo([],default_value="Template name",size=(20,1)),sg.Button("Save as Template",button_color="silver")]
    ]
    Layout_right_2_1_2_2 = [
        [sg.Text("・Range [deg]"),sg.Combo([],default_value="-1e-6",size=(10,1)),sg.Text("〜"),sg.Combo([],default_value="1e-6",size=(10,1))],
        [sg.Text("  -> All mirrors move the same amount.")],
        [sg.Frame("",Layout_right_2_1_2_2_1,expand_x=True,expand_y=True),sg.Frame("",Layout_right_2_1_2_2_2,expand_x=True,expand_y=True)],
        [sg.Text("Source Script")],
        [sg.Multiline(size=(80,10),expand_x=True,expand_y=True)],
        [sg.Button("Clear",button_color="silver",expand_x=True,size=(None,2))],
        
    ]
    Layout_right_2_1_2 = [
        [sg.TabGroup([[sg.Tab("Add DoF",Layout_right_2_1_2_2,key=f"{key_head}_Layout_right_2_1_2_add_doF")],[sg.Tab("Template",Layout_right_2_1_2_1,key=f"{key_head}_Layout_right_2_1_2_template")]])],
    ]
    Layout_right_2_1 = [
        [sg.Text("・Simulation Configuration")],
        #[sg.TabGroup([[sg.Tab("DC",Layout_right_2_1_1)],[sg.Tab("AC",Layout_right_2_1_2)]])],
        [sg.TabGroup([[sg.Tab("DC",Layout_right_2_1_1)]])],
        #[sg.Sizer(h_pixels = 580, v_pixels =50)]
    ]
    Layout_right_2 = [
        [sg.Column(Layout_right_2_1,scrollable=True,expand_x=True,expand_y=True,size=(800,600))]
    ]
    #Layout_right_3_1_1 = [
    #    [sg.Text("・Detectors")],
    #    [sg.Combo([],default_value="REFL_pd",size = (20,1))],
    #    [sg.Button("Add",button_color="silver",expand_x=True,size=(None,2))]
    #]
    #Layout_right_3_1_2 = [
    #    [sg.Text("Figure title"),sg.Combo([],default_value="test figure title",size = (20,1))],
    #    [sg.Text("Axes title"),sg.Combo([],default_value="test axes title",size = (20,1)),sg.Text("Axes number"),sg.Combo([1,2,3,4],default_value=1,size = (10,1))],
    #    [sg.Text("xlabels"),sg.Combo([],default_value="test x labels",size = (20,1)),sg.Text("ylabels"),sg.Combo([],default_value="test y labels",size = (20,1))],
    #    [sg.Text("Plot scale"),sg.Combo([],default_value="log-log",size = (20,1))],
    #    [sg.Listbox(["REFL_pd","REFL_pd2","REFL_pd3","REFL_pd4"],size=(60,10),expand_x=True)],
    #    [sg.Button("Plot",button_color="#333333",expand_x=True,size=(None,5))],
    #    [sg.Sizer(h_pixels = 0, v_pixels =5)],
    #    [sg.Button("Clear Axes",button_color="silver",expand_x=True,size=(None,1))]
    #]
    #Layout_right_3_1 = [
    #    [sg.Text("・Plot Configuration")],
    #    [sg.Frame("",Layout_right_3_1_1,expand_x=True,expand_y=True),sg.Frame("",Layout_right_3_1_2,expand_x=True,expand_y=True)],
    #    [sg.Button("Clear Figure",button_color="silver",expand_x=True,size=(None,1))]
#
    #]
    #Layout_right_3= [
    #    [sg.Column(Layout_right_3_1,scrollable=True,expand_x=True,expand_y=True,size=(600,360))]
    #]
#
#
    Layout_right_4 = [
        [sg.Column([
            [sg.Frame("",Layout_right_1,size=(640,480),expand_x=True,expand_y=True)],
            #[sg.Text("\n")],
            [sg.Frame("",Layout_right_2,size=(640,480),expand_x=True,expand_y=True)],
            [sg.Text("\n")],
            #[sg.Frame("",Layout_right_3,size=(640,480),expand_x=True,expand_y=True)]
        ],size=Mysize,scrollable=True,vertical_scroll_only=False,expand_x=True,expand_y=True)]
    ]

    return Layout_right_4
