import PySimpleGUI as sg

def Return_simulation_reusult_tab():

    Layout_right_3_1_1 = [
        [sg.Text("・Out history")],
        [sg.Combo([],default_value="current_out"        ,key="layout_right_simulation_result_out_history_name",size = (20,1),readonly=True)],
        [sg.Button("Load",button_color="silver"     ,key="layout_right_simulation_result_out_history_load",expand_x=True,size=(None,2),enable_events=True)],
        [sg.Text("・Detectors")],
        [sg.Combo([],default_value="REFL_pd"                                    ,key="layout_right_simulation_result_select_detectors_combo",size = (20,1),readonly=True)],
        #[sg.Text("Legend"),sg.Combo([],default_value="test_legend"              ,key="layout_right_simulation_result_select_pd_legend",size = (20,1))],
        [sg.Text("Line color"),sg.Combo(["default","r", "g", "b", "c", "m", "y", "k", "w"],key="layout_right_simulation_result_select_line_color",default_value="default",size = (20,1),readonly=True)],
        [sg.Text("Line style"),sg.Combo(["default","-","--",":","-."]                     ,key="layout_right_simulation_result_select_line_style",default_value="default",size = (20,1),readonly=True)],
        [sg.Button("Add",button_color="#333333"                              ,key="layout_right_simulation_result_select_detectors_add",expand_x=True,size=(None,2),enable_events=True)]
    ]
    
    Layout_right_3_1_2 = [
        [sg.Text("Figure title"),sg.Combo([],default_value="test figure title",size = (20,1),key="layout_right_simulation_result_figure_title")],
        [sg.Text("Axes title")  ,sg.Combo(["使えません"],default_value="test axes title"  ,size = (20,1),key="layout_right_simulation_result_axes_title",enable_events=True,readonly=True),sg.Text("Axes number"),sg.Combo([1,2,3,4],default_value=1        ,size = (10,1),key="layout_right_simulation_result_axes_number",enable_events=True)],
        [sg.Text("xlabels")     ,sg.Combo([],default_value="test x labels"    ,size = (20,1),key="layout_right_simulation_result_xlabels")   ,sg.Text("ylabels")    ,sg.Combo([],default_value="test y labels" ,size = (20,1),key="layout_right_simulation_result_ylabels")],
        [sg.Text("Plot scale")  ,sg.Combo(["linear-linear","linear-log","log-linear","log-log"],default_value="linear-linear"         ,size = (20,1),key="layout_right_simulation_result_plot_scale",readonly=True)
        ,sg.Text("Plot style"),sg.Combo(["default","power","abs power","abs","angle","abs+angle"]     ,key="layout_right_simulation_result_select_plot_style",default_value="default",size = (20,1),readonly=True)],
        [sg.Listbox([],key="layout_right_simulation_result_detectors_list",size=(60,10),expand_x=True)],
        [sg.Button("Plot",button_color="#333333",expand_x=True,size=(None,5),enable_events=True,key="layout_right_simulation_result_plot_run")],
        [sg.Sizer(h_pixels = 0, v_pixels =5)],
        [sg.Button("Clear Axes",button_color="silver",expand_x=True,size=(None,1),key="layout_right_simulation_result_clear_axes")]
    ]
    Layout_right_3_1 = [
        [sg.Text("・Plot Configuration")],
        [sg.Frame("",Layout_right_3_1_1,expand_x=True,expand_y=True),sg.Frame("",Layout_right_3_1_2,expand_x=True,expand_y=True)],
        [sg.Button("Clear Figure",enable_events=True,button_color="silver",expand_x=True,size=(None,1),key="layout_right_simulation_result_clear_figure")],
        [sg.Button("Open Figure window",enable_events=True,button_color="#333333",expand_x=True,size=(None,1),key="layout_right_simulation_result_open_figure_window")]

    ]
    Layout_right_3= [
        [sg.Column(Layout_right_3_1,scrollable=True,expand_x=True,expand_y=True,size=(600,360))]
    ]

    layout = [
        [sg.Image('../fig/DRFPMI_picture_normal.png', key='-IMAGE3-',  size=(400,300),background_color="White")],
        [sg.Frame("",Layout_right_3,size=(640,480),expand_x=True,expand_y=True)]
    ]

    return layout
