# %%
import PySimpleGUI as sg
sg.theme('GreenTan') # give our window a spiffy set of colors


# %%
layout = [
        [sg.Multiline(size=(800, 900), enter_submits=False, key='-QUERY-', do_not_clear=False)]
    ]

window = sg.Window('Chat window', layout, font=('Helvetica', ' 13'), default_button_element_size=(8,2),size = (1600, 900), use_default_focus=False)

# %%

while True:     # The Event Loop
    event, value = window.read()
    if event in (sg.WIN_CLOSED, 'EXIT'):            # quit if exit button or X
        break
    if event == 'SEND':
        query = value['-QUERY-'].rstrip()
        # EXECUTE YOUR COMMAND HERE
        print('The command you entered was {}'.format(query), flush=True)

window.close()

# %%



